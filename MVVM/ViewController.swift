//
//  ViewController.swift
//  Lela
//
//  Created by Вова Петров on 25.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SwiftEntryKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    ///Вернуть View, начиная с которой экран необходим при открытой клавиатуре
    func keyboardShowView() -> UIView? {
        return nil
    }
    
    ///Вернуть отступ для keyboardShowView()
    func keyboardViewOffsetY() -> CGFloat {
        return 0
    }
    
}

//MARK: - Alerts
extension ViewController {
    func showPopupMessage(title: String,
                          titleColor: EKColor = EKColor.init(UIColor.text.standart.standart),
                          description: String,
                          descriptionColor: EKColor = EKColor.init(UIColor.text.standart.standart),
                          buttonTitleColor: EKColor = EKColor.init(UIColor.button.standart.standart.background),
                          buttonBackgroundColor: EKColor = EKColor.init(UIColor.button.standart.standart.text),
                          icon: Assets.Icon = Assets.Icon.icSuccess,
                          action: @escaping(()->()) = {}) {
        var attributes = EKAttributesPresets.bottomAlertAttributes
        attributes.entranceAnimation = .init(
            translate: .init(duration: 0.65, spring: .init(damping: 0.8, initialVelocity: 0))
        )
        
        let themeImage = EKPopUpMessage.ThemeImage(
            image: EKProperty.ImageContent(
                image: icon.image,
                displayMode: .inferred,
                size: CGSize(width: 60, height: 60),
                tint: titleColor,
                contentMode: .scaleAspectFit
            )
        )
        
        let title = EKProperty.LabelContent(
            text: title,
            style: .init(
                font: .systemFont(ofSize: 17),
                color: titleColor,
                alignment: .center,
                displayMode: .inferred
            )
        )
        let description = EKProperty.LabelContent(
            text: description,
            style: .init(
                font: .systemFont(ofSize: 16),
                color: descriptionColor,
                alignment: .center,
                displayMode: .inferred
            )
        )
        let button = EKProperty.ButtonContent(
            label: .init(
                text: "Готово",
                style: .init(
                    font: .systemFont(ofSize: 16),
                    color: buttonTitleColor,
                    displayMode: .inferred
                )
            ),
            backgroundColor: buttonBackgroundColor,
            highlightedBackgroundColor: buttonTitleColor.with(alpha: 0.05),
            displayMode: .inferred
        )
        let message = EKPopUpMessage(
            themeImage: themeImage,
            title: title,
            description: description,
            button: button) {
                SwiftEntryKit.dismiss()
                action()
        }
        let contentView = EKPopUpMessageView(with: message)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
}

//MARK: - keyboard
extension ViewController {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            guard let keyboardVisibleView = keyboardShowView() else {
                return
            }
            let offset = keyboardViewOffsetY()
            let height = self.view.frame.height - keyboardVisibleView.frame.minY - offset
            if  height < keyboardSize.height + 20 {
                self.view.frame.origin.y = 0.0 - (keyboardSize.height + 20 - height)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y = 0.0
        }
    }
}
