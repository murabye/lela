//
//  File.swift
//  Lela
//
//  Created by Влада Кузнецова on 13/09/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

protocol ViewType: class {
    associatedtype ViewModelType: ViewModelProtocol
    /// Configurates controller with specified ViewModelProtocol subclass
    ///
    /// - Parameter viewModel: CPViewModel subclass instance to configure with
    func configure(with viewModel: ViewModelType)
    /// Factory function for view controller instatiation
    ///
    /// - Parameter viewModel: View model object
    /// - Returns: View of concrete type
    static func create(with viewModel: ViewModelType) -> UIView
}
