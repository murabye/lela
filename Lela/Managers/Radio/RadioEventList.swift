//
//  RadioEventList.swift
//  Lela
//
//  Created by Вова Петров on 15.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift


extension Radio.EventList {
    private static let updateListSubject = PublishSubject<Void>()
}

extension Radio.EventList.Say {
    static func needUpdate() {
        Radio.EventList.updateListSubject.onNext(())
    }
}

extension Radio.EventList.Listen {
    static func needUpdate() -> Observable<Void> {
        return Radio.EventList.updateListSubject
            .asObservable()
    }
}

