//
//  UserDefaultManager.swift
//  Lela
//
//  Created by Влада Кузнецова on 03/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

class UserDefaultsProvider {
    struct UserDefaultObject {
        let key: String
        let valueType: Any.Type
        
        init(_ key: String, of valueType: Any.Type) {
            self.key = key
            self.valueType = valueType
        }
    }
    
    static let login = UserDefaultObject("login", of: String.self)
    static let password = UserDefaultObject("password", of: String.self)
    static let token = UserDefaultObject("token", of: String.self)
}

extension UserDefaults {
    // задел на виджеты/часы
    static let our = UserDefaults.standard
}
