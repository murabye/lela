//
//  KeyChain.swift
//  Lela
//
//  Created by варя on 07/07/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import Security

// see https://stackoverflow.com/a/37539998/1694526

class KeychainService {
    
    // MARK:- public interface
    class func clearCredentials() {
        clearLogin()
    }

    class func setCredentials(_ credentials: Credentials) {
        let login = credentials.email
        setLogin(login)
        
        let password = credentials.password
        if let passwordData = password.data(using: .utf8) {
            let status = KeychainService.save(key: login, data: passwordData)
            print("keychain save status \(status)")
        }
        print("keychain cannot convert password to data!")
    }
    
    class func getCredentials() -> Credentials? {
        guard let login = getLogin(),
            let receivedData = KeychainService.load(key: login) else {
                return nil
        }
        
        let pass = String(decoding: receivedData, as: UTF8.self)
        return Credentials(email: login, password: pass)
    }
    
    
    // MARK:- password CRUD
    private class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]

        SecItemDelete(query as CFDictionary)

        return SecItemAdd(query as CFDictionary, nil)
    }

    private class func load(key: String) -> Data? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]

        var dataTypeRef: AnyObject? = nil

        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }

    private class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)

        let swiftString: String = cfStr as String
        return swiftString
    }

    // MARK:- login CRUD
    private class func getLogin() -> String? {
        let loginKey = UserDefaultsProvider.login.key
        let userDefaults = UserDefaults.our
        
        let login = userDefaults.string(forKey: loginKey)
        return login
    }
    
    private class func setLogin(_ login: String) {
        let loginKey = UserDefaultsProvider.login.key
        let userDefaults = UserDefaults.our
        userDefaults.set(login, forKey: loginKey)
        userDefaults.synchronize()
    }
    
    private class func clearLogin() {
        let loginKey = UserDefaultsProvider.login.key
        let userDefaults = UserDefaults.our
        userDefaults.removeObject(forKey: loginKey)
        userDefaults.synchronize()
    }

}
