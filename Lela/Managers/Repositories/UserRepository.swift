//
//  UserRepository.swift
//  Lela
//
//  Created by Вова Петров on 22.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

class UserRepository {
    static func getUser(by id: Int) -> Observable<Models.User>{
        
        struct Body: Codable {
            var id: Int
        }
        
        return NetworkManager
            .requestObservable(from: AdressesProvider.userIndex.value(),
                                          allowedFromCache: true,
                                          method: .post,
                                          body: Body(id: id))
    }
}
