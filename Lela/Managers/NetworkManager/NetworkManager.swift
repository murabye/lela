//
//  NetworkManager.swift
//  Lela
//
//  Created by варя on 07/07/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class NetworkManager {
    
    static let disableCache: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        return SessionManager(configuration: configuration)
    }()
    
    static let standart: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return SessionManager(configuration: configuration)
    }()

    static func requestObservable<T: Codable> (from address: URL,
                                        allowedFromCache: Bool = true,
                                        method: HTTPMethod,
                                        body: Codable?) -> Observable<T> {
        let observable: Observable<T> = NetworkManager.simpleRequest(from: address, allowedFromCache: allowedFromCache, method: method, body: body)
        
        let catched = observable
            .subscribeOn(MainScheduler.instance)
            .catchError { (uncustedError) -> Observable<T> in
                guard let error = uncustedError as? Errors else {
                    return Observable<T>.error(uncustedError)
                }
                
                switch error {
                case .oldToken:
                    let disloginSubject: PublishSubject<T> = PublishSubject()
                    LoginRouter.dislogin {
                        disloginSubject.onError(Errors.successLogin)
                    }
                    
                    let disloginObservable: Observable<T> = disloginSubject.asObservable()
                    let catched = disloginObservable.catchError { (error) -> Observable<T> in
                        return NetworkManager.simpleRequest(from: address, allowedFromCache: allowedFromCache,  method: method, body: body)
                    }
                    
                    return catched
                default:
                    return Observable<T>.error(error)
                }
        }
        
        return catched
    }
    
    static func register(_ body: RegisterModel) -> Observable<RegisterResultModel> {
        let observable = Observable<RegisterResultModel>.create { observer in
            let headers = NetworkManager.getHeaders()
            let sessionManager = disableCache
            let bodyDictionary = body.asDictionary
            UserService.saveLoginData(from: body)
            
            let req = sessionManager.request(AdressesProvider.createUser.value(),
                                             method: .post,
                                             parameters: bodyDictionary,
                                             encoding: JSONEncoding.default,
                                             headers: headers)
                .responseJSON(completionHandler: { response in
                    let result: Result<RegisterResultModel> = ResponceDecoder.decodeResponse(from: response)
                    let errorM: Result<ErrorModel> = ResponceDecoder.decodeResponse(from: response)
                    
                    if let error = try? errorM.unwrap(), error.error {
                        observer.onError(Errors.serverSide(description: error.reason,
                                                           code: response.response?.statusCode ?? -1))
                    } else if let error = result.error {
                        observer.onError(error)
                    } else if let value = try? result.unwrap() {
                        observer.onNext(value)
                        observer.onCompleted()
                    } else {
                        observer.onError(Errors.unknown(description: nil))
                    }
            })
            
            return Disposables.create {
                req.cancel()
            }
        }
        
        return observable.subscribeOn(MainScheduler.instance)
    }

    
    static func login(with credentials: Credentials) -> Observable<UserService.LoginResponce> {
        let observable = Observable<UserService.LoginResponce>.create { observer in
            
            let login = credentials.email
            let password = credentials.password

            var headers = NetworkManager.getHeaders()
            let authString = String(format: "%@:%@", login, password)
            let authData = authString.data(using: String.Encoding.utf8)!
            let base64LoginString = authData.base64EncodedString()
            headers["Authorization"] = "Basic \(base64LoginString)"
            
            let sessionManager = disableCache
            let req = sessionManager.request(AdressesProvider.login.value(),
                                             method: .post,
                                             parameters: [:],
                                             encoding: JSONEncoding.default,
                                             headers: headers)
                .responseJSON(completionHandler: { response in
                    let result: Result<UserService.LoginResponce> = ResponceDecoder.decodeResponse(from: response)
                    let errorM: Result<ErrorModel> = ResponceDecoder.decodeResponse(from: response)
                    
                    if response.response?.statusCode == 401 {
                        observer.onError(Errors.unknown(description: "Неправильный логин или пароль"))
                    } else if let error = try? errorM.unwrap(), error.error {
                        observer.onError(Errors.serverSide(description: error.reason,
                                                           code: response.response?.statusCode ?? -1))
                    } else if let error = result.error {
                        observer.onError(error)
                    } else if let value = try? result.unwrap() {
                        UserService.loginedWith(loginResponce: value)
                        observer.onNext(value)
                        observer.onCompleted()
                    } else {
                        observer.onError(Errors.unknown(description: nil))
                    }
            })
            
            return Disposables.create {
                req.cancel()
            }
        }
        
        return observable.subscribeOn(MainScheduler.instance)
    }

}

// MARK:- private helpers
private extension NetworkManager {
    static func getHeaders() -> HTTPHeaders {
        var headers: HTTPHeaders = ["Content-Type": "application/json"]
        
        guard let token = UserDefaults.standard.string(forKey: UserDefaultsProvider.token.key) else {
            return headers
        }
        
        headers["Authorization"] = "Bearer " + token
        return headers
    }
}

// MARK:- request senders
private extension NetworkManager {
    static private func simpleRequest<T: Codable> (from address: URL,
                                        allowedFromCache: Bool = true,
                                        method: HTTPMethod,
                                        body: Codable?) -> Observable<T> {
        
        return Observable<T>.create { observer in
            let headers = NetworkManager.getHeaders()
            let bodyDictionary = body?.asDictionary ?? [String: Any]()
            let sessionManager = allowedFromCache ? standart : disableCache
            
            let req = sessionManager.request(address, method: method, parameters: bodyDictionary, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in

                let result: Result<T> = ResponceDecoder.decodeResponse(from: response)
                let errorM: Result<ErrorModel> = ResponceDecoder.decodeResponse(from: response)
                
                if response.response?.statusCode == 401 {
                    observer.onError(Errors.oldToken)
                } else if let error = try? errorM.unwrap(), error.error {
                    observer.onError(Errors.serverSide(description: error.reason,
                                                       code: response.response?.statusCode ?? -1))
                } else if let error = result.error {
                    observer.onError(error)
                } else if let value = try? result.unwrap() {
                    observer.onNext(value)
                    observer.onCompleted()
                } else {
                    observer.onError(Errors.unknown(description: nil))
                }
            })
            
            return Disposables.create {
                req.cancel()
            }
        }
    }
}

