//
//  RequestAdress.swift
//  Lela
//
//  Created by варя on 07/07/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

public enum AdressesProvider: String {
    case createUser = "user/create"
    case login = "user/login"
    case changeUser = "user/change"
    case userList = "user/list"
    case userIndex = "user/index"
    
    case createCategory = "category/create"
    case categoryList = "category/list"
    
    case eventList = "event/list"
    case createEvent = "event/create"
    
    case partDelete = "part/delete"
    case partIndex = "part/index"
    case partCreate = "part/create"
    case partConfirm = "part/confirm"
    case partList = "part/list"
    
    case awardTypeList = "award/type/list"
    case awardTypeDelete = "award/type/delete"
    case awardTypeCreate = "award/type/create"
    case awardTypeChange = "award/type/change"

    case awardOfficeList = "award/office/list"
    case awardOfficeDelete = "award/office/delete"
    case awardOfficeCreate = "award/office/create"
    case awardOfficeChange = "award/office/change"

    case awardExemplareList = "award/list"
    case awardExemplareDelete = "award/delete"
    case awardExemplareCreate = "award/create"
    case awardExemplareChange = "award/change"

    func value(version: Int = 1) -> URL {
        let basicAddress = "http://lela-server.herokuapp.com"
        let apiVersion = "/v\(version)/"
        let standartCompleteAddress = basicAddress + apiVersion
        
        return URL.init(string: standartCompleteAddress + self.rawValue)!
    }
}
