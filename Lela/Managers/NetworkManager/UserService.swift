//
//  UserService.swift
//  Lela
//
//  Created by Влада Кузнецова on 04/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

class UserService {
    
    static private(set) var currentUser: Models.User?
    static func loginedWith(loginResponce: LoginResponce) {
        currentUser = loginResponce.user
        UserDefaults.standard.set(loginResponce.token.string, forKey: UserDefaultsProvider.token.key)
        UserDefaults.standard.synchronize()
    }
    
    static func saveLoginData(from registerModel: RegisterModel) {
        KeychainService.setCredentials(registerModel.toCredentials())
    }
    
    struct LoginResponce: Codable {
        var user: Models.User
        var token: UserToken
    }
    
     struct UserToken: Codable {
        var string: String
        var expiresAt: Date?
    }    
}
