//
//  ErrorModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 06/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

struct ErrorModel: Codable {
    let reason: String
    let error: Bool
}

extension NetworkManager {
    enum Errors: LocalizedError {
        case unknown(description: String?)
        case serverSide(description: String?, code: Int)
        case oldToken
        case successLogin
    
        public var errorDescription: String? {
            switch self {
            case .unknown(description: let description):
                return description ?? "Произошла неизвестная ошибка: попробуйте еще раз позже или   обновите приложение"
            case .oldToken:
                return "Войдите в приложение"
            case .successLogin:
                return "Попробуйте еще раз"
            case .serverSide(description: let description, code: let code):
                let descr = description ?? "Произошла неизвестная ошибка: попробуйте еще раз позже или  обновите приложение"
                 return "\(descr)\n Код ошибки: \(code)"
            }
        }
    }
}
