//
//  SiburImageProvider.swift
//  Lela
//
//  Created by Владимир on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

class SiburImageProvider: ImageProvider {
    func defaultAvatar() -> UIImage {
        return UIImage(named: "sibur_default_avatar")!
    }
    func money() -> UIImage {
        return UIImage(named: "sibur_money")!
    }
    func logout() -> UIImage {
        return UIImage(named: "sibur_logout")!
    }
    func logo() -> UIImage {
        return UIImage(named: "sibur_logo_white")!
    }
    func loadingImage() -> UIImage {
        return UIImage(named: "sibur_loading")!
    }
    func profileMenu() -> UIImage {
        return UIImage(named: "sibur_menu_profile")!
    }
    func eventsMenu() -> UIImage {
        return UIImage(named: "sibur_menu_store")!
    }
    func settings() -> UIImage {
        return UIImage(named: "sibur_settings")!
    }
    func store() -> UIImage {
        return UIImage(named: "sibur_store")!
    }
    func medium() -> UIImageView {
        let img = UIImage(named: "sibur_medium")!.withRenderingMode(.alwaysTemplate)
        let imgView = UIImageView(image: img)
        imgView.tintColor = UIColor.main
        
        return imgView
    }
    func median() -> UIImageView {
        let img = UIImage(named: "sibur_median")!.withRenderingMode(.alwaysTemplate)
        let imgView = UIImageView(image: img)
        imgView.tintColor = UIColor.main
        
        return imgView
    }
    func deviation() -> UIImageView {
        let img = UIImage(named: "sibur_deviation")!.withRenderingMode(.alwaysTemplate)
        let imgView = UIImageView(image: img)
        imgView.tintColor = UIColor.main
        
        return imgView
    }
    func statistic() -> UIImage {
        return UIImage(named: "sibur_statistic")!.withRenderingMode(.alwaysTemplate)
    }
    func back() -> UIImage {
        return UIImage(named: "sibur_back")!.withRenderingMode(.alwaysTemplate)
    }
    func darkPlaceholder() -> UIImage {
        return UIImage(named: "sibur_placeholder_dark")!
    }
    func photo() -> UIImage {
        return UIImage(named: "photo")!
    }
    func history() -> UIImage {
        return UIImage(named: "sibur_history")!
    }
}
