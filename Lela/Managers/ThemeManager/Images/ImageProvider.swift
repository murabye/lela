//
//  ImageProvider.swift
//  Lela
//
//  Created by Владимир on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

protocol ImageProvider {
    func logo() -> UIImage
    func loadingImage() -> UIImage
    func profileMenu() -> UIImage
    func eventsMenu() -> UIImage
    func defaultAvatar() -> UIImage
    func money() -> UIImage
    func logout() -> UIImage
    func settings() -> UIImage
    func store() -> UIImage
    func medium() -> UIImageView
    func median() -> UIImageView
    func deviation() -> UIImageView
    func statistic() -> UIImage
    func back() -> UIImage
    func darkPlaceholder() -> UIImage
    func photo() -> UIImage
    func history() -> UIImage
}
