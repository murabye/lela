//
//  ImageProvider.swift
//  Lela
//
//  Created by Владимир on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIImage {
    static var logo: UIImage {
        return ThemeManager.shared.imageProvider.logo()
    }
    static var loading: UIImage {
        return ThemeManager.shared.imageProvider.loadingImage()
    }
    static var profileMenu: UIImage {
        return ThemeManager.shared.imageProvider.profileMenu()
    }
    
    static var eventsMenu: UIImage {
        return ThemeManager.shared.imageProvider.eventsMenu()
    }
    static var logout: UIImage {
        return ThemeManager.shared.imageProvider.logout()
    }
    static var defaultAvatar: UIImage {
        return ThemeManager.shared.imageProvider.defaultAvatar()
    }
    static var money: UIImage {
        return ThemeManager.shared.imageProvider.money()
    }
    static var settings: UIImage {
        return ThemeManager.shared.imageProvider.settings()
    }
    static var store: UIImage {
        return ThemeManager.shared.imageProvider.store()
    }
    static var statistic: UIImage {
        return ThemeManager.shared.imageProvider.statistic()
    }
    static var medium: UIImageView {
        return ThemeManager.shared.imageProvider.medium()
    }
    static var median: UIImageView {
        return ThemeManager.shared.imageProvider.median()
    }
    static var deviation: UIImageView {
        return ThemeManager.shared.imageProvider.deviation()
    }
    static var back: UIImage {
        return ThemeManager.shared.imageProvider.back()
    }
    static var darkPlaceholder: UIImage {
        return ThemeManager.shared.imageProvider.darkPlaceholder()
    }
    static var photo: UIImage {
        return ThemeManager.shared.imageProvider.photo()
    }
    static var history: UIImage {
        return ThemeManager.shared.imageProvider.history()
    }
}
