//
//  UIColor+.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIColor {
    class var main: UIColor {
        return ThemeManager.shared.colorProvider.main
    }
    
    class text {
        class standart {
            class var disabled: UIColor {
                return ThemeManager.shared.colorProvider.textDisabled
            }
            class var standart: UIColor {
                return ThemeManager.shared.colorProvider.text
            }
            class var subtitle: UIColor {
                return ThemeManager.shared.colorProvider.textSubtitle
            }
            class var important: UIColor {
                return ThemeManager.shared.colorProvider.textImportant
            }
        }
        
        class inverted {
            class var disabled: UIColor {
                return ThemeManager.shared.colorProvider.textInvertedDisabled
            }
            class var standart: UIColor {
                return ThemeManager.shared.colorProvider.textInverted
            }
            class var subtitle: UIColor {
                return ThemeManager.shared.colorProvider.textInvertedSubtitle
            }
        }
    }
    
    class button {
        class standart {
            class disabled {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonDisabledBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonDisabledText
                }
            }
            class standart {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonText
                }
            }
            
            class highlighted {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonHighlightedBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonHighlightedText
                }
            }
        }
        class inverted {
            class disabled {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedDisabledBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedDisabledText
                }
            }
            class standart {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedText
                }
            }
            
            class highlighted {
                class var background: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedHighlightedBackground
                }
                class var text: UIColor {
                    return ThemeManager.shared.colorProvider.buttonInvertedHighlightedText
                }
            }
        }
    }
    
    class app {
        class var background: UIColor {
            return ThemeManager.shared.colorProvider.appBackground
        }
    }
    
    class view {
        class standart {
            class var background: UIColor {
                return ThemeManager.shared.colorProvider.viewBackground
            }
            class var accent: UIColor {
                return ThemeManager.shared.colorProvider.viewAccent
            }
        }
        class inverted {
            class var background: UIColor {
                return ThemeManager.shared.colorProvider.viewInvertedBackground
            }
            class var accent: UIColor {
                return ThemeManager.shared.colorProvider.viewInvertedAccent
            }
        }
    }
    
    class navigation {
        class var background: UIColor {
            return ThemeManager.shared.colorProvider.navigationBackground
        }
        class var text: UIColor {
            return ThemeManager.shared.colorProvider.navigationText
        }
    }
    
    class tabBar {
        class var background: UIColor {
            return ThemeManager.shared.colorProvider.tabBarBackground
        }
        class var selectedItem: UIColor {
            return ThemeManager.shared.colorProvider.tabBarSelectedItem
        }
        class var unselectedItem: UIColor {
            return ThemeManager.shared.colorProvider.tabBarUnselectedItem
        }
    }
    
    class shadow {
        class var standart: UIColor {
            return ThemeManager.shared.colorProvider.shadow
        }
    }

    class loading {
        class var activityIndicator: UIColor {
            return ThemeManager.shared.colorProvider.activityIndicator
        }
        
        class progressBar: UIColor {
            class var loaded: UIColor {
                return ThemeManager.shared.colorProvider.progressBarLoaded
            }
            class var inProgress: UIColor {
                return ThemeManager.shared.colorProvider.progressBarInProgress
            }
        }
    }

}
