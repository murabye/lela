//
//  ColorProvider.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

protocol ColorProvider {
    var main: UIColor { get }
    var text: UIColor { get }
    var textDisabled: UIColor { get }
    var textSubtitle: UIColor { get }
    var textImportant: UIColor { get }
    var textInverted: UIColor { get }
    var textInvertedDisabled: UIColor { get }
    var textInvertedSubtitle: UIColor { get }

    var buttonBackground: UIColor { get }
    var buttonText: UIColor { get }
    var buttonDisabledBackground: UIColor { get }
    var buttonDisabledText: UIColor { get }
    var buttonHighlightedBackground: UIColor { get }
    var buttonHighlightedText: UIColor { get }
    var buttonInvertedBackground: UIColor { get }
    var buttonInvertedText: UIColor { get }
    var buttonInvertedDisabledBackground: UIColor { get }
    var buttonInvertedDisabledText: UIColor { get }
    var buttonInvertedHighlightedBackground: UIColor { get }
    var buttonInvertedHighlightedText: UIColor { get }

    var appBackground: UIColor { get }
    
    var viewBackground: UIColor { get }
    var viewAccent: UIColor { get }
    var viewInvertedBackground: UIColor { get }
    var viewInvertedAccent: UIColor { get }

    var navigationBackground: UIColor { get }
    var navigationText: UIColor { get }

    var tabBarBackground: UIColor { get }
    var tabBarUnselectedItem: UIColor { get }
    var tabBarSelectedItem: UIColor { get }

    var shadow: UIColor { get }
    
    var activityIndicator: UIColor { get }
    var progressBarLoaded: UIColor { get }
    var progressBarInProgress: UIColor { get }
}
