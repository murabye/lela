//
// SiburColorProvider.swift
// Lela
//
// Created by варя on 19/05/2019.
// Copyright © 2019 варя. All rights reserved.
//

import UIKit

class SiburColorProvider: ColorProvider {
    static private var iMain: UIColor = UIColor(named: "sibur_main")!
    var main: UIColor = UIColor(named: "sibur_main")!

    var text: UIColor = .black
    var textDisabled: UIColor = .gray
    var textSubtitle: UIColor = .darkGray
    var textImportant: UIColor = SiburColorProvider.iMain
    var textInverted: UIColor = .white
    var textInvertedDisabled: UIColor = UIColor.blend(color1: .white, color2: SiburColorProvider.iMain)
    var textInvertedSubtitle: UIColor = UIColor.blend(color1: .white, intensity1: 0.75, color2: SiburColorProvider.iMain, intensity2: 0.25)
    
    var buttonBackground: UIColor = SiburColorProvider.iMain
    var buttonText: UIColor = .white
    var buttonDisabledBackground: UIColor = .gray
    var buttonDisabledText: UIColor = .white
    var buttonHighlightedBackground: UIColor = .white
    var buttonHighlightedText: UIColor = .white
    var buttonInvertedBackground: UIColor = .white
    var buttonInvertedText: UIColor = .black
    var buttonInvertedDisabledBackground: UIColor = .white
    var buttonInvertedDisabledText: UIColor = .gray
    var buttonInvertedHighlightedBackground: UIColor = .white
    var buttonInvertedHighlightedText: UIColor = .white
    
    var appBackground: UIColor = UIColor(named: "sibur_app_bg")!
    
    var viewBackground: UIColor = .white
    var viewAccent: UIColor = SiburColorProvider.iMain
    var viewInvertedBackground: UIColor = SiburColorProvider.iMain
    var viewInvertedAccent: UIColor = .white
    
    var navigationBackground: UIColor = .white
    var navigationText: UIColor =  SiburColorProvider.iMain
    
    var tabBarBackground: UIColor = SiburColorProvider.iMain
    var tabBarUnselectedItem: UIColor = UIColor.blend(color1: .white, color2: SiburColorProvider.iMain)
    var tabBarSelectedItem: UIColor = .white
    
    var shadow: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    
    var activityIndicator: UIColor = SiburColorProvider.iMain
    var progressBarLoaded: UIColor = SiburColorProvider.iMain
    var progressBarInProgress: UIColor = .gray

}
