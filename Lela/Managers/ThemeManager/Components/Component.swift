//
//  Component.swift
//  Lela
//
//  Created by Вова Петров on 15.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SwiftEntryKit

class Component {
    static var textField: CustomTextField {
        return ThemeManager.shared.componentProvider.textField
    }
    
    static var textView: KMPlaceholderTextView {
        return ThemeManager.shared.componentProvider.textView
    }
    
    static var button: ThemeManager.MainThemeButton {
        return ThemeManager.shared.componentProvider.button
    }
    
    static var invertedButton: ThemeManager.MainThemeButton {
        return ThemeManager.shared.componentProvider.invertedButton
    }
    
    static var helpButton: UIButton {
        return ThemeManager.shared.componentProvider.helpButton
    }
    
    static var accentHelpButton: UIButton {
        return ThemeManager.shared.componentProvider.accentHelpButton
    }

    static var roundButton: UIButton {
        return ThemeManager.shared.componentProvider.roundButton
    }
    
    static var invertedRoundButton: UIButton {
        return ThemeManager.shared.componentProvider.invertedRoundButton
    }
    
    static var headerLogo: UIView {
        return ThemeManager.shared.componentProvider.headerLogo
    }
    
    static var headerPageName: UILabel {
        return ThemeManager.shared.componentProvider.headerPageName
    }
    
    static func pickerTextField<T>(with rows: [T], block: @escaping (T) -> String) ->  (textfield: CustomTextField, delegate: PickerViewDelegate<T>) {
        return ThemeManager.shared.componentProvider.pickerTextField(with: rows, block: block)
    }
    
    static func dateTextField() ->  (textfield: CustomTextField, delegate: DatePickerDelegate) {
        return ThemeManager.shared.componentProvider.dateTextField()
    }
    
    static func label(text: String?, weight: UIFont.Weight?, size: CGFloat?) -> UILabel {
        return ThemeManager.shared.componentProvider.label(text: text, weight: weight, size: size)
    }
}


extension UIViewController {
    func setupFormPresets() -> EKAttributes {
        var attributes: EKAttributes

        attributes = .float
        attributes.windowLevel = .normal
        attributes.position = .top
        attributes.displayDuration = .infinity
        attributes.entranceAnimation = .init(
            translate: .init(
                duration: 0.65,
                anchorPosition: .top,
                spring: .init(damping: 1, initialVelocity: 0)
            )
        )
        attributes.exitAnimation = .init(
            translate: .init(
                duration: 0.65,
                anchorPosition: .top,
                spring: .init(damping: 1, initialVelocity: 0)
            )
        )
        attributes.popBehavior = .animated(
            animation: .init(
                translate: .init(
                    duration: 0.65,
                    spring: .init(damping: 1, initialVelocity: 0)
                )
            )
        )
        attributes.entryInteraction = .absorbTouches
        attributes.screenInteraction = .dismiss
        attributes.entryBackground = .color(color: EKColor.standardBackground)
        attributes.screenBackground = .color(color: EKColor.standardBackground.with(alpha: 0.5))
        attributes.border = .value(
            color: UIColor(white: 0.6, alpha: 1),
            width: 1
        )
        attributes.shadow = .active(
            with: .init(
                color: .black,
                opacity: 0.3,
                radius: 3
            )
        )
        attributes.scroll = .enabled(
            swipeable: true,
            pullbackAnimation: .jolt
        )
        attributes.statusBar = .currentStatusBar
        attributes.positionConstraints.keyboardRelation = .bind(
            offset: .init(
                bottom: 15,
                screenEdgeResistance: 0
            )
        )
        attributes.positionConstraints.maxSize = .init(
            width: .constant(value: UIScreen.main.minEdge),
            height: .intrinsic
        )
        return attributes
    }
}
