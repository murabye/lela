//
//  TextField.swift
//  Lela
//
//  Created by Вова Петров on 15.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

protocol ComponentProvider {
    var textField: CustomTextField { get }
    var textView: KMPlaceholderTextView { get }
    var button: ThemeManager.MainThemeButton { get }
    var invertedButton: ThemeManager.MainThemeButton { get }
    var helpButton: UIButton { get }
    var accentHelpButton: UIButton { get }
    var roundButton: UIButton { get }
    var invertedRoundButton: UIButton { get }
    var headerLogo: UIView { get }
    var headerPageName: UILabel { get }
    func pickerTextField<T>(with rows: [T], block: @escaping (T) -> String) ->  (textfield: CustomTextField, delegate: PickerViewDelegate<T>)
    func dateTextField() -> (textfield: CustomTextField, delegate: DatePickerDelegate)
    func label(text: String?, weight: UIFont.Weight?, size: CGFloat?) -> UILabel
}

extension ThemeManager {
    class MainThemeButton: UIButton {
        func show(loading: Bool) {}
    }
}
