//
//  SiburComponentProvider.swift
//  Lela
//
//  Created by Вова Петров on 15.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import Hero
import RxSwift
import RxCocoa

fileprivate var Constants = (
    activityContainerViewTag: 90909,
    variableForCreatingTuple: 1
)

class SiburComponentProvider: ComponentProvider {
    func label(text: String?, weight: UIFont.Weight?, size: CGFloat?) -> UILabel {
        let workWeight = weight ?? .light
        let workSize = size ?? 16
        
        let desc = UILabel()
        desc.font = UIFont.systemFont(ofSize: workSize, weight: workWeight)
        desc.text = text
        desc.numberOfLines = 0
        desc.textColor = UIColor.text.standart.standart
        return desc
    }
    
    var textField: CustomTextField {
        get {
            let textField = CustomTextField()
            textField.tintColor = UIColor.view.standart.accent
            textField.backgroundColor = UIColor.view.standart.background
            textField.layer.cornerRadius = 20
            textField.clipsToBounds = true
            textField.layer.borderWidth = 1.0
            textField.layer.borderColor = UIColor.view.standart.accent.cgColor
            textField.padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            return textField
        }
    }
        
    var textView: KMPlaceholderTextView {
        get {
            let textView = KMPlaceholderTextView()
            textView.tintColor = UIColor.view.standart.accent
            textView.layer.cornerRadius = 20
            textView.backgroundColor = UIColor.view.standart.background
            textView.clipsToBounds = true
            textView.layer.borderWidth = 1.0
            textView.layer.borderColor = UIColor.view.standart.accent.cgColor
            textView.textContainerInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            textView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            textView.isScrollEnabled = true
            textView.placeholderColor = UIColor.text.standart.subtitle
            return textView
        }
    }

    var button: ThemeManager.MainThemeButton {
        get {
            let button = SiburButton()
            button.layer.cornerRadius = 20
            button.clipsToBounds = true
            button.layer.backgroundColor = UIColor.button.standart.standart.background.cgColor
            button.setTitleColor(UIColor.button.standart.standart.text, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
            button.layer.shadowColor = UIColor.shadow.standart.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
            button.layer.shadowRadius = 5
            button.layer.shadowOpacity = 1
            button.layer.masksToBounds = false
            return button
        }
    }
    
    var invertedButton: ThemeManager.MainThemeButton {
        get {
            let button = SiburButton()
            button.layer.cornerRadius = 20
            button.clipsToBounds = true
            button.layer.backgroundColor = UIColor.button.inverted.standart.background.cgColor
            button.setTitleColor(UIColor.button.inverted.standart.text, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
            button.layer.shadowColor = UIColor.shadow.standart.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
            button.layer.shadowRadius = 5
            button.layer.shadowOpacity = 1
            button.layer.masksToBounds = false
            return button
        }
    }

    var helpButton: UIButton {
        get {
            let button = UIButton()
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .thin)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
            button.layer.shadowOpacity = 1
            button.layer.shadowRadius = 0
            button.layer.masksToBounds = false
            button.tintColor = UIColor.button.inverted.highlighted.background
            button.layer.backgroundColor = UIColor.button.inverted.standart.background.cgColor
            button.setBackgroundImage(UIImage.fromColor(color: UIColor.button.inverted.standart.background), for: .normal)
            button.setBackgroundImage(UIImage.fromColor(color: UIColor.button.inverted.disabled.background), for: .disabled)
            button.setBackgroundImage(UIImage.fromColor(color: UIColor.button.inverted.highlighted.background), for: .highlighted)
            button.setTitleColor(UIColor.button.inverted.standart.text, for: .normal)
            button.setTitleColor(UIColor.button.standart.disabled.text, for: .disabled)
            button.setTitleColor(UIColor.button.standart.highlighted.text, for: .highlighted)
            return button
        }
    }
    
    var accentHelpButton: UIButton {
        get {
            let button = UIButton()
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .thin)
            let color = UIColor.button.standart.standart.background

            button.tintColor = color
            button.setTitleColor(color, for: .normal)
            button.setTitleColor(color, for: .disabled)
            button.setTitleColor(color, for: .highlighted)
            button.adjustsImageWhenHighlighted = false
            
            button.layer.shadowColor = color.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
            button.layer.shadowOpacity = 1
            button.layer.shadowRadius = 0
            button.layer.masksToBounds = false
            
            let appColor = UIColor.app.background
            button.layer.backgroundColor = appColor.cgColor
            button.setBackgroundImage(UIImage.fromColor(color: appColor), for: .normal)
            button.setBackgroundImage(UIImage.fromColor(color: appColor), for: .disabled)
            button.setBackgroundImage(UIImage.fromColor(color: appColor), for: .highlighted)
            
            return button
        }
    }
    
    var roundButton: UIButton {
        get {
            let button = UIButton()
            button.layer.cornerRadius = 22
            button.backgroundColor = UIColor.button.inverted.standart.background
            button.tintColor = UIColor.button.standart.standart.background
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor.button.standart.standart.background.cgColor
            button.widthAnchor.constraint(equalToConstant: 44).isActive = true
            button.heightAnchor.constraint(equalToConstant: 44).isActive = true
            return button
        }
    }

    var invertedRoundButton: UIButton {
        get {
            let button = roundButton
            button.backgroundColor = UIColor.button.standart.standart.background
            button.tintColor = UIColor.button.standart.standart.text
            button.widthAnchor.constraint(equalToConstant: 44).isActive = true
            button.heightAnchor.constraint(equalToConstant: 44).isActive = true
            return button
        }
    }
    
    var headerLogo: UIView {
        get {
            let imageView = UIImageView()
            imageView.image = UIImage.logo
            imageView.contentMode = .scaleAspectFit
            imageView.hero.id = "LogoImage"
            
            return imageView
        }
    }

    var headerPageName: UILabel {
        let label = UILabel()
        label.textColor = UIColor.text.standart.standart
        label.font = UIFont.systemFont(ofSize: 28.0, weight: .thin)
        label.textAlignment = .center

        return label
    }
    
    func pickerTextField<T>(with rows: [T], block: @escaping (T) -> String) ->  (textfield: CustomTextField, delegate: PickerViewDelegate<T>)  {
        let textField = CustomTextField()
        textField.tintColor = UIColor.clear
        textField.backgroundColor = UIColor.view.standart.background
        textField.layer.cornerRadius = 20
        textField.clipsToBounds = true
        textField.layer.borderWidth = 1.0
        textField.layer.borderColor = UIColor.view.standart.accent.cgColor
        textField.padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        textField.isCopyEnabled = false
        textField.isPasteEnabled = false
        textField.isCutEnabled = false
        textField.isDeleteEnabled = false
                
        let pickerView = UIPickerView()
        textField.inputView = pickerView
        let delegate = PickerViewDelegate(with: rows, block: block)
        pickerView.delegate = delegate
        delegate.textField = textField
        
        return (textField, delegate)
    }
    
    func dateTextField() -> (textfield: CustomTextField, delegate: DatePickerDelegate) {
        let textField = CustomTextField()
        textField.tintColor = UIColor.clear
        textField.backgroundColor = UIColor.view.standart.background
        textField.layer.cornerRadius = 20
        textField.clipsToBounds = true
        textField.layer.borderWidth = 1.0
        textField.layer.borderColor = UIColor.view.standart.accent.cgColor
        textField.padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        textField.isCopyEnabled = false
        textField.isPasteEnabled = false
        textField.isCutEnabled = false
        textField.isDeleteEnabled = false
                
        let delegate = DatePickerDelegate()
        delegate.setInputViewDatePicker(to: textField)
        
        return (textField, delegate)

    }
}

fileprivate class SiburButton: ThemeManager.MainThemeButton {
    var _isEnabled = true
    override public var isEnabled: Bool {
        get {
            return _isEnabled
        }
        set {
            _isEnabled = newValue
            if newValue {
                layer.backgroundColor = UIColor.button.standart.standart.background.cgColor
            } else {
                layer.backgroundColor = UIColor.button.standart.disabled.background.cgColor
            }
        }
    }
    
    override func show(loading: Bool) {
        if loading {
            self.backgroundColor = .clear
            self.setTitleColor(.clear, for: .normal)

            let activityView = addActivityView()
            self.layoutIfNeeded()
            
            activityView.snp.remakeConstraints { (make) in
                make.width.equalTo(44)
                make.height.equalTo(44)
                make.center.equalToSuperview()
            }
            
            UIView.animate(withDuration: 0.25) { [weak self] in
                guard let self = self else { return }
                self.layoutIfNeeded()
            }
        } else {
            self.layoutIfNeeded()
            guard let container = self.subviews
                .first(where: { $0.tag == Constants.activityContainerViewTag })
                else {
                    self.backgroundColor = UIColor.button.standart.standart.background
                    self.setTitleColor(UIColor.button.standart.standart.text, for: .normal)
                    self.layoutIfNeeded()
                    return
            }
            
            container.snp.remakeConstraints { make in
                make.edges.equalToSuperview()
                make.center.equalToSuperview()
            }
            
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                guard let self = self else { return }
                self.layoutIfNeeded()
            }) { [weak self] _ in
                guard let self = self else { return }
                self.backgroundColor = UIColor.button.standart.standart.background
                self.setTitleColor(UIColor.button.standart.standart.text, for: .normal)
                container.removeFromSuperview()
                self.layoutIfNeeded()
            }
        }
    }
    
    fileprivate func addActivityView() -> UIView {
        let activityContainerView = UIView()
        activityContainerView.tag = Constants.activityContainerViewTag
        activityContainerView.clipsToBounds = true
        activityContainerView.layer.masksToBounds = false
        activityContainerView.layer.cornerRadius = 20.0
        self.addSubview(activityContainerView)
        
        let activityIndicator = UIActivityIndicatorView()
        activityContainerView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        activityContainerView.backgroundColor = UIColor.button.standart.standart.background
        activityContainerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        return activityContainerView
    }

    
}
