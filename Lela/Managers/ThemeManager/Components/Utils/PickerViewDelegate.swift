//
//  PickerViewDelegate.swift
//  Lela
//
//  Created by Влада Кузнецова on 29/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PickerViewDelegate<T>: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var values: [T]
    var block: (T) -> String
    let relay = ReplaySubject<T>.create(bufferSize: 1)

    weak var textField: UITextField?
    
    init(with values: [T], block: @escaping (T) -> String) {
        self.values = values
        self.block = block
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        values.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return block(values[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let textField = self.textField else { return }
        relay.onNext(values[row])
        textField.rx.text.onNext(block(values[row]))
    }
    
}
