//
//  DatePickerViewDelegate.swift
//  Lela
//
//  Created by Влада Кузнецова on 04.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class DatePickerDelegate {
    let relay = ReplaySubject<Date>.create(bufferSize: 1)
    weak var textField: UITextField?
    weak var datePicker: UIDatePicker? {
        get {
            return textField?.inputView as? UIDatePicker
        }
    }
    
    func setInputViewDatePicker(to textField: UITextField) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(changeValue), for: UIControl.Event.valueChanged)
        textField.inputView = datePicker
        self.textField = textField
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(tapCancel))
        let barButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(tapDone))
        toolBar.setItems([cancel, flexible, barButton], animated: false)
        textField.inputAccessoryView = toolBar
    }
    
    @objc func tapCancel() {
        textField?.resignFirstResponder()
    }
    
    @objc func tapDone() {
        if let datePicker = self.datePicker {
            textField?.text = datePicker.date.string(with: .date)
        }
        textField?.resignFirstResponder()
    }

    @objc func changeValue() {
        guard let datePicker = self.datePicker else { return }
        textField?.text = datePicker.date.string(with: .date)
    }
}
