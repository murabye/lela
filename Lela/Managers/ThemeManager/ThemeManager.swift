//
//  ThemeManager.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

class ThemeManager {
    var colorProvider: ColorProvider
    var imageProvider: ImageProvider
    var componentProvider: ComponentProvider

    static let shared: ThemeManager = ThemeManager()
    private init() {
        colorProvider = SiburColorProvider()
        imageProvider = SiburImageProvider()
        componentProvider = SiburComponentProvider()
    }
    
    func update() {}
}
