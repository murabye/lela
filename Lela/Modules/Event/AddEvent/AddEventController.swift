//
//  AddEventController.swift
//  Lela
//
//  Created by Вова Петров on 13.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import Hero
import SwiftEntryKit

extension Module.Event.Add {
    class Controller: ViewController, ControllerType {

        typealias ViewModelType = ViewModel
        
        // MARK: - Properties
        private var viewModel: ViewModelType!
        private let disposeBag = DisposeBag()
        
        var cancelButton: UIButton = {
            let cancelButton = UIButton()
            cancelButton.setImage(Assets.Icon.cancel.image, for: .normal)
            cancelButton.layer.cornerRadius = 22
            cancelButton.hero.id = "AddButton"
            return cancelButton
        }()
        
        var contentView: ContentView = {
            return ContentView()
                .configure(with: [])
        }()
        
        var fakeAddButton: UIButton = {
            let fakeAddButton = UIButton()
            fakeAddButton.setImage(Assets.Icon.plus.image, for: .normal)
            fakeAddButton.layer.cornerRadius = 22
            fakeAddButton.hero.id = "AddButton"
            return fakeAddButton
        }()
        
        let logo = Component.headerLogo
        
        // MARK: - Lifecycle
        override func viewDidLoad() {
            modalTransitionStyle = .flipHorizontal
            modalPresentationStyle = .overFullScreen
            super.viewDidLoad()
            
            configureView()
            configure(with: viewModel)
        }
    }
}

extension Module.Event.Add.Controller: Configurable {
    func configureView() {
        self.view.backgroundColor = UIColor.app.background
        
        view.addSubview(logo)
        
        let logoImgSize = UIImage.logo.size
        
        logo.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(logo.snp.height).multipliedBy(logoImgSize.width / logoImgSize.height)
            make.width.equalTo(150)
        }
        
        self.view.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().offset(-16).priority(750)
            make.width.lessThanOrEqualTo(500)
            make.bottom.equalTo(self.view.snp.bottomMargin).offset(-16)
            make.top.equalTo(logo.snp.bottom).offset(20)
        }
        
        self.view.addSubview(fakeAddButton)
        self.view.addSubview(cancelButton)
        cancelButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(44)
            make.top.equalTo(logo.snp.bottom).offset(28)
            make.right.equalToSuperview().offset(-15)
        }
        fakeAddButton.snp.makeConstraints { (make) in
            make.edges.equalTo(cancelButton).inset(6)
        }
    }
}

extension Module.Event.Add.Controller {
    func configure(with viewModel: ViewModelType) {
        viewModel.output.categoryList.subscribe(onNext: { [weak self] categories in
            guard let self = self else { return }
            self.contentView = self.contentView.configure(with: categories)
            }).disposed(by: disposeBag)
        
        cancelButton.rx.tap
            .bind(to: viewModel.input.cancelEvent)
            .disposed(by: disposeBag)
        
        viewModel.output.closeModule.subscribe {[weak self] (_) in
            self?.hero.isEnabled = true
            self?.contentView.hero.isEnabled = true
            self?.contentView.hero.id = "AddView"
            self?.hero.isEnabled = true
            self?.dismiss(animated: true, completion: nil)
        }
        .disposed(by: disposeBag)
        
        viewModel.output.errorCreate.subscribe(onNext: { [weak self] errorStr in
            self?.showAlert(title: "Что–то пошло не так", text: errorStr)
            self?.contentView.createButton.show(loading: false)
            }).disposed(by: disposeBag)
        
        viewModel.output.successCreate.subscribe(onNext: { [weak self] successStr in
            
            let title = "Успешно!"
            let description = "Создание события " + successStr + " прошло успешно!"
            self?.showPopupMessage(title: title,
                                  description: description,
                                  action: {[weak self] in
                                      self?.dismiss(animated: true, completion: nil)
                                  })
            
            
        }).disposed(by: disposeBag)
        
        contentView.delegate?.relay
            .asObservable()
            .subscribe(viewModel.input.category)
            .disposed(by: disposeBag)
        
        contentView.createButton
            .rx.tap.subscribe(onNext: { [unowned self] _ in
                viewModel.input.create.onNext(())
                self.contentView.createButton.show(loading: true)
            })
            .disposed(by: disposeBag)
        
        contentView.nameField
            .rx.text.orEmpty.subscribe(viewModel.input.title)
            .disposed(by: disposeBag)
        
        contentView.descriptionTextView
            .rx.text.orEmpty.subscribe(viewModel.input.description)
            .disposed(by: disposeBag)
        
        contentView.minField
            .rx.text.orEmpty
            .map { Int($0) }
            .ignoreNil()
            .subscribe(viewModel.input.minPerson)
            .disposed(by: disposeBag)
        
        contentView.maxField
            .rx.text.orEmpty
            .map { Int($0) }
            .ignoreNil()
            .subscribe(viewModel.input.maxPerson)
            .disposed(by: disposeBag)

    }
}

extension Module.Event.Add.Controller {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = Module.Event.Add.Controller()
        controller.viewModel = viewModel
        return controller
    }
}
