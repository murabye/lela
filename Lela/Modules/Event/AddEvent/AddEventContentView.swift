//
//  AddEventContentView.swift
//  Lela
//
//  Created by Вова Петров on 14.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension Module.Event.Add {
    class ContentView: UIView {
        var delegate: PickerViewDelegate<Models.EventCategory>?
        
        var titleLabel: UILabel = {
            let titleLabel = UILabel()
            titleLabel.text = "создать\nмероприятие"
            titleLabel.numberOfLines = 2
            titleLabel.textColor = UIColor.text.inverted.standart
            titleLabel.font = UIFont.systemFont(ofSize: 28.0, weight: .semibold)
            titleLabel.textAlignment = .center
            
            return titleLabel
        }()
        
        var nameField = Component.textField
        var descriptionTextView: UITextView = {
            let textView = UITextView()
            textView.backgroundColor = .white
            textView.layer.cornerRadius = 16
            textView.isScrollEnabled = false
            textView.font = .systemFont(ofSize: 17)
            return textView
        }()
        
        var categoryLabel: UILabel = {
            let label = UILabel()
            label.font = .systemFont(ofSize: 17)
            return label
        }()
        
        var maxField = Component.textField
        var minField = Component.textField
        
        var createButton: ThemeManager.MainThemeButton = {
            let button = Component.invertedButton
            button.setTitle("Создать", for: .normal)
            return button
        }()
        
        func makeLabel(text: String) -> UILabel {
            let label = UILabel()
            label.textColor = UIColor.text.inverted.standart
            label.text = text
            return label
        }
        
        func configure(with categoryList: [Models.EventCategory]) -> ContentView {
            self.subviews.forEach { $0.removeFromSuperview() }
            let scrollView = UIScrollView()
            scrollView.translatesAutoresizingMaskIntoConstraints = true
            scrollView.keyboardDismissMode = .onDrag
            scrollView.alwaysBounceVertical = true
            addSubview(scrollView)
            scrollView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            self.layer.cornerRadius = 26
            self.backgroundColor = UIColor.view.inverted.background
            self.hero.id = "AddView"
            
            scrollView.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.snp.centerX)
                make.top.equalToSuperview().offset(20)
            }
            
            let nameLabel = makeLabel(text: "Название:")
            scrollView.addSubview(nameLabel)
            nameLabel.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(titleLabel.snp.bottom).offset(20)
            }
            
            nameField.placeholder = "Введите название"
            scrollView.addSubview(nameField)
            nameField.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(15)
                make.width.equalTo(self.snp.width).inset(15)
                make.top.equalTo(nameLabel.snp.bottom).offset(8)
                make.height.greaterThanOrEqualTo(44)
            }
            
            let descriptionLabel = makeLabel(text: "Описание:")
            scrollView.addSubview(descriptionLabel)
            descriptionLabel.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(nameField.snp.bottom).offset(15)
            }
            
            scrollView.addSubview(descriptionTextView)
            descriptionTextView.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(15)
                make.width.equalTo(self.snp.width).inset(15)
                make.height.greaterThanOrEqualTo(54)
                make.top.equalTo(descriptionLabel.snp.bottom).offset(8)
            }
            
            let categoryTitleLabel = makeLabel(text: "Категория:")
            
            scrollView.addSubview(categoryTitleLabel)
            categoryTitleLabel.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(descriptionTextView.snp.bottom).offset(15)
            }
            
            let categoryCarthage = Component.pickerTextField(with: categoryList) { $0.title }
            if let del = self.delegate,
                let inputView = categoryCarthage.textfield.inputView as? UIPickerView {
                del.textField = categoryCarthage.textfield
                del.values = categoryList
                del.block = { $0.title }
                inputView.delegate = del
            } else {
                self.delegate = categoryCarthage.delegate
            }
            let categoryView = categoryCarthage.textfield
            
            scrollView.addSubview(categoryView)
            categoryView.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(15)
                make.width.equalTo(self.snp.width).inset(15)
                make.height.greaterThanOrEqualTo(50)
                make.top.equalTo(categoryTitleLabel.snp.bottom).offset(8)
            }
            
            categoryView.addSubview(categoryLabel)
            categoryLabel.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            let minPersonLabel = makeLabel(text: "Мин. участников:")
            scrollView.addSubview(minPersonLabel)
            minPersonLabel.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(categoryView.snp.bottom).offset(15)
                make.width.equalTo(self.snp.width).dividedBy(2).offset(-15)
            }
            
            let maxPersonLabel = makeLabel(text: "Макс. участников:")
            maxPersonLabel.textAlignment = .right
            scrollView.addSubview(maxPersonLabel)
            maxPersonLabel.snp.makeConstraints { (make) in
                make.right.equalTo(descriptionTextView.snp.right).inset(10)
                make.top.equalTo(categoryView.snp.bottom).offset(15)
                make.width.equalTo(self.snp.width).dividedBy(2).offset(-15)
            }
            
            minField.placeholder = "Участников"
            minField.keyboardType = .numberPad
            scrollView.addSubview(minField)
            minField.snp.makeConstraints { (make) in
                make.left.equalTo(minPersonLabel.snp.left).inset(-8)
                make.width.equalTo(minPersonLabel).inset(12)
                make.top.equalTo(minPersonLabel.snp.bottom).offset(8)
                make.height.greaterThanOrEqualTo(44)
            }
            
            maxField.placeholder = "Участников"
            maxField.keyboardType = .numberPad
            scrollView.addSubview(maxField)
            maxField.snp.makeConstraints { (make) in
                make.right.equalTo(maxPersonLabel.snp.right).offset(8)
                make.width.equalTo(maxPersonLabel).inset(12)
                make.top.equalTo(maxPersonLabel.snp.bottom).offset(8)
                make.height.greaterThanOrEqualTo(44)
            }
            
            scrollView.addSubview(createButton)
            createButton.snp.makeConstraints { (make) in
                make.left.right.equalTo(self).inset(20)
                make.top.equalTo(minField.snp.bottom).offset(30)
                make.height.equalTo(44)
                make.bottom.equalToSuperview().inset(10).priority(.low)
            }
            
            return self
        }
    }
}
