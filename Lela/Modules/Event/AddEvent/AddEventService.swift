//
//  AddEventService.swift
//  Lela
//
//  Created by Влада Кузнецова on 01/12/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol AddEventServiceProtocol {
    func create(event: Models.Event) -> Observable<Models.Event>
    func categories() -> Observable<[Models.EventCategory]>
}

extension Module.Event.Add {
    class Service: AddEventServiceProtocol {
        let addEventRepo = Repository()
        let getCategoriesRepo = EventListRepository()
        
        func create(event: Models.Event) -> Observable<Models.Event> {
            return addEventRepo.create(event: event)
        }
        
        func categories() -> Observable<[Models.EventCategory]> {
            return getCategoriesRepo.getEvents()
        }
    }
}

/*
 protocol AddEventRepositoryProtocol {
     func create(event: Models.Event) -> Observable<Models.Event>
 }

 extension Module.Event.Add {
     class Repository: AddEventRepositoryProtocol {
         func create(event: Models.Event) -> Observable<Models.Event> {
             return NetworkManager.requestObservable(from: AdressesProvider.createEvent.value(),
                                                     method: .post,
                                                     body: event)
         }
     }
 }

 */
