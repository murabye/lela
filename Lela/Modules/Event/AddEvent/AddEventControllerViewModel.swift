//
//  AddEventControllerViewModel.swift
//  Lela
//
//  Created by Вова Петров on 13.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import RxSwift

class AddEventControllerViewModel: ViewModelProtocol {
    struct Input {
        let cancelEvent: AnyObserver<Void>
        
        let title: AnyObserver<String>
        let description: AnyObserver<String>
        let category: AnyObserver<Models.EventCategory>
        let minPerson: AnyObserver<Int>
        let maxPerson: AnyObserver<Int>
        
        let create: AnyObserver<Void>
    }
    struct Output {
        let closeModule: Observable<Void>
        let successCreate: Observable<String>
        let errorCreate: Observable<String>
    }
    // MARK: - Public properties
    let input: Input
    let output: Output
    
    // MARK: - Private properties
    private let titleSubject            = PublishSubject<String>()
    private let descriptionSubject      = PublishSubject<String>()
    private let categorySubject         = PublishSubject<Models.EventCategory>()
    private let minPersonSubject        = PublishSubject<Int>()
    private let maxPersonSubject        = PublishSubject<Int>()
    private let createSubject           = PublishSubject<Void>()

    private let errorsSubject           = PublishSubject<String>()
    private let closeModuleSubject      = PublishSubject<Void>()
    private let categoryArray           = PublishSubject<[Models.EventCategory]>()
    private let successCreateSubject    = PublishSubject<String>()
    
    private let disposeBag = DisposeBag()
    
    private var eventObservable: Observable<Models.Event> {
        return Observable.combineLatest(titleSubject.asObservable(), descriptionSubject.asObservable(), categorySubject.asObservable(), minPersonSubject.asObservable(), maxPersonSubject.asObservable()) { (title, description, category, min, max) in
            let note = Models.Note(number: 0, theme: nil, text: description, isAnonimus: false, personId: UserService.currentUser?.id, person: UserService.currentUser, time: Date().getString(with: .server), attachmentId: nil)
            return Models.Event(title: title, points: 0, maxPersons: max, periodArray: [], categoryId: category.id, ownerId: UserService.currentUser!.id, partArray: [], noteArray: [note])
        }
    }
    
    // MARK: - Init and deinit
    init(_ eventCreateRepository: AddEventRepositoryProtocol) {
        
        input = Input(cancelEvent: closeModuleSubject.asObserver(),
                      title: titleSubject.asObserver(),
                      description: descriptionSubject.asObserver(),
                      category: categorySubject.asObserver(),
                      minPerson: minPersonSubject.asObserver(),
                      maxPerson: maxPersonSubject.asObserver(),
                      create: createSubject.asObserver())
        
        output = Output(closeModule: closeModuleSubject.asObservable(),
                        successCreate: successCreateSubject.asObservable(),
                        errorCreate: errorsSubject.asObservable())
        
        createSubject.withLatestFrom(eventObservable)
            .subscribe(onNext: { event in
                eventCreateRepository.create(event: event)
                    .subscribe(onNext: { [weak self] (event) in
                        self?.successCreateSubject.onNext("Успешно!")
                    },
                               onError: { [weak self] (error) in
                                self?.errorsSubject.onNext(error.localizedDescription)
                    }).disposed(by: self.disposeBag)
            }).disposed(by: self.disposeBag)
    }
    
    deinit {
        print("\(self) dealloc")
    }
}
