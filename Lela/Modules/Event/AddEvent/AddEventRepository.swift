//
//  AddEventRepository.swift
//  Lela
//
//  Created by Вова Петров on 13.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol AddEventRepositoryProtocol {
    func create(event: Models.Event) -> Observable<Models.Event>
}

extension Module.Event.Add {
    class Repository: AddEventRepositoryProtocol {

        func create(event: Models.Event) -> Observable<Models.Event> {
            return NetworkManager
                .requestObservable(from: AdressesProvider.createEvent.value(),
                                                    method: .post,
                                                    body: event)
                .do(afterNext: { (_) in
                    Radio.EventList.Say.needUpdate()
                })
        }
    }
}
