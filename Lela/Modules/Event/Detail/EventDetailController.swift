//
//  EventDetailViewController.swift
//  Lela
//
//  Created by Влада Кузнецова on 16/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import Hero

extension Module.Event.Detail {
    class Controller: ViewController, ControllerType {
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!
        let db = DisposeBag()
        
        let scrollView = UIScrollView()
        
        var cancelButton: UIButton = {
            let cancelButton = UIButton()
            cancelButton.backgroundColor = UIColor.button.inverted.standart.background
            cancelButton.setImage(Assets.Icon.cancel.image, for: .normal)
            cancelButton.tintColor = UIColor.button.inverted.standart.text
            cancelButton.layer.cornerRadius = 22
            return cancelButton
        }()
        
        var mainStack: UIStackView = {
            let stack = UIStackView()
            stack.axis = .vertical
            stack.distribution = .equalSpacing
            stack.alignment = .center
            return stack
        }()
        
        var detailView = DetailView.create()
        
        override func viewDidLoad() {
            modalPresentationStyle = .overFullScreen
            super.viewDidLoad()
            setupView()
        }
        
        func configure(with viewModel: ViewModel) {
            
            cancelButton.rx.tap
                .bind(to: viewModel.input.backAction)
                .disposed(by: db)
            
            viewModel.output.event
                .subscribe(onNext: {[weak self] (event) in
                self?.detailView.configureData(with: event)
            })
            .disposed(by: db)
            
            viewModel.output.closeModule
                .subscribe(onNext: {[weak self] (_) in
                    self?.dismiss(animated: true, completion: nil)
                })
            .disposed(by: db)
        }
            
        func setupView() {
            let closeRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gr:)))
            view.addGestureRecognizer(closeRecognizer)
            view.backgroundColor = UIColor.app.background
            view.addSubview(visualEffectView)
            visualEffectView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            view.addSubview(scrollView)
            scrollView.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.top.equalTo(self.view.snp.topMargin)
            }
            
            view.addSubview(cancelButton)
            cancelButton.snp.makeConstraints { (make) in
                make.height.width.equalTo(44)
                make.top.equalTo(self.view.snp.topMargin)
                make.right.equalToSuperview().offset(-15)
            }
            
            scrollView.addSubview(mainStack)
            mainStack.snp.makeConstraints { make in
                make.edges.equalToSuperview()
                make.width.equalTo(self.view.snp.width)
            }
            
            mainStack.addArrangedSubview(detailView)
            detailView.snp.makeConstraints { (make) in
                make.width.equalToSuperview().inset(32)
            }
        }
    
        @objc func handlePan(gr: UIPanGestureRecognizer) {
            let translation = gr.translation(in: view)
            switch gr.state {
                case .began:
                    guard translation.y / view.bounds.height > 0 && translation.y / view.bounds.height < 1 else { return }
                    dismiss(animated: true, completion: nil)
                case .changed:
                    guard translation.y / view.bounds.height > 0 && translation.y / view.bounds.height < 1 else { return }
                    Hero.shared.update(translation.y / view.bounds.height)
                default:
                    let velocity = gr.velocity(in: view)
                    if ((translation.y + velocity.y) / view.bounds.height) > 0.5 {
                        Hero.shared.finish()
                    } else {
                        Hero.shared.cancel()
                    }
                }
        }
    }
}

extension Module.Event.Detail.Controller {
    static func create(with viewModel: Module.Event.Detail.ViewModel) -> UIViewController {
        let controller = Module.Event.Detail.Controller()
        controller.viewModel = viewModel
        controller.configure(with: viewModel)
        controller.setupView()
        return controller
    }
}
