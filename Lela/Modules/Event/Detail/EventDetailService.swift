
//
//  EventDetailRepository.swift
//  Lela
//
//  Created by Влада Кузнецова on 16/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol EventDetailServiceProtocol {
    func loadEvent() -> Observable<Models.Event>
}

extension Module.Event.Detail {
    class Service: EventDetailServiceProtocol {
        private var eventId: Int
        private var allEventsRepository = EventListRepository()
        
        init(with eventId: Int) {
            self.eventId = eventId
        }
        
        func loadEvent() -> Observable<Models.Event> {
            allEventsRepository.getEvents().map {[weak self] catogories in
                var event: Models.Event?
                catogories.forEach {[weak self] category in
                    if let findEvent = category.events?.first(where: { $0.id == self?.eventId }) {
                        event = findEvent
                    }
                }
                return event!
            }
        }
        

        
        //func loadData() -> Observable<Event> {
            
        //}
        
    }
}
