//
//  EventDetailViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 16/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Event.Detail {
    class ViewModel: ViewModelProtocol {
        var disposeBag = DisposeBag()
        var service: EventDetailServiceProtocol
        
        struct Input {
            var backAction: AnyObserver<Void>
        }
        
        struct Output {
            var closeModule: Observable<Void>
            var event: Observable<Models.Event>
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output
        
        let closeModuleSubject = PublishSubject<Void>()
        
        let eventSubject = ReplaySubject<Models.Event>.create(bufferSize: 1)

        // MARK: - Init and deinit
        init(_ service: EventDetailServiceProtocol) {
            self.service = service
            input = Input(
                backAction: closeModuleSubject.asObserver()
            )
            output = Output(
                closeModule: closeModuleSubject.asObservable(),
                event: eventSubject.asObservable()
            )
            
            service.loadEvent()
                .subscribe(onNext: {[weak self] (event) in
                    self?.eventSubject.onNext(event)
            }, onError: { (error) in
                
            })
                .disposed(by: disposeBag)
        }

    }
}
