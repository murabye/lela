//
//  EventDetailCommentCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 17/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit

extension Module.Event.Detail {
    class CommentCell: UIView {
        func setupView(with note: Models.Note) {
            self.round(radius: 15)
            self.backgroundColor = UIColor.view.standart.background
            
            let author = UILabel()
            author.textColor = UIColor.text.standart.standart
            author.font = UIFont.boldSystemFont(ofSize: 14)
            author.text = String(note.personId ?? 0)
                + (note.theme == nil ? "" : ": " + note.theme!)
            
            let message = UILabel()
            message.textColor = UIColor.text.standart.standart
            message.font = UIFont.systemFont(ofSize: 14)
            message.text = note.text
            
            let time = UILabel()
            time.textColor = UIColor.text.standart.subtitle
            time.font = UIFont.systemFont(ofSize: 14)
            time.text = Date.transform(string: note.time, with: .server, to: .dateTime)
            
            let stack = UIStackView()
            stack.axis = .vertical
            stack.addArrangedSubview(author)
            stack.addArrangedSubview(message)
            stack.addArrangedSubview(time)
            self.addSubview(stack)
            stack.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }
    }
}
