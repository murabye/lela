//
//  EventDetailCreateCommentCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 17/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

import UIKit
import SnapKit

extension Module.Event.Detail {
    class CreateCommentCell: UIView {
        func setupView() -> (theme: UITextField, text: UITextField, button: UIButton) {
            self.round(radius: 15)
            self.backgroundColor = UIColor.view.standart.background
                    
            let theme = Component.textField
            let text = Component.textField
            let sendButton = Component.button
            
            theme.placeholder = "Введите тему"
            text.placeholder = "Введите текст"
            sendButton.setTitle("Отправить", for: .normal)
            
            let stack = UIStackView()
            stack.axis = .vertical
            stack.addArrangedSubview(theme)
            stack.addArrangedSubview(text)
            stack.addArrangedSubview(sendButton)
            self.addSubview(stack)
            stack.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            
            return (theme: theme, text: text, button: sendButton)
        }
    }
}
