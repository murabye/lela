//
//  TwoLabelHorizontalView.swift
//  Lela
//
//  Created by Вова Петров on 20.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//


import UIKit
import SnapKit
import RxSwift
import RxCocoa

extension Module.Event.Detail.DetailView {
    class TwoLabelHorizontalView: UIView {
        
        var db = DisposeBag()
        
        var leftLabel: UILabel = {
            let label = UILabel()
            label.font = .systemFont(ofSize: 17)
            label.textColor = UIColor.text.standart.important
            label.textAlignment = .left
            label.numberOfLines = 0
            return label
        }()
        
        var rightLabel: UILabel = {
            let label = UILabel()
            label.font = .systemFont(ofSize: 17)
            label.textColor = UIColor.text.standart.standart
            label.textAlignment = .right
            label.numberOfLines = 0
            return label
        }()
        
        enum Orientation {
            case horizontal
            case vertical
        }
        
        init(_ orientation: Orientation = .horizontal) {
            super.init(frame: .zero)
            configureView(orientation)
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }
        
        func configureView(_ orientation: Orientation) {
            addSubview(leftLabel)
            addSubview(rightLabel)
            
            switch orientation {
            case .horizontal:
                leftLabel.snp.makeConstraints { (make) in
                    make.height.greaterThanOrEqualTo(17)
                    make.left.equalToSuperview()
                    make.top.equalToSuperview().offset(4)
                    make.bottom.equalToSuperview().inset(4)
                }
                
                rightLabel.snp.makeConstraints { (make) in
                    make.height.greaterThanOrEqualTo(17)
                    make.right.equalToSuperview()
                    make.left.equalTo(leftLabel.snp.right).priority(.low)
                    make.top.equalToSuperview().offset(4)
                    make.bottom.equalToSuperview().inset(4)
                }
            case .vertical:
                leftLabel.snp.makeConstraints { (make) in
                    make.height.greaterThanOrEqualTo(17)
                    make.left.equalToSuperview()
                    make.top.equalToSuperview().offset(4)
                    make.right.equalToSuperview()
                }
                
                rightLabel.textAlignment = .left
                rightLabel.snp.makeConstraints { (make) in
                    make.height.greaterThanOrEqualTo(17)
                    make.right.equalToSuperview()
                    make.left.equalToSuperview()
                    make.top.equalTo(leftLabel.snp.bottom).offset(8)
                    make.bottom.equalToSuperview().inset(4)
                }
            }
            

        }
    }
}
