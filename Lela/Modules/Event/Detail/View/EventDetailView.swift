//
//  EventDetailView.swift
//  Lela
//
//  Created by Вова Петров on 01.12.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

extension Module.Event.Detail {
    class DetailView: UIView {
        
        private var titleLabel: UILabel = {
            let label = UILabel()
            label.numberOfLines = 0
            label.textColor = UIColor.text.standart.standart
            label.font = UIFont.systemFont(ofSize: 28.0, weight: .semibold)
            label.textAlignment = .center
            return label
        }()
        
        private var descriptionView = TwoLabelHorizontalView(.vertical)
        private var creatorView = TwoLabelHorizontalView()
        private var personsView = TwoLabelHorizontalView()
        
        private var periodStack = UIStackView()
        
        static func create() -> DetailView {
            let view = DetailView()
            view.configureView()
            return view
        }
        
        private func configureView() {
            let mainStack = UIStackView()
            mainStack.axis = .vertical
            
            addSubview(mainStack)
            mainStack.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            
            mainStack.addArrangedSubview(titleLabel)
            mainStack.addArrangedSubview(descriptionView)
            mainStack.addArrangedSubview(creatorView)
            mainStack.addArrangedSubview(personsView)
//            mainStack.addArrangedSubview(makeLabel(text: "Дата проведения: "))
            mainStack.addArrangedSubview(periodStack)
            showLoading()
        }
        
        func configureData(with event: Models.Event) {
            titleLabel.text = event.title
            
            descriptionView.leftLabel.text = "Описание:"
            descriptionView.rightLabel.text = event.noteArray.first { $0.number == 0 }?.text
            
            if let maxPersons = event.maxPersons {
                personsView.isHidden = false
                let maxPersonsString = "\(event.partArray.count) из \(maxPersons)"
                personsView.leftLabel.text = "Участников:"
                personsView.rightLabel.text = maxPersonsString
            } else {
                personsView.isHidden = true
            }
            
            periodStack.arrangedSubviews.forEach { $0.removeFromSuperview() }
            event.periodArray.map{ toView($0) }.forEach{ periodStack.addArrangedSubview($0) }
            stopLoading()
        }
        
        private func toView(_ period: Models.Period) -> UIView {
            let view = UIView()
            let label = makeLabel(text: "\(period.startDate.getString(with: .dateTime)) - \(period.endDate.getString(with: .dateTime))")
            view.addSubview(label)
            label.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(8)
                make.top.equalToSuperview().offset(3)
                make.bottom.equalToSuperview().inset(3)
                make.right.equalToSuperview().inset(8)
            }
            return view
        }
        
        private func makeLabel(text: String) -> UILabel {
            let label = UILabel()
            label.textColor = UIColor.text.standart.important
            label.text = text
            return label
        }
    }
}
