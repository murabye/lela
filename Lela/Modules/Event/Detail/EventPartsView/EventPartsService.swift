//
//  EventPartsService.swift
//  Lela
//
//  Created by Вова Петров on 21.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

protocol EventDetailPartsServiceProtocol {
    func loadList(for eventId: Int) -> Observable<[Models.Part]>
    func create(userId: Int, eventId: Int) -> Observable<Models.Part>
    func delete(partId: Int) -> Observable<Bool>
    func index(partId: Int) -> Observable<Models.Part>
    func confirm(partId: Int, confirmed: Bool) -> Observable<Models.Part>
    func select(userId: Int)
}

extension Module.Event.Detail.Parts {
    class Service: EventDetailPartsServiceProtocol {
        func loadList(for eventId: Int) -> Observable<[Models.Part]> {
            struct Body: Codable {
                var eventId: Int
            }
            
            return NetworkManager
                .requestObservable(from: AdressesProvider.partList.value(),
                                                  allowedFromCache: true,
                                                  method: .post,
                                                  body: Body(eventId: eventId))
        }

        func create(userId: Int, eventId: Int) -> Observable<Models.Part> {
            struct Body: Codable {
                var userId: Int
                var eventId: Int
            }
            
            return NetworkManager
                .requestObservable(from: AdressesProvider.partCreate.value(),
                                                  allowedFromCache: false,
                                                  method: .post,
                                                  body: Body(userId: userId,
                                                             eventId: eventId))
        }
        
        func delete(partId: Int) -> Observable<Bool> {
            struct Body: Codable {
                var id: Int
            }
            struct VoidObject: Codable {}
            
            let observable: Observable<VoidObject> = NetworkManager
                .requestObservable(from: AdressesProvider.partDelete.value(),
                                                  allowedFromCache: false,
                                                  method: HTTPMethod.delete,
                                                  body: Body(id: partId))
            return observable.map { _ in true }
        }
        
        func index(partId: Int) -> Observable<Models.Part> {
            struct Body: Codable {
                var id: Int
            }
            
            return NetworkManager
                .requestObservable(from: AdressesProvider.partIndex.value(),
                                                  allowedFromCache: true,
                                                  method: .post,
                                                  body: Body(id: partId))
        }
        
        func confirm(partId: Int, confirmed: Bool) -> Observable<Models.Part> {
            struct Body: Codable {
                var id: Int
                var confirmed: Bool
            }
            
            return NetworkManager
                .requestObservable(from: AdressesProvider.partConfirm.value(),
                                                  allowedFromCache: false,
                                                  method: .post,
                                                  body: Body(id: partId,
                                                             confirmed: confirmed))
        }
        
        func select(userId: Int) {
            
        }
    }
}
