//
//  PartCellTableViewCell.swift
//  Lela
//
//  Created by Вова Петров on 22.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class PartCellTableViewCell: UITableViewCell {
    
    var db = DisposeBag()
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.view.standart.background
        view.clipsToBounds = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.shadow.standart.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 8
        
        view.layer.cornerRadius = 12.0
        
        return view
    }()
    
    let fioLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.textColor = UIColor.text.standart.standart
        label.text = "Загрузка..."
        return label
    }()
    
    let checkButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        return button
    }()
    
    var selectObservable: AnyObserver<Int>?
    var confirmObservable: AnyObserver<Module.Event.Detail.Parts.ViewModel.Input.ConfirmPart>?
    var part: Models.Part?
    
    func setupViews() {
        contentView.backgroundColor = .clear
        contentView.addSubview(bubbleView)
        bubbleView.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.leftMargin)
            make.right.equalTo(self.snp.rightMargin)
            make.top.equalToSuperview().offset(4)
            make.bottom.equalToSuperview().inset(4)
        }
        
        bubbleView.addSubview(fioLabel)
        fioLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(4)
            make.top.equalToSuperview().offset(4)
            make.bottom.equalToSuperview().inset(4)
            make.height.greaterThanOrEqualTo(17)
        }
        
        bubbleView.addSubview(checkButton)
        checkButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(4)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(30)
            make.left.equalTo(fioLabel.snp.right).offset(8).priority(.low)
        }
    }
    
    func configure(with part: Models.Part, selectObservable: AnyObserver<Int>, confirmObservable: AnyObserver<Module.Event.Detail.Parts.ViewModel.Input.ConfirmPart>) {
        self.part = part
        self.selectObservable = selectObservable
        self.confirmObservable = confirmObservable
        UserRepository.getUser(by: part.userId).map{
            var fio = ""
            if let name = $0.name {
                fio += name
            }
            if let surname = $0.surname {
                fio += " " + surname
            }
            if fio.count == 0 {
                fio = $0.email
            }
            return fio
        }
            .subscribe(fioLabel.rx.text)
            .disposed(by: db)
        
        let confirmImage = part.confirmed ? Assets.Icon.checked.image : Assets.Icon.link.image
        checkButton.setImage(confirmImage, for: .normal)
        checkButton.rx.tap.subscribe(onNext: { (_) in
                confirmObservable.onNext(Module.Event.Detail.Parts.ViewModel.Input.ConfirmPart(partId: part.id, confirmed: !part.confirmed))
            }).disposed(by: db)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            if let select = selectObservable,
                let part = part {
                select.onNext(part.id)
            }
        }
    }

}
