//
//  EventPartsViewModel.swift
//  Lela
//
//  Created by Вова Петров on 21.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift


extension Module.Event.Detail.Parts {
    class ViewModel: ViewModelProtocol {
        var db = DisposeBag()
        var service: EventDetailPartsServiceProtocol
        
        struct Input {
            
            struct ConfirmPart {
                var partId: Int
                var confirmed: Bool
            }
            
            var confirm: AnyObserver<ConfirmPart>
            var createWithUserId: AnyObserver<Int>
            var deletePartId: AnyObserver<Int>
            
            var backAction: AnyObserver<Void>
            var eventId: AnyObserver<Int>
            var selectUserId: AnyObserver<Int>
        }
        
        struct Output {
            var closeModule: Observable<Void>
            var parts: Observable<[Models.Part]>
            
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output
        
        let confirmSubject = PublishSubject<Input.ConfirmPart>()
        let createWithUserIdSubject = PublishSubject<Int>()
        let deletePartIdSubject = PublishSubject<Int>()
        let eventIdSubject = ReplaySubject<Int>.create(bufferSize: 1)
        let closeModuleSubject = PublishSubject<Void>()
        let selectUserIdSubject = PublishSubject<Int>()
        
        let updateListSubject = PublishSubject<Void>()
        
        let partsSubject = ReplaySubject<[Models.Part]>.create(bufferSize: 1)

        // MARK: - Init and deinit
        init(_ service: EventDetailPartsServiceProtocol) {
            self.service = service
            
            input = Input(confirm: confirmSubject.asObserver(),
                          createWithUserId: createWithUserIdSubject.asObserver(),
                          deletePartId: deletePartIdSubject.asObserver(),
                          backAction: closeModuleSubject.asObserver(),
                          eventId: eventIdSubject.asObserver(),
                          selectUserId: selectUserIdSubject.asObserver())
            
            output = Output(closeModule: closeModuleSubject.asObservable(),
                            parts: partsSubject.asObservable())
            
            updateListSubject
                .withLatestFrom(eventIdSubject)
                .flatMap { service.loadList(for: $0) }
                .subscribe(partsSubject)
                .disposed(by: db)
            
            eventIdSubject.map { _ in return }
                .subscribe(updateListSubject)
                .disposed(by: db)
            
            confirmSubject
                .flatMap { service.confirm(partId: $0.partId, confirmed: $0.confirmed) }
                .map { _ in return }
                .subscribe(updateListSubject)
                .disposed(by: db)
            
            createWithUserIdSubject
                .asObservable()
                .withLatestFrom(eventIdSubject.asObservable(), resultSelector: { (userId, eventId) -> Observable<Models.Part> in
                    service.create(userId: userId, eventId: eventId)
                })
                .map { _ in return }
                .subscribe(updateListSubject)
                .disposed(by: db)
            
            deletePartIdSubject
                .flatMap { service.delete(partId: $0) }
                .map { _ in return }
                .subscribe(updateListSubject)
                .disposed(by: db)
        }

    }
}
