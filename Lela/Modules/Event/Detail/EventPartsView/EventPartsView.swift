//
//  EventPartsView.swift
//  Lela
//
//  Created by Вова Петров on 21.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

extension Module.Event.Detail.Parts {
    class Controller: UIView {
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!
        let db = DisposeBag()
        
        var tableView: UITableView = {
            let table = UITableView()
            table.register(PartCellTableViewCell.self, forCellReuseIdentifier: "PartCellTableViewCell")
            return table
        }()
        
        func configure(with viewModel: ViewModel) {
            viewModel.output.parts
            .bind(to: tableView.rx.items) { (tableView, row, part) in
                let cell = tableView.dequeueReusableCell(withIdentifier: "PartCellTableViewCell")! as! PartCellTableViewCell
                cell.setupViews()
                cell.configure(with: part,
                               selectObservable: viewModel.input.selectUserId,
                               confirmObservable: viewModel.input.confirm)
                return cell
            }
            .disposed(by: db)
        }
        
        func setupView() {
            addSubview(tableView)
            tableView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
}

extension Module.Event.Detail.Parts.Controller {
    static func create(with viewModel: Module.Event.Detail.Parts.ViewModel) -> UIView {
        let controller = Module.Event.Detail.Parts.Controller()
        controller.viewModel = viewModel
        controller.configure(with: viewModel)
        controller.setupView()
        return controller
    }
}
