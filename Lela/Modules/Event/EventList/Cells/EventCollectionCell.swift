//
//  EventCollectionCell.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit

class EventCollectionCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //setupViews()
    }
    
    var cellEvent: Models.Event? = nil
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.view.standart.background
        
        view.clipsToBounds = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.shadow.standart.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 16
        
        view.layer.cornerRadius = 26.0
        
        return view
    }()
    
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Мероприятие"
        label.font = UIFont.systemFont(ofSize: 21.0, weight: .semibold)
        label.textColor = UIColor.text.standart.standart
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.text.standart.standart
        label.numberOfLines = 0
        return label
    }()
    
    let personsLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.text.standart.important
        return label
    }()
    
    let moreLabel: UILabel = {
        let label = UILabel()
        label.text = "Подробнее"
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.textColor = UIColor.text.standart.important
        return label
    }()
    
    
    func setupViews() {
        self.backgroundColor = .clear
        contentView.addSubview(bubbleView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(personsLabel)
        contentView.addSubview(moreLabel)
        
        let comment = cellEvent != nil && cellEvent!.noteArray.count > 0
            ? cellEvent!.noteArray[0]
            : Models.Note.getEmpty()
        
        nameLabel.text = cellEvent?.title ?? nameLabel.text
        descriptionLabel.text = comment.text
        if let event = cellEvent {
            let maxPersonsString = event.maxPersons != nil ? " из \(event.maxPersons!)" : ""
            personsLabel.text = "Участников: \(event.partArray.count)" + maxPersonsString
        }
        bubbleView.hero.id = "event \(cellEvent!.id!)"
        bubbleView.hero.modifiers = [.spring(stiffness: 250, damping: 25), .fade]

        bubbleView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 5))
        }
        
        nameLabel.snp.makeConstraints { (make) -> Void in
            make.height.greaterThanOrEqualTo(17)
            make.top.equalTo(bubbleView).offset(12)
            make.left.right.equalTo(bubbleView).offset(20)
        }
        
        descriptionLabel.snp.makeConstraints { (make) -> Void in
            make.height.greaterThanOrEqualTo(17)
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
            make.left.right.equalTo(bubbleView).offset(20)
            make.right.equalTo(bubbleView).inset(20)
            make.bottom.lessThanOrEqualTo(personsLabel.snp.top)
        }
        
        personsLabel.snp.makeConstraints { (make) in
            make.height.greaterThanOrEqualTo(17)
            make.bottom.equalTo(bubbleView.snp.bottom).inset(10)
            make.left.equalTo(bubbleView).offset(20)
        }
        
        moreLabel.snp.makeConstraints { (make) in
            make.height.greaterThanOrEqualTo(17)
            make.bottom.equalTo(bubbleView.snp.bottom).inset(10)
            make.right.equalTo(bubbleView).inset(20)
            make.left.equalTo(moreLabel.snp.right).offset(15).priority(.low)
        }

    }
}




