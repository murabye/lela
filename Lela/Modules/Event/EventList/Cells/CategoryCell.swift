//
//  CategoryCell.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class CategoryCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private let cellId = "eventCellId"
    private let addCellId = "addEventCellId"
    var selectObserver: AnyObserver<Models.Event>?

    var category: Models.EventCategory? = nil {
        didSet {
            eventsCollectionView.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.Icon.binoculars.image
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Категория события"
        label.textColor = UIColor.navigation.text
        label.font = UIFont.systemFont(ofSize: 32, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let eventsCollectionView: UICollectionView = {
        let layout = BouncyLayout(style: .subtle)
        layout.scrollDirection = .horizontal
        let width = UIScreen.main.bounds.width - 50
        let cellWidth = width > 400 ? 400 : width
        layout.itemSize = CGSize(width: cellWidth, height: 180.0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.clipsToBounds = false
        collectionView.layer.masksToBounds = false
        
        return collectionView
    }()
    
    override func layoutSubviews() {
        iconImageView.image = Assets.Icon.icon(for: category?.iconId).image
        nameLabel.text = category?.title ?? nameLabel.text
    }
    
    
    func setupViews() {
        backgroundColor = UIColor.clear
        
        addSubview(eventsCollectionView)
        addSubview(nameLabel)
        addSubview(iconImageView)
        
        eventsCollectionView.dataSource = self
        eventsCollectionView.delegate = self
        
        eventsCollectionView.register(EventCollectionCell.self, forCellWithReuseIdentifier: cellId)
        
        iconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(20)
            make.left.equalTo(self).offset(15)
            make.height.width.equalTo(30)
        }
        
        nameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(15)
            make.left.equalTo(iconImageView.snp.right).offset(8)
        }
        
        eventsCollectionView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(nameLabel.snp.bottom)
            make.height.equalTo(180)
            make.left.equalTo(self)
            make.bottom.equalTo(self).offset(5)
            make.right.equalTo(self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let category = category, let events = category.events else { return 0 }
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventCollectionCell
        cell.cellEvent = category?.events?[indexPath.item] ?? nil
        cell.setupViews()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectObserver = selectObserver,
            let category = category,
            let events = category.events else {
            return
        }
        selectObserver.onNext(events[indexPath.item])
    }
}
