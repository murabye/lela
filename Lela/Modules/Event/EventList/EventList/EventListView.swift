//
//  EventListView.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//Copyright © 2019 варя. All rights reserved.
//
/*
import UIKit
import SnapKit

//MARK: - Public Interface Protocol
protocol EventListViewInterface {
}

//MARK: EventListView Class
final class EventListView: View {
    private let cellId = "eventCategoryCellId"

    var categories: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
//        categories.append(Category(name: "Волонтерство", events: [
//            Event(name: "Съездим в приют", description: ""),
//            Event(name: "Дом престарелых", description: ""),
//            Event(name: "Ветеранам на 9 мая", description: ""),
//            Event(name: "Сироты", description: "Детский дом Дружба")
//            ]))
//        categories.append(Category(name: "Безопасность", events: [
//            Event(name: "Охрана труда", description: "Анонимный раздел")
//            ]))
//        categories.append(Category(name: "Молодежка", events: [
//            Event(name: "Танцы", description: ""),
//            Event(name: "Съездим на лыжах!", description: "")
//            ]))
//        categories.append(Category(name: "Корпоративные", events: [
//            Event(name: "Шашлычок", description: "у Семеныча на баньке")
//            ]))
//        categories.append(Category(name: "Обучение и развитие", events: [
//            Event(name: "Вебинар управленческий", description: "Как повысить заинтересованность"),
//            Event(name: "Занятия по скорочтению", description: ""),
//            Event(name: "Собираем свою группу", description: ""),
//            Event(name: "Репетитор математика", description: "10 лет стажа")
//            ]))
    }
}

//MARK:- configure view
extension EventListView: Configurable {
    
    func configure() {
        configureMainView()
        configureCollection()
    }
    
    func configureMainView() {
        self.view.backgroundColor = UIColor.app.background
        self.navigationItem.title = "Мероприятия"
    }
    
    func configureCollection() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: view.frame.width, height: 130)
        
        let collection = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.register(CategoryCell.self, forCellWithReuseIdentifier: cellId)
        
        collection.backgroundColor = UIColor.view.standart.background

        self.view.addSubview(collection)
        collection.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self.view)
        }
    }
}

//MARK:- collection delegate
extension EventListView: EventListViewInterface {
}

//MARK:-collection datasource
extension EventListView: UICollectionViewDelegate {
}

//MARK: - Public interface
extension EventListView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CategoryCell
        cell.category = categories[indexPath.item]
        cell.setupViews()
        return cell
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension EventListView {
    var presenter: EventListPresenter {
        return _presenter as! EventListPresenter
    }
}
*/
