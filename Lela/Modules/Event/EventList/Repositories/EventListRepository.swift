//
//  EventListRepository.swift
//  Lela
//
//  Created by Вова Петров on 12.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol EventListRepositoryProtocol {
    func getEvents() -> Observable<[Models.EventCategory]>
}

class EventListRepository: EventListRepositoryProtocol {
    struct RequestModel: Codable {
        let memberId: Int?
    }
    
    func getEvents() -> Observable<[Models.EventCategory]> {
        let categoryObservable: Observable<[Models.EventCategory]> = NetworkManager
            .requestObservable(from: AdressesProvider.categoryList.value(),
                                              allowedFromCache: true,
                                              method: .post,
                                              body: nil)
        
        let eventsObservable: Observable<[Models.Event]> = NetworkManager
            .requestObservable(from: AdressesProvider.eventList.value(),
                                          allowedFromCache: true,
                                          method: .post,
                                          body: RequestModel(memberId: nil)
        )
        
        let result: Observable<[Models.EventCategory]> = Observable
            .combineLatest(categoryObservable, eventsObservable)
            .map { (categoryList, eventList) -> [Models.EventCategory] in
                var categoryDict = [Int: Models.EventCategory]()
                for category in categoryList {
                    if let categoryId = category.id {
                        categoryDict[categoryId] = category
                    }
                }
                
                for event in eventList {
                    if categoryDict[event.categoryId]?.events == nil {
                        categoryDict[event.categoryId]?.events = [Models.Event]()
                    }
                    
                    categoryDict[event.categoryId]?.events?.append(event)
                }
                
                return Array(categoryDict.values)
        }
        
        return result
    }
}
