//
//  EventListViewModel.swift
//  Lela
//
//  Created by Вова Петров on 12.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import RxSwift

class EventListControllerViewModel: ViewModelProtocol {
    struct Input {
        let selectEvent: AnyObserver<Models.Event>
        let addEvent: AnyObserver<Void>
        let addCategory: AnyObserver<Void>
        let search: AnyObserver<String>
        let refresh: AnyObserver<Void>
    }
    struct Output {
        let isLoading: Observable<Bool>
        let categories: Observable<[Models.EventCategory]>
        let showEventModule: Observable<Models.Event>
        let addEventModule: Observable<Void>
        let addCategoryModule: Observable<Void>
        let error: Observable<Error>
    }
    // MARK: - Public properties
    let input: Input
    let output: Output
    
    // MARK: - Private properties
    private let isLoadingSubject = PublishSubject<Bool>()
    private let addEventSubject = PublishSubject<Void>()
    private let addCategorySubject = PublishSubject<Void>()
    private let searchSubject = PublishSubject<String>()
    private let refreshSubject = PublishSubject<Void>()
    
    private let categoriesSubject = PublishSubject<[Models.EventCategory]>()
    private let showEventSubject = PublishSubject<Models.Event>()
    
    private let errorsSubject = PublishSubject<Error>()
    private let db = DisposeBag()
    
    // MARK: - Init and deinit
    init(_ eventListRepository: EventListRepositoryProtocol) {
        
        input = Input(selectEvent: showEventSubject.asObserver(),
                      addEvent: addEventSubject.asObserver(),
                      addCategory: addCategorySubject.asObserver(),
                      search: searchSubject.asObserver(),
                      refresh: refreshSubject.asObserver())
        
        output = Output(isLoading: isLoadingSubject.asObservable(),
                        categories: categoriesSubject.asObservable(),
                        showEventModule: showEventSubject.asObservable(),
                        addEventModule: addEventSubject.asObservable(),
                        addCategoryModule: addCategorySubject.asObservable(),
                        error: errorsSubject.asObservable())
        
        refreshSubject
            .flatMap { eventListRepository.getEvents() }
            .subscribe { event in
                switch event {
                case .next(let categories):
                    self.categoriesSubject.onNext(categories)
                case .error(let error):
                    self.errorsSubject.onNext(error)
                default:
                    break
                } 
            }
            .disposed(by: db)
        Radio.EventList.Listen.needUpdate()
            .do(onNext: { (_) in
                print("update")
            })
            .subscribe(refreshSubject)
            .disposed(by: db)
        
        refreshSubject.onNext(())
    }
    
    deinit {
        print("\(self) dealloc")
    }
}
