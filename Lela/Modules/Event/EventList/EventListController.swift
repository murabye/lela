//
//  EventListController.swift
//  Lela
//
//  Created by Вова Петров on 12.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import Hero
import SwiftEntryKit

final class EventListController: ViewController, ControllerType {
    private static let cellId = "eventCategoryCellId"
    
    var collectionView: UICollectionView = {
        let layout = BouncyLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 230)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(CategoryCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.backgroundColor = UIColor.app.background
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.hero.id = "EventListCollectionView"
        collectionView.hero.isEnabled = true
        return collectionView
    }()
    
    var addButton: UIButton = {
        let addButton = UIButton()
        addButton.setImage(Assets.Icon.plus.image, for: .normal)
        addButton.layer.cornerRadius = 22
        addButton.hero.id = "AddButton"
        return addButton
    }()
    
    var addCategoryButton: UIButton = {
        let addButton = UIButton()
        addButton.setImage(Assets.Icon.plus.image, for: .normal)
        addButton.layer.cornerRadius = 22
        addButton.hero.id = "AddCategoryButton"
        return addButton
    }()
    
    var searchButton: UIButton = {
        let searchButton = UIButton()
        searchButton.backgroundColor = UIColor.button.standart.standart.background
        searchButton.setImage(UIImage(named: "search_image"), for: .normal)
        searchButton.tintColor = UIColor.button.standart.standart.text
        searchButton.layer.cornerRadius = 22
        return searchButton
    }()
    
    var shadowView: UIView = {
        let shadowView = UIView()
        shadowView.layer.shadowColor = UIColor.view.standart.background.cgColor
        shadowView.backgroundColor = UIColor.view.standart.background
        shadowView.layer.shadowOffset = .init(width: 0, height: 15)
        shadowView.layer.shadowRadius = 10
        shadowView.layer.shadowOpacity = 1.0
        shadowView.clipsToBounds = false
        shadowView.layer.masksToBounds = false
        return shadowView
    }()
    
    let logo = Component.headerLogo
    var pageName = Component.headerPageName
    
    typealias ViewModelType = EventListControllerViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()

    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configure(with: viewModel)
    }
}

extension EventListController: Configurable {
    
    func configureView() {
        self.view.backgroundColor = UIColor.app.background
        
        pageName.text = "Мероприятия".uppercased()
        
        view.addSubview(collectionView)
        view.addSubview(shadowView)
        view.addSubview(logo)
        view.addSubview(pageName)
        
        let logoImgSize = UIImage.logo.size
        
        logo.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(logo.snp.height).multipliedBy(logoImgSize.width / logoImgSize.height)
            make.width.equalTo(150)
        }
        
        pageName.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(25)
            make.left.right.equalTo(view)
            make.height.equalTo(50)
        }
        

        shadowView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.bottom.equalTo(pageName)
        }

       
        collectionView.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.bottom.equalTo(self.view.snp.bottom)
            make.top.equalTo(pageName.snp.bottom)
        }
        
        let addView = UIView()
        addView.backgroundColor = UIColor.button.standart.standart.background
        addView.layer.cornerRadius = 22
        addView.hero.id = "AddView"
        
        self.view.addSubview(addView)
        self.view.addSubview(addButton)
        addButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(44)
            make.centerY.equalTo(pageName.snp.centerY)
            make.right.equalToSuperview().inset(15)
        }
        
        addView.snp.makeConstraints { (make) in
            make.edges.equalTo(addButton).inset(6)
        }
        
        let addCategoryView = UIView()
        addCategoryView.backgroundColor = UIColor.button.standart.standart.background
        addCategoryView.layer.cornerRadius = 22
        addCategoryView.hero.id = "AddCategoryView"
        
        self.view.addSubview(addCategoryView)
        self.view.addSubview(addCategoryButton)
        addCategoryButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(44)
            make.centerY.equalTo(pageName.snp.centerY)
            make.left.equalToSuperview().offset(15)
        }
        
        addCategoryView.snp.makeConstraints { (make) in
            make.edges.equalTo(addCategoryButton).inset(6)
        }
        
//        self.view.addSubview(searchButton)
//        searchButton.snp.makeConstraints { (make) in
//            make.height.width.equalTo(44)
//            make.centerY.equalTo(pageName.snp.centerY)
//            make.left.equalToSuperview().offset(15)
//        }
    }
}

extension EventListController {
    func configure(with viewModel: ViewModelType) {
        //MARK: Input
        addButton.rx
            .tap.subscribe(viewModel.input.addEvent)
            .disposed(by: disposeBag)
        addCategoryButton.rx
            .tap.subscribe(viewModel.input.addCategory)
            .disposed(by: disposeBag)
        
        //MARK: - Output
        viewModel.output.categories
            .bind(to: collectionView.rx.items(cellIdentifier: EventListController.cellId)) {index, model, cell in
                let cell = cell as! CategoryCell
                cell.selectObserver = viewModel.input.selectEvent
                cell.category = model
        }
        .disposed(by: disposeBag)
        
        viewModel.output.addEventModule.subscribe {[weak self] _ in
            let service = Module.Event.Add.Service()
            let vm = Module.Event.Add.ViewModel(service)
            let vc = Module.Event.Add.Controller.create(with: vm)
            vc.modalTransitionStyle = .flipHorizontal
            vc.modalPresentationStyle = .overFullScreen
            vc.hero.isEnabled = true
            self?.present(vc, animated: true)
        }
        .disposed(by: disposeBag)
        
        viewModel.output.showEventModule.subscribe { [weak self] event in
            guard let self = self,
                let eventModel = event.element else { return }
            
            let service = Module.Event.Detail.Service(with: eventModel.id!)
            let vm = Module.Event.Detail.ViewModel(service)
            let vc = Module.Event.Detail.Controller.create(with: vm) as! Module.Event.Detail.Controller
            
            vc.hero.isEnabled = true
            vc.hero.modalAnimationType = .none
            
            vc.view.hero.id = "event \(eventModel.id!)"
            vc.view.hero.modifiers = [.useNoSnapshot, .spring(stiffness: 250, damping: 25)]
            
            vc.mainStack.hero.modifiers = [.source(heroID: "\(String(describing: eventModel.id))"), .spring(stiffness: 250, damping: 25)]
            
//            vc.mainStack.hero.modifiers = [.useNoSnapshot, .forceAnimate, .spring(stiffness: 250, damping: 25)]
            
//            vc.visualEffectView.hero.modifiers = [.fade, .useNoSnapshot]
            
            self.present(vc, animated: true)
        }.disposed(by: disposeBag)
        
        viewModel.output.addCategoryModule.subscribe(onNext: { (_) in
            
            let service = Module.Category.Add.Service()
            let vm = Module.Category.Add.ViewModel(service)
            let vc = Module.Category.Add.Controller.create(with: vm)
            SwiftEntryKit.display(entry: vc, using: self.setupFormPresets())
        })
        .disposed(by: disposeBag)
    }
}

extension EventListController {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = EventListController()
        controller.viewModel = viewModel
        return controller
    }
}
