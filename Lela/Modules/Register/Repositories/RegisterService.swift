//
//  RegisterRepository.swift
//  Lela
//
//  Created by Вова Петров on 28.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import RxSwift

protocol RegisterServiceProtocol {
    func register(with model: RegisterModel) -> Observable<Models.User>
    init(with loginRepository: LoginRepositoryProtocol)
}

struct RegisterModel : Codable {
    let email: String
    let orgInfo = "СИБУР"
    let password: String
    let verifyPassword: String
    
    func toCredentials() -> Credentials {
        return Credentials(email: self.email, password: self.password)
    }
}

struct RegisterResultModel : Codable {
    let email: String
    let id: Int
}

class RegisterService: RegisterServiceProtocol {
    let loginRepository: LoginRepositoryProtocol
    
    required init(with loginRepository: LoginRepositoryProtocol) {
        self.loginRepository = loginRepository
    }
    
    func register(with model: RegisterModel) -> Observable<Models.User> {
        return NetworkManager.register(model)
            .flatMap{ [unowned self] registerResult in
                self.loginRepository.signIn(with: model.toCredentials())
                    .map{ $0.user }
        }
    }
}
