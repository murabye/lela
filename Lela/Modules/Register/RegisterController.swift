//
//  RegisterController.swift
//  Lela
//
//  Created by Вова Петров on 25.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

//MARK: LoginView Class
final class RegisterController: ViewController, ControllerType {
    
    typealias ViewModelType = RegisterControllerViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()
    
    var loginField: CustomTextField = {
        let field = Component.textField
        field.placeholder = "Обязательно"
        field.returnKeyType = .continue
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.hero.id = "loginField"
        return field
    }()
    var passwordField: CustomTextField = {
        let field = Component.textField
        field.placeholder = "Обязательно"
        field.returnKeyType = .continue
        field.isSecureTextEntry = true
        field.autocapitalizationType = .none
        field.hero.id = "passwordField"
        return field
    }()
    var confirmPasswordField: CustomTextField = {
        let field = Component.textField
        field.placeholder = "Обязательно"
        field.returnKeyType = .done
        field.isSecureTextEntry = true
        field.autocapitalizationType = .none
        field.hero.id = "signInButton"
        return field
    }()
    var registerButton = Component.button
    var canselButton = Component.helpButton

    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configure(with: viewModel)
    }
    
    func configureView() {
        self.view.backgroundColor = .white
        
        let logoView = UIImageView(image: .logo)
        logoView.contentMode = .scaleAspectFit
        
        self.view.addSubview(logoView)
        logoView.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
            make.height.equalTo(logoView.snp.width).dividedBy(1958.0/468.0)
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY).offset(-200)
        }
        
        let descriptionLabel = UILabel()
        descriptionLabel.text = "ВХОД"
        descriptionLabel.hero.id = "descriptionLabel"
        descriptionLabel.font = UIFont.systemFont(ofSize: 17.0, weight: .thin)
        descriptionLabel.textColor = UIColor.text.standart.standart
        
        self.view.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) in
            make.height.equalTo(35)
            make.width.equalTo(60)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(logoView.snp.bottom).offset(20)
        }
        
        let loginLabel = UILabel()
        loginLabel.text = "Логин"
        loginLabel.hero.id = "loginLabel"
        loginLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .thin)
        loginLabel.textColor = UIColor.text.standart.standart
        self.view.addSubview(loginLabel)
        loginLabel.snp.makeConstraints { (make) in
            make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
            make.width.equalTo(270)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(descriptionLabel.snp.bottom).offset(20)
        }
        
        self.view.addSubview(loginField)
        loginField.snp.makeConstraints { (make) in
            make.width.equalTo(loginLabel.snp.width).offset(30)
            make.height.equalTo(40)
            make.top.equalTo(loginLabel.snp.bottom).offset(6)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        let passwordLabel = UILabel()
        passwordLabel.text = "Пароль"
        passwordLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .thin)
        passwordLabel.textColor = UIColor.text.standart.standart
        passwordLabel.hero.id = "passwordLabel"
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (make) in
            make.width.equalTo(loginLabel.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(loginField.snp.bottom).offset(12)
        }
        
        self.view.addSubview(passwordField)
        passwordField.snp.makeConstraints { (make) in
            make.width.equalTo(loginField.snp.width)
            make.height.equalTo(40)
            make.top.equalTo(passwordLabel.snp.bottom).offset(6)
            make.centerX.equalTo(self.view.snp.centerX)
        }

        let confirmPasswordLabel = UILabel()
        confirmPasswordLabel.text = "Подтверждение пароля"
        confirmPasswordLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .thin)
        confirmPasswordLabel.textColor = UIColor.text.standart.standart
        self.view.addSubview(confirmPasswordLabel)
        confirmPasswordLabel.snp.makeConstraints { (make) in
            make.width.equalTo(passwordField.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(passwordField.snp.bottom).offset(12)
        }

        self.view.addSubview(confirmPasswordField)
        confirmPasswordField.snp.makeConstraints { (make) in
            make.width.equalTo(passwordField.snp.width)
            make.height.equalTo(40)
            make.top.equalTo(confirmPasswordLabel.snp.bottom).offset(6)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        registerButton.setTitle("РЕГИСТРАЦИЯ", for: .normal)
        registerButton.isEnabled = false
        registerButton.hero.id = "signInButton"
        
        self.view.addSubview(registerButton)
        registerButton.snp.makeConstraints { (make) in
            make.width.equalTo(confirmPasswordField.snp.width)
            make.height.equalTo(40)
            make.top.equalTo(confirmPasswordField.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        canselButton.setTitle("Отмена", for: .normal)
        canselButton.hero.id = "signUpButton"
        
        self.view.addSubview(canselButton)
        canselButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.top.equalTo(registerButton.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.snp.centerX)
        }
    }
    
    func configure(with viewModel: RegisterControllerViewModel) {

        loginField.rx.text.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.email)
            .disposed(by: disposeBag)

        passwordField.rx.text.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.password)
            .disposed(by: disposeBag)

        confirmPasswordField.rx.text.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.confirmPassword)
            .disposed(by: disposeBag)

        let registerDidTapObservable = registerButton.rx.tap.asObservable()
        registerDidTapObservable
            .subscribe(viewModel.input.registerDidTap)
            .disposed(by: disposeBag)
        registerDidTapObservable
            .subscribe(onNext: { [weak self] in
                self?.registerButton.show(loading: true)
            })
            .disposed(by: disposeBag)
        
        
        canselButton.rx.tap.asObservable()
            .subscribe(viewModel.input.canselDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.canselDidTap
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)

        viewModel.output.errorsObservable
            .subscribe(onNext: { [unowned self] (error) in
                self.presentError(error)
                self.registerButton.show(loading: false)
            })
            .disposed(by: disposeBag)

        viewModel.output.registerResultObservable
            .subscribe(onNext: { [weak self] (user) in
                self?.show(MainTabBar(), sender: nil)
            })
            .disposed(by: disposeBag)

        viewModel.output.registerAvailable
            .subscribe(onNext: {[unowned self] (available) in
                self.registerButton.isEnabled = available
            })
            .disposed(by: disposeBag)

    }
    
    override func keyboardShowView() -> UIView? {
        return confirmPasswordField
    }

    override func keyboardViewOffsetY() -> CGFloat {
        return 100
    }
}

extension RegisterController {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = RegisterController()
        controller.viewModel = viewModel
        return controller
    }
}
