//
//  RegisterViewModel.swift
//  Lela
//
//  Created by Вова Петров on 25.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

class RegisterControllerViewModel: ViewModelProtocol {
    struct Input {
        let email: AnyObserver<String>
        let password: AnyObserver<String>
        let confirmPassword: AnyObserver<String>
        let registerDidTap: AnyObserver<Void>
        let canselDidTap: AnyObserver<Void>
    }
    struct Output {
        let registerResultObservable: Observable<Models.User>
        let errorsObservable: Observable<Error>
        let registerAvailable: Observable<Bool>
        let canselDidTap: Observable<Void>
    }
    // MARK: - Public properties
    let input: Input
    let output: Output
    
    // MARK: - Private properties
    private let emailSubject = PublishSubject<String>()
    private let passwordSubject = PublishSubject<String>()
    private let confirmPasswordSubject = PublishSubject<String>()
    private let registerDidTapSubject = PublishSubject<Void>()
    private let registerResultSubject = PublishSubject<Models.User>()
    private let errorsSubject = PublishSubject<Error>()
    private let registerAvailableSubject = PublishSubject<Bool>()
    private let canselSubject = PublishSubject<Void>()
    private let disposeBag = DisposeBag()

    private var registerModelObservable: Observable<RegisterModel> {
        return Observable.combineLatest(emailSubject,
                                        passwordSubject,
                                        confirmPasswordSubject) {
                                            (email, password, confirmPassword) in
            return RegisterModel(email: email,
                                 password: password,
                                 verifyPassword: confirmPassword)
        }
    }
    
    // MARK: - Init and deinit
    init(_ registerRepository: RegisterServiceProtocol) {
        
        input = Input(email: emailSubject.asObserver(),
                      password: passwordSubject.asObserver(),
                      confirmPassword: confirmPasswordSubject.asObserver(),
                      registerDidTap: registerDidTapSubject.asObserver(),
                      canselDidTap: canselSubject.asObserver())
        
        output = Output(registerResultObservable: registerResultSubject.asObservable(),
                        errorsObservable: errorsSubject.asObservable(),
                        registerAvailable: registerAvailableSubject.asObservable(),
                        canselDidTap: canselSubject.asObservable())
        

        registerModelObservable
            .subscribe(onNext: { [weak self] model in
                self?.registerAvailableSubject.onNext(
                    model.email.count > 0 &&
                    model.password == model.verifyPassword &&
                    model.password.count > 0)
            }, onError: { [weak self] error in
                    self?.errorsSubject.onNext(error)
            })
            .disposed(by: disposeBag)

        registerDidTapSubject
            .withLatestFrom(registerModelObservable)
            .flatMapLatest { registerModel in
                return registerRepository.register(with: registerModel).materialize()
            }
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .next(let person):
                    self?.registerResultSubject.onNext(person)
                case .error(let error):
                    self?.errorsSubject.onNext(error)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)

    }
    
    deinit {
        print("\(self) dealloc")
    }
}
