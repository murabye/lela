//
//  LKViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 03/09/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Cabinet.Personal {
    class ViewModel: ViewModelProtocol {
        struct Input {
            let settingInited: AnyObserver<Void>
            let outInited: AnyObserver<Void>
        }
        
        struct Output {
            let user: Observable<Models.User>
            let error: Observable<Error>
            let showSettingsModule: Observable<Void>
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output
        
        // MARK: - Private propertiess
        private let userSubject = ReplaySubject<Models.User>.create(bufferSize: 1)
        private let showSettingsSubject = PublishSubject<Void>()
        private let outSubject = PublishSubject<Void>()
        private let errorsSubject = PublishSubject<Error>()
        
        private let disposeBag = DisposeBag()
        
        // MARK: - Init and deinit
        init(_ lkRepository: LKRepository) {
            
            input = Input(settingInited: showSettingsSubject.asObserver(),
                          outInited: outSubject.asObserver())
            
            output = Output(user: userSubject.asObservable(),
                            error: errorsSubject.asObservable(),
                            showSettingsModule: showSettingsSubject.asObservable())
            
            lkRepository
                .getCurrentUser()
                .asObservable()
                .subscribe { event in
                    switch event {
                    case .next(let users):
                        self.userSubject.onNext(users)
                    case .error(let error):
                        self.errorsSubject.onNext(error)
                    default:
                        break
                    }
                }
                .disposed(by: disposeBag)
            
            outSubject.asObservable()
                .subscribe { _ in
                    KeychainService.clearCredentials()
                    LoginRouter.dislogin(completion: nil)
            }.disposed(by: disposeBag)
        }
        
        deinit {
            print("\(self) dealloc")
        }
    }
}
