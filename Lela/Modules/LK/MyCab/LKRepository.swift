//
//  LKRepository.swift
//  Lela
//
//  Created by Влада Кузнецова on 12/09/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol LKRepositoryProtocol {
    func getCurrentUser() -> Observable<Models.User>
}

class LKRepository: LKRepositoryProtocol {

    enum LKError: Error {
        case loginError
    }
    
    func getCurrentUser() -> Observable<Models.User> {
        return Observable.create { observer in
            if let loginedUser = UserService.currentUser {
                observer.onNext(loginedUser)
            } else {
                observer.onError(LKError.loginError)
            }
            
            return Disposables.create()
        }
    }
    
}
