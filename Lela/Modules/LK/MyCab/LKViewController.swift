//
//  LKView.swift
//  Lela
//
//  Created by Владимир on 21/05/2019.
//Copyright © 2019 варя. All rights reserved.
//

import UIKit
import RxSwift

extension Module.Cabinet.Personal {
    class Controller: ViewController, ControllerType {
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!
        private let disposeBag = DisposeBag()

        private var profileView: ProfileView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }

        func configure(with viewModel: ViewModel) {
            guard let lkProfileView = ProfileView.create(with: viewModel) as? ProfileView else {return}
            profileView = lkProfileView
            
            viewModel.output.showSettingsModule.subscribe(onNext: { () in
                let vm = Module.Cabinet.Redo.ViewModel()
                let redoCab = Module.Cabinet.Redo.Controller.create(with: vm)
                redoCab.hero.isEnabled = true
                redoCab.modalPresentationStyle = .fullScreen
                self.hero.isEnabled = true
                self.present(redoCab, animated: true, completion: nil)
                })
                .disposed(by: disposeBag)
        }

        func setupView() {
            let scrollView = self.makeBaseScrollController(named: "Личный кабинет")

            scrollView.addSubview(profileView)
            profileView.snp.makeConstraints{ make in
                make.edges.equalTo(scrollView).inset(15)
                make.width.equalTo(scrollView).inset(15)
            }
        }
        
        static func create(with viewModel: ViewModelType) -> UIViewController {
            let controller = Controller()
            controller.viewModel = viewModel
            return controller
        }
    }
}
