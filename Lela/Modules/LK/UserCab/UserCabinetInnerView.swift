//
//  UserCabinetInnerView.swift
//  Lela
//
//  Created by Влада Кузнецова on 29/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SnapKit

extension Module.Cabinet.User {
    class InnerView : UIView, ViewType {
        typealias ViewModelType = ViewModel
        typealias StringDriver = SharedSequence<DriverSharingStrategy, String>
        var viewModel: ViewModelType!
        let disposeBag = DisposeBag()
        
        // MARK:- view elems
        let avatarView = UIImageView()
        let ratingLabel = UILabel()
        let balanceLabel = UILabel()

        // MARK:- init
        static func create(with viewModel: ViewModelType) -> UIView {
            let view = InnerView()
            view.configure(with: viewModel)
            return view
        }

        func configure(with viewModel: ViewModelType) {
            self.setupView(with: viewModel)
            self.viewModel = viewModel
        }

        // MARK:- setuppers
        func setupView(with viewModel: ViewModelType) {
            self.backgroundColor = UIColor.view.standart.background
            self.round(radius: 15)

            let parentStackView = UIStackView()
            parentStackView.spacing = 15.0
            parentStackView.axis = .vertical
            
            self.addSubview(parentStackView)
            parentStackView.snp.makeConstraints { make in
                make.left.right.top.equalTo(self).inset(20)
            }

            parentStackView.arrangedSubviews.forEach({ parentStackView.removeArrangedSubview($0)})
            let accountInfoView = UIView()
            parentStackView.addArrangedSubview(accountInfoView)
            fill(withAccountInfo: accountInfoView, withAccountInfoOf: viewModel.output.user)
            fill(stackView: parentStackView, withAccountInfoOf: viewModel.output.user)
        }
        
        fileprivate func fill(stackView view: UIStackView, withAccountInfoOf user: Observable<Models.User>) {
            let nameObservable = driver(of: user, map: { String.getNameInitials(name: $0.name, surname: $0.surname) })
            let mailObservable = driver(of: user, map: { $0.email })
            let birtdayObservable = driver(of: user, optionalMap: { $0.birthday })
            let registrationObservable = driver(of: user, map: {
                $0.registerDate.getString(with: .date)
            })

            view.addArrangedSubview(accentLabel(withText: "Личная информация"))
            view.addArrangedSubview(infoLine(withTitle: "Почта", subtitleDriver: mailObservable))
            view.addArrangedSubview(infoLine(withTitle: "ФИО", subtitleDriver: nameObservable))
            view.addArrangedSubview(infoLine(withTitle: "День рождения", subtitleDriver: birtdayObservable))
            view.addArrangedSubview(infoLine(withTitle: "Дата регистрации", subtitleDriver: registrationObservable))
            view.addArrangedSubview(accentLabel(withText: "Место работы"))
            view.addArrangedSubview(infoLine(withTitle: "Организация", subtitle: "СИБУР-ХИМПРОМ"))
        }
                
        fileprivate func fill(withAccountInfo view: UIView, withAccountInfoOf user: Observable<Models.User>) {
            let ratingPlaceObservable = driver(of: user, optionalMap: { String($0.points) })
            let balanceObservable = driver(of: user, map: { String($0.balance) })
            
            view.addSubview(avatarView)
            avatarView.image = UIImage.defaultAvatar
            avatarView.contentMode = .scaleAspectFill
            avatarView.layer.masksToBounds = false
            avatarView.clipsToBounds = true
            avatarView.layer.cornerRadius = 32
            
            avatarView.snp.makeConstraints { make in
                make.width.height.equalTo(64)
                make.left.top.bottom.equalTo(view)
            }
                    
            let quickInfo = UIView()
            view.addSubview(quickInfo)
            quickInfo.snp.makeConstraints { make in
                make.left.equalTo(avatarView.snp.right).offset(15)
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(view)
            }
            
            let ratingDescription = UILabel()
            ratingDescription.text = "Набрано очков:"
            ratingDescription.font = UIFont.systemFont(ofSize: 14)
            let ratingStackView = UIStackView()
            ratingPlaceObservable.drive(ratingLabel.rx.text).disposed(by: disposeBag)
            ratingLabel.font = UIFont.boldSystemFont(ofSize: 14)
            ratingStackView.addArrangedSubview(ratingDescription)
            ratingStackView.addArrangedSubview(ratingLabel)
            ratingStackView.alignment = .leading
            ratingStackView.distribution = .equalSpacing
            ratingStackView.spacing = 5.0
            ratingStackView.axis = .horizontal
            quickInfo.addSubview(ratingStackView)
            ratingStackView.snp.makeConstraints { make in
                make.left.top.equalTo(quickInfo)
                make.right.lessThanOrEqualTo(quickInfo)
            }
            
            balanceObservable.drive(balanceLabel.rx.text).disposed(by: disposeBag)
            balanceLabel.font = UIFont.boldSystemFont(ofSize: 17)
            let balanceDescription = UILabel()
            balanceDescription.text = "Баланс:"
            balanceDescription.font = UIFont.systemFont(ofSize: 14)
            balanceLabel.font = UIFont.systemFont(ofSize: 14)
            let currencyImage = UIImageView(image: UIImage.money)
            let balanceStackView = UIStackView(arrangedSubviews: [balanceDescription, balanceLabel, currencyImage])
            balanceStackView.alignment = .leading
            balanceStackView.distribution = .equalSpacing
            balanceStackView.spacing = 5.0
            balanceStackView.axis = .horizontal
            quickInfo.addSubview(balanceStackView)
            balanceStackView.snp.makeConstraints { make in
                make.left.bottom.equalTo(quickInfo)
                make.top.equalTo(ratingLabel.snp.bottom)
                make.right.lessThanOrEqualTo(quickInfo)
            }
        }

        // MARK:- helpers
        func driver(of observable: Observable<Models.User>,
                              map: @escaping (Models.User) -> (String))
            ->  StringDriver {
            return observable.asDriver(onErrorJustReturn: Models.User.empty()).map { mappedUser in
                return map(mappedUser)
            }
        }
        
        func driver(of observable: Observable<Models.User>,
                              optionalMap: @escaping (Models.User) -> (String?))
            ->  StringDriver {
            return observable.asDriver(onErrorJustReturn: Models.User.empty()).map { mappedUser in
                return optionalMap(mappedUser) ?? "не заполнено"
            }
        }

        func infoLine(withTitle title: String, subtitleDriver: StringDriver) -> UIView {
            let titleLabel = label(withText: title)
            let valueLabel = boldLabel(withText: "")
            subtitleDriver.drive(valueLabel.rx.text).disposed(by: disposeBag)
            
            let parentView = UIView()
            
            parentView.addSubview(titleLabel)
            parentView.addSubview(valueLabel)
            
            titleLabel.snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
                make.left.equalToSuperview().inset(15)
            }
            
            valueLabel.snp.makeConstraints { make in
                make.top.bottom.right.equalToSuperview()
                make.left.equalTo(titleLabel.snp.right)
            }
            
            return parentView
        }

        func infoLine(withTitle title: String, subtitle: String) -> UIView {
            let titleLabel = label(withText: title)
            let valueLabel = boldLabel(withText: subtitle)
            let parentView = UIView()
            
            parentView.addSubview(titleLabel)
            parentView.addSubview(valueLabel)
            
            titleLabel.snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
                make.left.equalToSuperview().inset(15)
            }
            
            valueLabel.snp.makeConstraints { make in
                make.top.bottom.right.equalToSuperview()
                make.left.equalTo(titleLabel.snp.right)
            }
            
            return parentView
        }

        func accentLabel(withText text: String) -> UILabel {
            let label = UILabel()
            label.text = text
            label.textAlignment = .left
            label.textColor = UIColor.view.standart.accent
            label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
            return label
        }

        func boldLabel(withText text: String) -> UILabel {
            let label = UILabel()
            label.text = text
            label.textAlignment = .right
            label.font = UIFont.boldSystemFont(ofSize: 14)
            return label
        }
        
        func label(withText text: String) -> UILabel {
            let label = UILabel()
            label.text = text
            label.textAlignment = .left
            label.font = UIFont.systemFont(ofSize: 14)
            return label
        }

    }
}
