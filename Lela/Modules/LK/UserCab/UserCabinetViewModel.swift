//
//  UserCabinetViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 29/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Cabinet.User {
    class ViewModel: ViewModelProtocol {
        struct Input {}
        
        struct Output {
            let user: Observable<Models.User>
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output
        
        // MARK: - Private propertiess
        private let repo = UserListRepository()
        
        private let userSubject = ReplaySubject<Models.User>.create(bufferSize: 1)
        private let disposeBag = DisposeBag()
        
        // MARK: - Init and deinit
        init(userId: Int) {
            input = Input()
            output = Output(user: userSubject.asObservable())
            
            repo.getUsers().map { (users) -> Models.User? in
                return users.filter { $0.id == userId }.first
            }
            .subscribe(onNext: { [weak self] (user) in
                guard let sUser = user else { return }
                self?.userSubject.onNext(sUser)
            })
            .disposed(by: disposeBag)
        }
    }
}
