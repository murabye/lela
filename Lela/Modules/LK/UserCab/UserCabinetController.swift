//
//  UserCabinetController.swift
//  Lela
//
//  Created by Влада Кузнецова on 29/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit

extension Module.Cabinet.User {
    class Controller: ViewController, ControllerType  {
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!

        private var profileView: InnerView!
        
        static func create(with viewModel: ViewModelType) -> UIViewController {
            let controller = Controller()
            controller.viewModel = viewModel
            return controller
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }

        func configure(with viewModel: ViewModel) {
            guard let profileV = InnerView.create(with: viewModel) as? InnerView else {return}
            profileView = profileV
        }

        func setupView() {
            self.view.addSubview(profileView)
            profileView.snp.makeConstraints{ make in
                make.edges.equalToSuperview()
                make.height.greaterThanOrEqualTo(400)
            }
        }

    }
}

