//
//  UserRedoRepository.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol RedoProfileRepositoryProtocol {
    func redo(to: Models.User) -> Observable<Models.User>
}

class RedoProfileRepository: RedoProfileRepositoryProtocol {
    func redo(to: Models.User) -> Observable<Models.User> {
        return NetworkManager
            .requestObservable(from: AdressesProvider.changeUser.value(),
                                                method: .post,
                                                body: to)
    }
}

