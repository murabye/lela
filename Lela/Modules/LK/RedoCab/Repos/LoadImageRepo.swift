//
//  LoadImageRepo.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol LoadImageRepositoryProtocol {
    func load(image: UIImage) -> Observable<Models.File>
}

class LoadImageRepository: LoadImageRepositoryProtocol {
    func load(image: UIImage) -> Observable<Models.File> {
        return PublishSubject<Models.File>
            .from([Models.File(id: 0, url: "https://sun9-68.userapi.com/c844520/v844520084/23d3d/LG05L0s3_HQ.jpg")])
            .asObservable()
    }
}

