//
//  RedoCabinetViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Cabinet.Redo {
    class ViewModel: ViewModelProtocol {
        struct Input {
            let avatar: AnyObserver<UIImage>
            let name: AnyObserver<String>
            let birthday: AnyObserver<String>
            let endEditing: AnyObserver<Void>
            let imagePickInit: AnyObserver<Void>
        }
        
        struct Output {
            let user: Observable<Models.User>
            let loadingStarted: Observable<Void>
            let avatar: Observable<UIImage>
            let imagePickInit: Observable<Void>
            let close: Observable<Void>
        }

        // MARK: - Public properties
        let input: Input
        let output: Output

        // MARK: - Private propertiess
        private let userSubject = ReplaySubject<Models.User>.create(bufferSize: 1)
        private let avatarSubject = PublishSubject<UIImage>()
        private let nameSubject = PublishSubject<String>()
        private let birthdaySubject = PublishSubject<String>()
        private let endEditingSubject = PublishSubject<Void>()
        private let closeSubject = PublishSubject<Void>()
        private let imagePickerShow = PublishSubject<Void>()
        private let close = PublishSubject<Void>()

        private let service = Service()
        private let disposeBag = DisposeBag()

        struct UserChanges {
            let name: String
            let birthday: String
        }
        
        struct RedoModel {
            let user: Models.User
            let avatar: UIImage
        }
        
        private var userChanges: Observable<UserChanges> {
            return Observable.combineLatest(
                nameSubject.asObservable(),
                birthdaySubject.asObservable()
            ) { (name, birthday) in
                return UserChanges(name: name, birthday: birthday)
            }
        }
        
        private var newUserModel: Observable<Models.User> {
            return Observable.combineLatest(
                userSubject.asObservable(),
                userChanges
            ) { (user, changes) in
                return Models.User(id: user.id, avatarURL: user.avatarURL, name: changes.name, surname: user.name, birthday: changes.birthday, balance: user.balance, points: user.points, email: user.email, registerDate: user.registerDate, role: user.role, orgInfo: user.orgInfo)
            }
        }
        
        private var registerModel: Observable<RedoModel> {
            return Observable.combineLatest(
                avatarSubject.asObservable(),
                newUserModel
            ) { (avatar, user) in
                return RedoModel(user: user, avatar: avatar)
            }
        }
        
        // MARK: - Init and deinit
        init() {
            input = Input(avatar: avatarSubject.asObserver(),
                          name: nameSubject.asObserver(),
                          birthday: birthdaySubject.asObserver(),
                          endEditing: endEditingSubject.asObserver(),
                          imagePickInit: imagePickerShow.asObserver())
            
            output = Output(user: userSubject.asObservable(),
                            loadingStarted: endEditingSubject.asObservable(),
                            avatar: avatarSubject.asObservable(),
                            imagePickInit: imagePickerShow.asObservable(),
                            close: close.asObservable()) // TODO: CLOSE
            
            service.getUser().subscribe(onNext: { [weak self] (user) in
                self?.userSubject.onNext(user)
            }).disposed(by: disposeBag)
            
            endEditingSubject
                .withLatestFrom(registerModel)
                .flatMapLatest { redoModel in
                    return self.service.redo(user: redoModel.user, set: redoModel.avatar)
            }.subscribe(onNext: { [weak self] (user) in
                self?.userSubject.onNext(user)
                self?.closeSubject.onNext(())
            }).disposed(by: disposeBag)
        }
    }
}
