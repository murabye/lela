//
//  RedoCabinetInnerView.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit

extension Module.Cabinet.Redo {
    class ProfileView: UIView, ViewType {
        typealias ViewModelType = ViewModel
        typealias StringDriver = SharedSequence<DriverSharingStrategy, String>

        var datePickerDelegate: DatePickerDelegate! = nil
        
        var viewModel: ViewModelType!
        let disposeBag = DisposeBag()
        
        // MARK:- view elems
        let avatarView: UIImageView =  {
            let imageView = UIImageView()
            imageView.image = UIImage.defaultAvatar
            imageView.contentMode = .scaleAspectFill
            imageView.layer.masksToBounds = false
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = 32
            return imageView
        }()
        
        let avatarLoadButton: UIButton = {
            let button = Component.button
            button.hero.id = Module.Cabinet.Constants.ava
            button.layer.cornerRadius = 32
            button.setImage(UIImage.photo, for: .normal)
            button.tintColor = UIColor.white
            button.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            return button
        }()

        
        let ratingLabel: UILabel = {
            let ratingLabel = UILabel()
            ratingLabel.font = UIFont.boldSystemFont(ofSize: 14)
            ratingLabel.hero.id = "ratingLabel"
            return ratingLabel
        }()
        
        let balanceLabel: UILabel = {
            let balanceLabel = UILabel()
            balanceLabel.hero.id = "balanceLabel"
            balanceLabel.font = UIFont.systemFont(ofSize: 14)
            return balanceLabel
        }()
        
        let doneButton: UIButton = {
            let button = Component.button
            button.hero.id = Module.Cabinet.Constants.endButton
            button.setTitle("Готово", for: .normal)
            button.round(radius: 22)
            return button
        }()
        
        let activityIndicatorViewContainer = UIView()
        let activityIndicatorView = UIActivityIndicatorView()
            
        // MARK:- init
        static func create(with viewModel: ViewModelType) -> UIView {
            let view = ProfileView()
            view.hero.id = "ProfileView"
            view.configure(with: viewModel)
            return view
        }

        func configure(with viewModel: ViewModelType) {
            self.viewModel = viewModel
            self.setupView(with: viewModel)

            viewModel.output.avatar.subscribe(onNext: { [weak self] (avatar) in
                self?.avatarView.image = avatar
            }).disposed(by: disposeBag)
            
            viewModel.output.loadingStarted.subscribe(onNext: { [weak self] () in
                self?.doneButton.showLoading(isWhite: true)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
                .disposed(by: disposeBag)
            
            doneButton.rx.tap.asObservable()
                .bind(to: viewModel.input.endEditing)
                .disposed(by: disposeBag)
            
            avatarLoadButton.rx.tap.asObservable()
                .bind(to: viewModel.input.imagePickInit)
                .disposed(by: disposeBag)
        }
        
        // MARK:- setuppers
        func setupView(with viewModel: ViewModelType) {
            makeBaseView()
            
            let parentStackView = UIStackView()
            parentStackView.spacing = 15.0
            parentStackView.distribution = .equalSpacing
            parentStackView.axis = .vertical
            
            self.addSubview(parentStackView)
            parentStackView.snp.makeConstraints { make in
                make.left.right.top.equalTo(self).inset(20)
            }

            parentStackView.arrangedSubviews.forEach({ parentStackView.removeArrangedSubview($0)})
            let accountInfoView = UIView()
            parentStackView.addArrangedSubview(accountInfoView)
            fill(withAccountInfo: accountInfoView, withAccountInfoOf: viewModel.output.user)
            fill(stackView: parentStackView, withAccountInfoOf: viewModel.output.user)
            
            self.addSubview(doneButton)
            doneButton.snp.makeConstraints { make in
                make.top.equalTo(parentStackView.snp.bottom).offset(45)
                make.bottom.right.equalToSuperview().inset(20)
                make.width.equalTo(100)
                make.height.equalTo(44)
            }
        }
        
        fileprivate func fill(stackView view: UIStackView, withAccountInfoOf user: Observable<Models.User>) {
            let mailObservable = driver(of: user, map: { $0.email })
            let registrationObservable = driver(of: user, map: {
                $0.registerDate.getString(with: .date)
            })

            view.addArrangedSubview(accentLabel(withText: "Личная информация"))
            view.addArrangedSubview(infoLine(withTitle: "Почта", subtitleDriver: mailObservable, subtitleId: "mail"))
            view.addArrangedSubview(redoLine(withTitle: "ФИО", subtitleObserver: viewModel.input.name, subtitleId: Module.Cabinet.Constants.name))
            view.addArrangedSubview(dateRedoLine(withTitle: "День рождения", subtitleObserver: viewModel.input.birthday, subtitleId: Module.Cabinet.Constants.birthday))
            view.addArrangedSubview(infoLine(withTitle: "Дата регистрации", subtitleDriver: registrationObservable, subtitleId: "date"))
            view.addArrangedSubview(accentLabel(withText: "Место работы"))
            view.addArrangedSubview(infoLine(withTitle: "Организация", subtitle: "СИБУР-ХИМПРОМ"))
        }
                
        fileprivate func fill(withAccountInfo view: UIView, withAccountInfoOf user: Observable<Models.User>) {
            let ratingPlaceObservable = driver(of: user, optionalMap: { String($0.points) })
            let balanceObservable = driver(of: user, map: { String($0.balance) })
            
            view.addSubview(avatarView)
            view.addSubview(avatarLoadButton)
            
            avatarView.snp.makeConstraints { make in
                make.width.height.equalTo(64)
                make.left.top.bottom.equalTo(view)
            }
            
            avatarLoadButton.snp.makeConstraints { (make) in
                make.width.height.equalTo(64)
                make.left.top.bottom.equalTo(view)
            }
            
            let quickInfo = UIStackView()
            quickInfo.distribution = .equalSpacing
            quickInfo.alignment = .top
            quickInfo.axis = .vertical
            view.addSubview(quickInfo)
            quickInfo.snp.makeConstraints { make in
                make.left.equalTo(avatarView.snp.right).offset(15)
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(view)
            }
            
            let ratingDescription = UILabel()
            ratingDescription.hero.id = ratingDescription.text
            ratingDescription.text = "Набрано очков:"
            ratingDescription.hero.id = "ratingDescription"
            ratingDescription.font = UIFont.systemFont(ofSize: 14)
            let ratingStackView = UIStackView()
            ratingPlaceObservable.drive(ratingLabel.rx.text).disposed(by: disposeBag)
            ratingStackView.addArrangedSubview(ratingDescription)
            ratingStackView.addArrangedSubview(ratingLabel)
            ratingStackView.alignment = .leading
            ratingStackView.distribution = .equalSpacing
            ratingStackView.spacing = 5.0
            ratingStackView.axis = .horizontal
            quickInfo.addArrangedSubview(ratingStackView)
            
            balanceObservable.drive(balanceLabel.rx.text).disposed(by: disposeBag)
            let balanceDescription = UILabel()
            balanceDescription.text = "Баланс:"
            balanceDescription.hero.id = "balanceDescription"
            balanceDescription.font = UIFont.systemFont(ofSize: 14)
            let currencyImage = UIImageView(image: UIImage.money)
            currencyImage.hero.id = "currencyImage"
            let balanceStackView = UIStackView(arrangedSubviews: [balanceDescription, balanceLabel, currencyImage])
            balanceStackView.alignment = .leading
            balanceStackView.distribution = .equalSpacing
            balanceStackView.spacing = 5.0
            balanceStackView.axis = .horizontal
            quickInfo.addArrangedSubview(balanceStackView)
        }
    }
}

// MARK:- helpers
fileprivate extension Module.Cabinet.Redo.ProfileView {
    func driver(of observable: Observable<Models.User>,
                          map: @escaping (Models.User) -> (String))
        ->  StringDriver {
        return observable.asDriver(onErrorJustReturn: Models.User.empty()).map { mappedUser in
            return map(mappedUser)
        }
    }
    
    func driver(of observable: Observable<Models.User>,
                          optionalMap: @escaping (Models.User) -> (String?))
        ->  StringDriver {
        return observable.asDriver(onErrorJustReturn: Models.User.empty()).map { mappedUser in
            return optionalMap(mappedUser) ?? "не заполнено"
        }
    }
}

// MARK:- view element fabric
fileprivate extension Module.Cabinet.Redo.ProfileView {
    func infoLine(withTitle title: String, subtitleDriver: StringDriver, subtitleId: String? = nil) -> UIView {
        let titleLabel = label(withText: title)
        let valueLabel = boldLabel(withText: "")
        valueLabel.hero.id = subtitleId
        subtitleDriver.drive(valueLabel.rx.text).disposed(by: disposeBag)
        
        let parentView = UIView()
        
        parentView.addSubview(titleLabel)
        parentView.addSubview(valueLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(15)
        }
        
        valueLabel.snp.makeConstraints { make in
            make.top.bottom.right.equalToSuperview()
            make.left.equalTo(titleLabel.snp.right)
        }
        
        return parentView
    }

    func redoLine(withTitle title: String, subtitleObserver: AnyObserver<String>, subtitleId: String? = nil) -> UIView {
        let titleLabel = label(withText: title)
        let valueLabel = Component.textField
        valueLabel.textAlignment = .right
        valueLabel.autocorrectionType = .no
        valueLabel.hero.id = subtitleId
        valueLabel.rx.text.asObservable()
            .ignoreNil()
            .subscribe(subtitleObserver)
            .disposed(by: disposeBag)

        let parentView = UIView()
        
        parentView.addSubview(titleLabel)
        parentView.addSubview(valueLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(15)
        }
        
        valueLabel.snp.makeConstraints { make in
            make.top.bottom.right.equalToSuperview()
            make.height.equalTo(40)
            make.left.equalTo(titleLabel.snp.right).offset(15)
        }
        
        return parentView
    }

    func dateRedoLine(withTitle title: String, subtitleObserver: AnyObserver<String>, subtitleId: String? = nil) -> UIView {
        let titleLabel = label(withText: title)
        let variablesForTextfield = Component.dateTextField()
        datePickerDelegate = variablesForTextfield.delegate
        let valueLabel = variablesForTextfield.textfield
        valueLabel.textAlignment = .right
        valueLabel.autocorrectionType = .no
        valueLabel.hero.id = subtitleId
        valueLabel.rx.text.asObservable()
            .ignoreNil()
            .subscribe(subtitleObserver)
            .disposed(by: disposeBag)

        let parentView = UIView()
        
        parentView.addSubview(titleLabel)
        parentView.addSubview(valueLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(15)
        }
        
        valueLabel.snp.makeConstraints { make in
            make.top.bottom.right.equalToSuperview()
            make.height.equalTo(40)
            make.left.equalTo(titleLabel.snp.right).offset(15)
        }
        
        return parentView
    }

    
    func infoLine(withTitle title: String, subtitle: String) -> UIView {
        let titleLabel = label(withText: title)
        let valueLabel = boldLabel(withText: subtitle)
        let parentView = UIView()
        
        parentView.addSubview(titleLabel)
        parentView.addSubview(valueLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(15)
        }
        
        valueLabel.snp.makeConstraints { make in
            make.top.bottom.right.equalToSuperview()
            make.left.equalTo(titleLabel.snp.right)
        }
        
        return parentView
    }

    func accentLabel(withText text: String) -> UILabel {
        let label = UILabel()
        label.hero.id = text.isEmpty ? nil : text
        label.text = text
        label.textAlignment = .left
        label.textColor = UIColor.view.standart.accent
        label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        return label
    }

    func boldLabel(withText text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.hero.id = text.isEmpty ? nil : text
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }
    
    func label(withText text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.hero.id = text.isEmpty ? nil : text
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }
}
