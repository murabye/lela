//
//  RedoCabinetController.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

extension Module.Cabinet.Redo {
    class Controller: ViewController, ControllerType, RxMediaPickerDelegate {
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!
        private let disposeBag = DisposeBag()

        private var profileView: ProfileView!
        
        var picker: RxMediaPicker!

        override func viewDidLoad() {
            super.viewDidLoad()
            picker = RxMediaPicker(delegate: self)
            configure(with: viewModel)
            setupView()
        }

         func configure(with viewModel: ViewModel) {
             guard let lkProfileView = ProfileView.create(with: viewModel) as? ProfileView else {return}
             profileView = lkProfileView
            
            viewModel.output.close.subscribe(onNext: { () in
                    self.dismiss(animated: true, completion: nil)
                })
                .disposed(by: disposeBag)
            
            
            viewModel.output.imagePickInit.flatMap { () in
                return self.picker.selectImage(editable: true)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (image, editedImage) in
                let img = editedImage ?? image
                self.viewModel.input.avatar.onNext(img)
            })
            .disposed(by: disposeBag)
         }

         func setupView() {
             let scrollView = self.makeBaseScrollController(named: "Личный кабинет")

             scrollView.addSubview(profileView)
             profileView.snp.makeConstraints{ make in
                 make.edges.equalTo(scrollView).inset(15)
                 make.width.equalTo(scrollView).inset(15)
             }
         }
         
         static func create(with viewModel: ViewModelType) -> UIViewController {
             let controller = Controller()
             controller.viewModel = viewModel
             return controller
         }
        
        func present(picker: UIImagePickerController) {
            self.present(picker, animated: true, completion: nil)
        }
        
        func dismiss(picker: UIImagePickerController) {
            self.dismiss(animated: true, completion: nil)
        }
     }
 }

