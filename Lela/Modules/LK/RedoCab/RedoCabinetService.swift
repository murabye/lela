//
//  RedoCabinetService.swift
//  Lela
//
//  Created by Влада Кузнецова on 30/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift

protocol RedoProfileServiceProtocol {
    func redo(user: Models.User) -> Observable<Models.User>
    func redo(user: Models.User, set avatar: UIImage) -> Observable<Models.User>
    func getUser() -> Observable<Models.User>
}

extension Module.Cabinet.Redo {
    class Service: RedoProfileServiceProtocol {
        let loadImageRepo = LoadImageRepository()
        let redoProfileRepo = RedoProfileRepository()
        let lkRepo = LKRepository()
        private let disposeBag = DisposeBag()
     }
 }

extension Module.Cabinet.Redo.Service {
    func getUser() -> Observable<Models.User> {
        return lkRepo.getCurrentUser()
    }
    
    func redo(user: Models.User) -> Observable<Models.User> {
        return redoProfileRepo.redo(to: user)
    }
    
    func redo(user: Models.User, set avatar: UIImage) -> Observable<Models.User> {
        return loadImageRepo.load(image: avatar)
            .flatMap({ (file) -> Observable<Models.User> in
                var userCopy = user
                userCopy.avatarURL = file.url
                return self.redo(user: userCopy)
            })
    }
}
