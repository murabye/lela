//
//  CabinetModule.swift
//  Lela
//
//  Created by Влада Кузнецова on 29/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation

extension Module {
    class Cabinet {
        class Personal {}
        class User {}
        class Redo {}
    }
}
