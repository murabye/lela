//
//  CategoryAddService.swift
//  Lela
//
//  Created by Вова Петров on 12.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol CategoryAddServiceProtocol {
    func create(category: Models.EventCategory) -> Observable<Models.EventCategory>
}

extension Module.Category.Add {
    class Service: CategoryAddServiceProtocol {
        
        func create(category: Models.EventCategory) -> Observable<Models.EventCategory> {
            return NetworkManager.requestObservable(from: AdressesProvider.createCategory.value(),
                                                    method: .post,
                                                    body: category).do(afterNext: { (_) in
                                                        Radio.EventList.Say.needUpdate()
                                                    })
        }
    }
}
