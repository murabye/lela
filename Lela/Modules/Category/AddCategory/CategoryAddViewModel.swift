//
//  CategoryAddViewModel.swift
//  Lela
//
//  Created by Вова Петров on 12.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Category.Add {
    class ViewModel: ViewModelProtocol {
        var db = DisposeBag()
        var service: CategoryAddServiceProtocol
        
        struct Input {
            var title: AnyObserver<String>
            var imageId: AnyObserver<Assets.Icon>
            var createAction: AnyObserver<Void>
        }
        
        struct Output {
            var isCreateAvailable: Observable<Bool>
            var iconIdArray: Observable<[Assets.Icon]>
            var category: Observable<Models.EventCategory>
            var isLoading: Observable<Bool>
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output
        
        let titleSubject = PublishSubject<String>()
        let imageIdSubject = PublishSubject<Assets.Icon>()
        let createSubject = PublishSubject<Void>()
        
        let categoryOutputSubject = PublishSubject<Models.EventCategory>()
        let iconIdArraySubject = Observable.from([Assets.Icon.allCases])
        let isCreateAvailable = PublishSubject<Bool>()
        let isLoadingSubject = PublishSubject<Bool>()
        
        var categoryInput: Observable<Models.EventCategory> {
            return Observable.combineLatest(titleSubject.asObservable(),
                                            imageIdSubject.asObservable()) { (title, iconId) in
                                                return Models.EventCategory(title: title, iconId: iconId.rawValue, id: nil, events: nil)
            }
        }
        
        // MARK: - Init and deinit
        init(_ service: CategoryAddServiceProtocol) {
            self.service = service
            input = Input(
                title: titleSubject.asObserver(),
                imageId: imageIdSubject.asObserver(),
                createAction: createSubject.asObserver()
            )
            output = Output(
                isCreateAvailable: isCreateAvailable.asObservable(),
                iconIdArray: iconIdArraySubject,
                category: categoryOutputSubject.asObservable(),
                isLoading: isLoadingSubject.asObservable()
            )
            
            Observable.combineLatest(titleSubject.asObservable(),
                                     imageIdSubject.asObservable())
                .map { $0.0.count > 0 }
                .subscribe(isCreateAvailable)
                .disposed(by: db)
            
            createSubject.withLatestFrom(categoryInput)
                .do(afterNext: {[weak self] (_) in self?.isLoadingSubject.onNext(true) })
                .flatMap { service.create(category: $0) }
                .subscribe(categoryOutputSubject)
                .disposed(by: db)
            
            categoryOutputSubject.subscribe {[weak self] (_) in
                self?.isLoadingSubject.onNext(false)
            }.disposed(by: db)
            
        }
    }
}
