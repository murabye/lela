//
//  CategoryAddIconCell.swift
//  Lela
//
//  Created by Вова Петров on 12.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit

class CategoryAddIconCell: UICollectionViewCell {
    static let id = "CategoryAddIconCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    var icon: UIImage? = nil
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear//UIColor.view.standart.background
        
        view.clipsToBounds = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.shadow.standart.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 3
        
        view.layer.cornerRadius = 4.0
        
        return view
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = false
        imageView.layer.masksToBounds = false
        imageView.layer.shadowColor = UIColor.shadow.standart.cgColor
        imageView.layer.shadowOpacity = 1
        imageView.layer.shadowOffset = .zero
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    let selectedView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.Icon.checked.image
        return imageView
    }()
    
    func setupViews() {
        self.backgroundColor = .clear
        contentView.addSubview(bubbleView)

        bubbleView.hero.modifiers = [.spring(stiffness: 250, damping: 25), .fade]

        bubbleView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 5))
        }
        
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(bubbleView)
        }
        
        contentView.addSubview(selectedView)
        selectedView.snp.makeConstraints { (make) in
            make.height.width.equalTo(15)
            make.right.top.equalToSuperview()
        }
        selectedView.isHidden = !isSelected
    }
    
    func configure(icon: Assets.Icon) {
        imageView.image = icon.image
        imageView.hero.id = icon.rawValue
    }
    
    override var isSelected: Bool {
        didSet {
            selectedView.isHidden = !isSelected
        }
    }
    
}


