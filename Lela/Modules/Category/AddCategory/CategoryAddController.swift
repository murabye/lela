//
//  CategoryAddController.swift
//  Lela
//
//  Created by Вова Петров on 12.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import SwiftEntryKit

extension Module.Category.Add {
    class Controller: UIViewController, ControllerType {
        
        typealias ViewModelType = ViewModel
        
        private var viewModel: ViewModelType!
        let db = DisposeBag()
        
        var titleField = Component.textField
        
        var mainTitleLabel: UILabel = {
            let label = UILabel()
            label.text = "Создать категорию"
            label.font = UIFont.systemFont(ofSize: 28.0, weight: .semibold)
            label.textColor = UIColor.text.standart.subtitle
            label.textAlignment = .center
            return label
        }()
        
        var titleLabel: UILabel = {
            let label = UILabel()
            label.text = "Название"
            label.font = UIFont.systemFont(ofSize: 14)
            label.textColor = UIColor.text.standart.subtitle
            return label
        }()
        
        var collectionView: UICollectionView = {
            let layout = BouncyLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: 50, height: 50)
            layout.minimumLineSpacing = 2.0
            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.register(CategoryAddIconCell.self, forCellWithReuseIdentifier: CategoryAddIconCell.id)
            collectionView.backgroundColor = .clear
            collectionView.showsVerticalScrollIndicator = false
            collectionView.showsHorizontalScrollIndicator = false
            return collectionView
        }()
        
        var createButton: ThemeManager.MainThemeButton = {
            let button = Component.button
            button.setTitle("СОЗДАТЬ", for: .normal)
            button.isEnabled = false
            return button
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            configureView()
            configure(with: viewModel)
        }
        
        override func viewDidAppear(_ animated: Bool) {
            titleField.becomeFirstResponder()
        }
        
        func configureView() {
            self.view.clipsToBounds = true
            self.view.layer.cornerRadius = 5
            
            self.view.addSubview(mainTitleLabel)
            mainTitleLabel.snp.makeConstraints { (make) in
                make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
                make.width.equalToSuperview()
                make.centerX.equalTo(self.view.snp.centerX)
                make.top.equalToSuperview().offset(20).priority(.medium)
            }
            
            self.view.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (make) in
                make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
                make.width.equalTo(270)
                make.centerX.equalTo(self.view.snp.centerX)
                make.top.equalTo(mainTitleLabel.snp.bottom).offset(15).priority(.medium)
            }
            
            titleField.placeholder = "Обязательно"
            titleField.returnKeyType = .continue
            titleField.autocorrectionType = .no
            titleField.autocapitalizationType = .none
            
            self.view.addSubview(titleField)
            titleField.snp.makeConstraints { (make) in
                make.width.equalTo(titleLabel.snp.width).offset(30)
                make.height.equalTo(40)
                make.top.equalTo(titleLabel.snp.bottom).offset(6).priority(.medium)
                make.centerX.equalTo(self.view.snp.centerX)
            }
            
            self.view.addSubview(collectionView)
            collectionView.snp.makeConstraints { (make) in
                make.width.equalToSuperview()
                make.height.equalTo(110)
                make.top.equalTo(titleField.snp.bottom).offset(10).priority(.medium)
                make.centerX.equalTo(self.view.snp.centerX)
            }
            
            self.view.addSubview(createButton)
            createButton.snp.makeConstraints { (make) in
                make.width.equalTo(titleField.snp.width)
                make.height.equalTo(40)
                make.top.equalTo(collectionView.snp.bottom).offset(30).priority(.medium)
                make.centerX.equalTo(self.view.snp.centerX)
                make.bottom.equalToSuperview().inset(15).priority(.medium)
            }
        }
        
        func configure(with viewModel: Module.Category.Add.ViewModel) {
            titleField.rx.text.asObservable()
                .ignoreNil()
                .subscribe(viewModel.input.title)
                .disposed(by: db)
            
            createButton.rx.tap.subscribe(viewModel.input.createAction).disposed(by: db)
            
            Observable.combineLatest(collectionView.rx.itemSelected.asObservable(),
                                     viewModel.output.iconIdArray.asObservable()) { (selectedIndex, iconIdArray) in
                    return iconIdArray[selectedIndex.row]
                }.subscribe(viewModel.input.imageId)
                .disposed(by: db)
            
            viewModel.output.iconIdArray
                .bind(to: collectionView.rx.items(cellIdentifier: CategoryAddIconCell.id)) {index, model, cell in
                    let cell = cell as! CategoryAddIconCell
                    cell.configure(icon: model)
                }
                .disposed(by: db)
            
            viewModel.output.isCreateAvailable
                .subscribe(onNext: {[unowned self] (available) in
                    self.createButton.isEnabled = available
                })
                .disposed(by: db)
            
            viewModel.output.isLoading
                .subscribe(onNext: {[weak self] (isLoading) in
                    self?.createButton.show(loading: isLoading)
                }).disposed(by: db)
            
            viewModel.output.category
                .subscribe(onNext: { (category) in
                    SwiftEntryKit.dismiss()
                }, onError: { (error) in
                    self.showAlert(title: "Ошибка", text: error.localizedDescription) {
                        SwiftEntryKit.dismiss()
                    }
                }).disposed(by: db)
        }
    }
}

extension Module.Category.Add.Controller {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = Module.Category.Add.Controller()
        controller.viewModel = viewModel
        return controller
    }
}
