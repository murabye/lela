//
//  MathStatisticViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 24/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift

extension Module.Statistic.Math {
    class ViewModel: ViewModelProtocol {
        struct Input {}
        struct Output {
            let mathStat: Observable<Model>
        }

        // MARK: - Public properties
        let input: Input
        let output: Output
        
        // MARK: - Private properties
        private let mathStatSubject = ReplaySubject<Model>.create(bufferSize: 1)
        private let disposeBag = DisposeBag()
        
        // MARK: - Init and deinit
        init(_ statistic: Model) {
            input = Input()
            output = Output(mathStat: mathStatSubject.asObservable())
            mathStatSubject.onNext(statistic)
        }
        
        deinit {
            print("\(self) dealloc")
        }
    }
}
