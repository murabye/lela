//
//  MathStatisticController.swift
//  Lela
//
//  Created by Влада Кузнецова on 24/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

extension Module.Statistic.Math {
    final class Controller: ViewController, ControllerType {
         let mediumDescription = "Средняя температура в больнице 36.6"
         let medianaDescription = "Такое число, что ровно половина из значений больше него, а другая половина меньше него"
         let mediumLinearDeviationDescription = "Поясняет, насколько в среднем сильно разбросаны значения от среднего"

        // MARK:- private
        private let disposeBag = DisposeBag()
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!

        // MARK:- inner views
        let nameLabel: UILabel = {
            let desc = UILabel()
            desc.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            desc.textColor = UIColor.text.standart.important
            desc.numberOfLines = 0
            return desc
        }()
        let sumLabel = Component.label(text: nil, weight: nil, size: nil)
        let maxLabel = Component.label(text: nil, weight: nil, size: nil)
        let minLabel = Component.label(text: nil, weight: nil, size: nil)
        let countLabel = Component.label(text: nil, weight: nil, size: nil)
        let mediumLabel = Component.label(text: nil, weight: .medium, size: nil)
        let medianaLabel = Component.label(text: nil, weight: .medium, size: nil)
        let deviationLabel = Component.label(text: nil, weight: .medium, size: nil)
        
        // MARK:- lifecycle && initializers
        static func create(with viewModel: ViewModelType) -> UIViewController {
            let controller = Module.Statistic.Math.Controller()
            controller.viewModel = viewModel
            return controller
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }
        
        func configure(with viewModel: ViewModel) {
            self.viewModel = viewModel
            
            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Статистика по характеристике \(stat.valueName.lowercased())"
                }
                .asDriver(onErrorJustReturn: "не определено")
                .drive(nameLabel.rx.text)
                .disposed(by: disposeBag)
            
            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Сумма: \(Int(stat.sum.rounded()))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(sumLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Максимум: \(String(Int(stat.max.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(maxLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Минимум: \(String(Int(stat.min.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(minLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Всего данных: \(String(Int(stat.count.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(countLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Среднее: \(String(Int(stat.medium.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(mediumLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Медиана: \(String(Int(stat.mediana.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(medianaLabel.rx.text)
                .disposed(by: disposeBag)

            viewModel.output.mathStat
                .map { (stat: ViewModel.Model) -> String in
                    return "Отклонение: \(String(Int(stat.mediumLinearDeviation.rounded())))"
                }
                .asDriver(onErrorJustReturn: "?")
                .drive(deviationLabel.rx.text)
                .disposed(by: disposeBag)
        }

        func setupView() {
            let mediumDesc = Component.label(text: mediumDescription, weight: nil, size: nil)
            let medianaDesc = Component.label(text: medianaDescription, weight: nil, size: nil)
            let deviationDesc = Component.label(text: mediumLinearDeviationDescription, weight: nil, size: nil)
                    
            let mediumImg = UIImage.medium
            let medianImg = UIImage.median
            let deviationImg = UIImage.deviation
            
            let mainStat = Controller.line(with: [maxLabel, sumLabel])
            let maxMinLine = Controller.line(with: [minLabel, countLabel])
            
            let mediumInfo = Controller.verticalStack(with: [mediumLabel, mediumDesc])
            let medianaInfo = Controller.verticalStack(with: [medianaLabel, medianaDesc])
            let deviationInfo = Controller.verticalStack(with: [deviationLabel, deviationDesc])
            
            let mediumLine = Controller.lineWithImage(img: mediumImg, data: mediumInfo)
            let medianLine = Controller.lineWithImage(img: medianImg, data: medianaInfo)
            let deviationLine = Controller.lineWithImage(img: deviationImg, data: deviationInfo)
            
            let mainStack = UIStackView(arrangedSubviews: [nameLabel, mainStat, maxMinLine, mediumLine, medianLine, deviationLine])
            mainStack.axis = .vertical
            mainStack.distribution = .fill
            mainStack.alignment = .fill
            mainStack.spacing = 15
            mainStack.setCustomSpacing(0, after: mainStat)

            self.view.addSubview(mainStack)
            mainStack.snp.makeConstraints { (make) in
                make.edges.equalToSuperview().inset(15)
            }
        }

        static func lineWithImage(img: UIImageView, data: UIView) -> UIStackView {
            let stack = UIStackView(arrangedSubviews: [img, data])
            stack.axis = .horizontal
            stack.distribution = .fillProportionally
            stack.alignment = .fill
            stack.spacing = 15
            img.widthAnchor.constraint(equalToConstant: 25).isActive = true
            img.contentMode = .scaleAspectFit
            img.setContentHuggingPriority(.defaultLow, for: .horizontal)
            img.setContentHuggingPriority(.defaultLow, for: .vertical)
            img.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
            img.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
            return stack
        }
        
        static func line(with views: [UIView]) -> UIStackView {
            let stack = UIStackView(arrangedSubviews: views)
            stack.axis = .horizontal
            stack.distribution = .fillEqually
            return stack
        }
        
        static func verticalStack(with views: [UIView]) -> UIStackView {
            let stack = UIStackView(arrangedSubviews: views)
            stack.axis = .vertical
            stack.distribution = .equalSpacing
            stack.alignment = .firstBaseline
            stack.spacing = 0
            for view in views {
                view.widthAnchor.constraint(equalTo: stack.widthAnchor, multiplier: 1.0).isActive = true
            }
            return stack
        }
    }
}
