//
//  MathStatisticService.swift
//  Lela
//
//  Created by Влада Кузнецова on 24/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
extension Module.Statistic.Math.ViewModel {
    struct Model {
        /// среднее значение
        let medium: CGFloat
    
        /// медиана
        let mediana: CGFloat
    
        /// среднее линейное отклонение
        let mediumLinearDeviation: CGFloat
        
        let sum: CGFloat
        let max: CGFloat
        let min: CGFloat
        let count: CGFloat
        
        let valueName: String
        
        init(valueName: String, from array: [CGFloat]) {
            self.valueName = valueName
            
            let workArray = array.sorted()
            self.count = CGFloat(workArray.count)
            self.max = workArray.last ?? 0
            self.min = workArray.first ?? 0
    
            self.mediana = workArray.count % 2 == 0
                ? (workArray[workArray.count / 2 - 1] + workArray[workArray.count / 2]) / 2
                : workArray[workArray.count / 2]
    
            self.sum = workArray.reduce(0, { (res, newVal) -> CGFloat in
                return res + newVal
            })
    
            self.medium = sum / count
    
            let medium = self.medium
            let deviation = workArray.reduce(0) { (res, newVal) -> CGFloat in
                 return res + abs(newVal - medium)
            }
    
            self.mediumLinearDeviation = deviation / count
        }
    }
}
