//
//  LeaderListViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Store.Leaders {
    class ViewModel: ViewModelProtocol {
        struct Input {
            let search: AnyObserver<Void>
            let statistic: AnyObserver<Void>
            let searchText: AnyObserver<String?>
            let userSelected: AnyObserver<Int>
            let back: AnyObserver<Void>
        }
        
        struct Output {
            let users: Observable<[RatedUser]>
            let statisticData: Observable<[CGFloat]>
            let searchToggle: Observable<Void>
            let showUser: Observable<RatedUser>
            let close: Observable<Void>
        }
        
        struct RatedUser {
            let name: String
            let balance: Int
            let isSelf: Bool
            let number: Int
            let id: Int
        }

        // MARK: - Public properties
        let input: Input
        let output: Output

        // MARK: - Private properties
        private let originalUserSubject = ReplaySubject<[RatedUser]>.create(bufferSize: 1)
        private let userSubject = ReplaySubject<[RatedUser]>.create(bufferSize: 1)
        private let searchSubject = PublishSubject<Void>()
        private let statisticSubject = PublishSubject<Void>()
        private let statisticDataSubject = PublishSubject<[CGFloat]>()
        private let searchTextSubject = PublishSubject<String?>()
        private let closeSubject = PublishSubject<Void>()

        private let userSelectedSubject = PublishSubject<Int>()
        private let showUserSubject = PublishSubject<RatedUser>()
        
        private let disposeBag = DisposeBag()

        // MARK: - Init and deinit
        init(_ service: Service) {
            input = Input(search: searchSubject.asObserver(),
                          statistic: statisticSubject.asObserver(),
                          searchText: searchTextSubject.asObserver(),
                          userSelected: userSelectedSubject.asObserver(),
                          back: closeSubject.asObserver())
            output = Output(users: userSubject.asObservable(),
                            statisticData: statisticDataSubject.asObservable(),
                            searchToggle: searchSubject.asObservable(),
                            showUser: showUserSubject.asObservable(),
                            close: closeSubject.asObserver())
            
            service.getUsers()
                .subscribe(onNext: { [weak self] (users) in
                    let newArr = users
                        .enumerated()
                        .map { (index: Int, user: Models.User) -> RatedUser in
                        return RatedUser(name: user.email,
                                         balance: user.balance,
                                         isSelf: user.email == UserService.currentUser?.email ?? "?",
                                         number: index + 1,
                                         id: user.id)
                    }
                    self?.originalUserSubject.onNext(newArr)
                    }, onError: { [weak self] error in
                        self?.originalUserSubject.onError(error)
                })
                .disposed(by: disposeBag)
            
            self.originalUserSubject
                .subscribe(userSubject)
                .disposed(by: disposeBag)
            
            searchTextSubject
                .withLatestFrom(originalUserSubject) { (searchStr, users: [RatedUser]) -> ([RatedUser]) in
                    guard let search = searchStr,
                        search.count > 0
                        else { return users }
                    return users.filter { $0.name.lowercased().contains(search.lowercased()) }
                }.subscribe(userSubject)
                .disposed(by: disposeBag)
            
            statisticSubject.withLatestFrom(userSubject)
                .subscribe(onNext: { [weak self] (users) in
                    let balance = users.map { (usr) -> CGFloat in
                        return CGFloat(usr.balance)
                    }
                    
                    self?.statisticDataSubject.onNext(balance)
                })
                .disposed(by: disposeBag)
            
            userSelectedSubject.withLatestFrom(userSubject) {
                (index, users: [RatedUser]) -> (RatedUser) in
                return users[index]
            }
            .subscribe(showUserSubject)
            .disposed(by: disposeBag)
        }
        
        deinit {
            print("\(self) dealloc")
        }
    }
}
