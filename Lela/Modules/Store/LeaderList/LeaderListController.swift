//
//  LeaderListController.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import SwiftEntryKit

extension Module.Store.Leaders {
    final class Controller: ViewController, ControllerType {
                
        // MARK:- private
        private let disposeBag = DisposeBag()
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!

        // MARK:- inner views
        static let reusableID = "Module.Store.Leaders.Cell"
        public let userTable: UITableView = {
            let table = UITableView()
            table.backgroundColor = .clear
            table.separatorStyle = .none
            table.register(Cell.self, forCellReuseIdentifier: reusableID)
            table.showLoading(isWhite: false)
            return table
        }()
        
        let searchBar: UISearchBar = {
            let sb = UISearchBar()
            sb.placeholder = "Введите email пользователя"
            sb.isTranslucent = true
            sb.searchBarStyle = .minimal
            sb.keyboardType = .emailAddress
            return sb
        }()
        
        let statisticButton: UIButton = {
            let button = Component.invertedRoundButton
            button.setImage(UIImage.statistic, for: .normal)
            button.hero.modifiers = [.translate(x: -100, y: 0, z: 0)]
            return button
        }()
        
        let searchButton: UIButton = {
            let button = Component.invertedRoundButton
            button.setImage(UIImage(named: "search_image"), for: .normal)
            button.tintColor = UIColor.button.standart.standart.text
            button.hero.modifiers = [.translate(x: 100, y: 0, z: 0)]
            return button
        }()

        let backButton: UIButton = {
            let button = Component.accentHelpButton
            button.setImage(UIImage.back, for: .normal)
            button.setTitle("   Магазин подарков", for: .normal)
            button.hero.id = Module.Store.StoreConstants.balanceId
            return button
        }()
        
        // MARK:- lifecycle && initializers
        static func create(with viewModel: ViewModelType) -> UIViewController {
            let controller = Module.Store.Leaders.Controller()
            controller.viewModel = viewModel
            return controller
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }
        
        func configure(with viewModel: ViewModel) {
            self.viewModel = viewModel
            
            viewModel.output.users.subscribe(onNext: { [weak self] _ in
                self?.userTable.stopLoading()
                })
            .disposed(by: disposeBag)
            
            viewModel.output.users.bind(to: userTable.rx.items(cellIdentifier: Controller.reusableID)) { index, model, cell in
                let cell = cell as! Cell
                cell.setupViews()
                cell.user = model
                cell.selectionStyle = .none
            }
            .disposed(by: disposeBag)
                        
            viewModel.output.statisticData.subscribe(onNext: { (balanceArr) in
                let service = Module.Statistic.Math.ViewModel.Model(valueName: "Баланс", from: balanceArr)
                let vm = Module.Statistic.Math.ViewModel(service)
                let vc = Module.Statistic.Math.Controller.create(with: vm)
                SwiftEntryKit.display(entry: vc, using: self.setupFormPresets())
            }).disposed(by: disposeBag)
            
            viewModel.output.searchToggle
                .subscribe(onNext: { [weak self] (_) in
                    let isClosed = self?.searchBar.frame.height == 0
                    self?.searchBar.text = nil

//                    if isClosed {
//                        self?.searchBar.becomeFirstResponder()
//                    } else if self?.searchBar.isFirstResponder ?? false {
//                        self?.searchBar.resignFirstResponder()
//                    }
                    
                    UIView.animate(withDuration: 0.25) { [weak self] in
                        self?.searchBar.snp.updateConstraints { (make) in
                            make.height.equalTo(isClosed ? 44 : 0)
                        }
                        self?.view.layoutIfNeeded()
                    }
                })
                .disposed(by: disposeBag)
            
            viewModel.output.showUser
                .subscribe(onNext: { (user) in
                    let vm = Module.Cabinet.User.ViewModel(userId: user.id)
                    let vc = Module.Cabinet.User.Controller.create(with: vm)
                    SwiftEntryKit.display(entry: vc, using: self.setupFormPresets())
                }).disposed(by: disposeBag)
            
            viewModel.output.close
                .subscribe(onNext: { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
                })
                .disposed(by: disposeBag)
            
            searchBar.rx.text
                .subscribe(viewModel.input.searchText)
                .disposed(by: disposeBag)
            
            searchButton.rx.tap
                .subscribe(viewModel.input.search)
                .disposed(by: disposeBag)
            
            statisticButton.rx.tap
                .subscribe(viewModel.input.statistic)
                .disposed(by: disposeBag)
            
            backButton.rx.tap
                .subscribe(viewModel.input.back)
                .disposed(by: disposeBag)

            userTable.rx.itemSelected
                .subscribe(onNext: { (indexPath) in
                    viewModel.input.userSelected.onNext(indexPath.row)
                }).disposed(by: disposeBag)
        }

        func setupView() {
            self.view.addSubview(searchButton)
            self.view.addSubview(statisticButton)
            self.view.addSubview(searchBar)
            self.view.addSubview(userTable)
            self.view.addSubview(backButton)
            let bottomVC = self.makeBaseController(named: "Рейтинг", dontUseHero: true)
            
            searchBar.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview().inset(10)
                make.height.equalTo(0)
                make.top.equalTo(bottomVC.snp.bottom).offset(7)
            }
            
            backButton.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.height.equalTo(40)
                make.top.equalTo(searchBar.snp.bottom).offset(7)
            }
            
            userTable.snp.makeConstraints { (make) in
                make.left.bottom.right.equalToSuperview().inset(10)
                make.top.equalTo(backButton.snp.bottom).offset(7)
            }
            
            searchButton.snp.makeConstraints { (make) in
                make.centerY.right.equalTo(bottomVC).inset(15)
            }
            
            statisticButton.snp.makeConstraints { (make) in
                make.centerY.left.equalTo(bottomVC).inset(15)
            }

        }
    }
}
