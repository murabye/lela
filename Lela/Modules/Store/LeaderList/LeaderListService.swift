//
//  LeaderListService.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol LeaderListServiceProtocol {
    func getUsers() -> Observable<[Models.User]>
}

extension Module.Store.Leaders {
    class Service: LeaderListServiceProtocol {
        var userListRepo = UserListRepository()
        
        func getUsers() -> Observable<[Models.User]> {
            return userListRepo.getUsers().map { (users: [Models.User]) -> [Models.User] in
                return users.sorted { $0.balance > $1.balance }
            }
        }
        
    }
}
