//
//  LeaderBoardUserCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 23/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import Hero

extension Module.Store.List {
    class RecordCell: UITableViewCell {
        var user: ViewModel.RatedUser? = nil {
            didSet {
                guard let user = user else { return }
                self.name.text = user.name
                self.number.text = "\(user.number)."
                self.hero.id = Module.Store.StoreConstants.makeCellId(cellEmail: user.name)
                let selfColor = UIColor.blend(color1: UIColor.view.standart.background, intensity1: 0.95, color2: UIColor.view.standart.accent, intensity2: 0.05)
                self.bgView.backgroundColor = user.isSelf ? selfColor : UIColor.view.standart.background
                self.money.text = String(user.balance)
            }
        }

        let number = RecordCell.text()
        let name: UILabel = {
            let label = RecordCell.text()
            label.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
            return label
        }()
        let money = RecordCell.text(isBold: true)
        let currency = UIImageView(image: UIImage.money)
        let bgView = UIView()
        
        func setupViews() {
            guard self.contentView.subviews.count == 0 else { return }
            let stack = UIStackView(arrangedSubviews: [number, name, money, currency])
            self.selectionStyle = .none
            stack.axis = .horizontal
            stack.distribution = .fill
            stack.spacing = 5
            stack.makeBaseView()
            
            bgView.addSubview(stack)
            stack.snp.makeConstraints { (make) in
                make.edges.equalToSuperview().inset(10)
            }

            bgView.backgroundColor = UIColor.view.standart.background
            bgView.round(radius: 10)
            bgView.shadow(radius: 5)

            self.contentView.addSubview(bgView)
            bgView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview().inset(10)
            }
        }
        
        static func text(isBold: Bool = false) -> UILabel {
            let label = UILabel()
            label.font = isBold ? UIFont.boldSystemFont(ofSize: 14.0) : UIFont.systemFont(ofSize: 14.0)
            return label
        }
    }
}
