//
//  LeaderBoard.swift
//  Lela
//
//  Created by Влада Кузнецова on 21/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

extension Module.Store.List {
    class LeaderBoardView: UIView, ViewType {
        typealias ViewModelType = Module.Store.List.ViewModel
        var viewModel: ViewModelType!
        let disposeBag = DisposeBag()
        
        // MARK:- view elems
        static let reusableID = "Module.Store.Leaders.Cell"
        let recordTable: UITableView = {
            let table = UITableView()
            table.backgroundColor = UIColor.app.background
            table.separatorStyle = .none
            table.showsVerticalScrollIndicator = false
            table.showsHorizontalScrollIndicator = false
            table.isScrollEnabled = false
            table.register(RecordCell.self, forCellReuseIdentifier: LeaderBoardView.reusableID)
            return table
        }()
        
        let headerLabel = UILabel()
        
        let moreButton: UIButton = {
            let button = UIButton()
            button.setTitleColor(UIColor.text.standart.important, for: .normal)
            button.setTitle("Все пользователи >", for: .normal)
            button.contentHorizontalAlignment = .right
            button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            return button
        }()
        
        // MARK:- init
        static func create(with viewModel: ViewModelType) -> UIView {
            let view = LeaderBoardView()
            view.setupView()
            view.configure(with: viewModel)
            return view
        }
        
        func configure(with viewModel: ViewModelType) {
            self.viewModel = viewModel
        
            viewModel.output.users.bind(to: recordTable.rx.items(cellIdentifier: LeaderBoardView.reusableID)) { index, model, cell in
                    let cell = cell as! RecordCell
                    cell.setupViews()
                    cell.user = model
            }
            .disposed(by: disposeBag)
            
            viewModel.output.users.subscribe(onNext: { [weak self] (users) in
                guard let self = self else { return }
                self.recordTable.snp.remakeConstraints { (make) in
                    make.left.right.equalToSuperview()
                    make.top.equalTo(self.headerLabel.snp.bottom)
                    make.height.equalTo(users.count * 60)
                }
                self.recordTable.stopLoading()
                return ()
            }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)

            moreButton.rx.tap
                .subscribe(viewModel.input.allUsersInited)
                .disposed(by: disposeBag)
        }
        
        func setupView() {
            headerLabel.text = "Список лидеров"
            headerLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
            self.addSubview(headerLabel)
            headerLabel.snp.makeConstraints { (make) in
                make.top.left.right.equalToSuperview().inset(15)
            }
        
            self.addSubview(recordTable)
            recordTable.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview()
                make.top.equalTo(headerLabel.snp.bottom)
                make.width.equalToSuperview()
                make.height.equalTo(58)
            }
        
            recordTable.showLoading(isWhite: false)
            recordTable.showsVerticalScrollIndicator = false
            recordTable.showsHorizontalScrollIndicator = false
            
            self.addSubview(moreButton)
            moreButton.snp.makeConstraints { (make) in
                make.bottom.lessThanOrEqualToSuperview().inset(15)
                make.left.right.equalToSuperview().inset(15)
                make.top.equalTo(recordTable.snp.bottom).offset(15)
            }
        }
    }
}

