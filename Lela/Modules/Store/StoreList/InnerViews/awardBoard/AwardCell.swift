//
//  AwardCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 26.04.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

extension Module.Store.List {
    class AwardCell: UICollectionViewCell {
        // MARK: - data
        var award: Module.Store.List.ViewModel.AwardType? = nil {
            didSet {
                guard let strongAward = award else { return }

                self.titleLabel.text = strongAward.name
                self.priceLabel.text = String(strongAward.price)
                
                if let imgUrl = strongAward.imageUrl,
                    let url = URL(string: imgUrl) {
                    self.imageView.isHidden = false
                    self.imageView.kf.setImage(with: url,
                                               placeholder: UIImage.darkPlaceholder)
                } else {
                    self.imageView.isHidden = true
                }
                
                if strongAward.isAvailable {
                    [titleLabel, priceLabel, imageView].forEach { $0.alpha = 1 }
                } else {
                    [titleLabel, priceLabel, imageView].forEach { $0.alpha = 0.8 }
                }
            }
        }
        
        // MARK: - view
        let bubbleView: UIView = {
            let view = UIView()
            view.round()
            return view
        }()
        
        let imageView: UIImageView = {
            let imgView = UIImageView()
            imgView.contentMode = .scaleAspectFill
            imgView.isHidden = true
            return imgView
        }()
        
        let titleLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.textColor = UIColor.text.inverted.standart
            return label
        }()
        
        let priceLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 21, weight: .semibold)
            label.textColor = UIColor.text.inverted.standart
            return label
        }()
        
        let currencyImage: UIImageView = {
            let imgView = UIImageView()
            imgView.contentMode = .scaleAspectFit
            imgView.image = UIImage.money
                .withRenderingMode(.alwaysTemplate)
            imgView.tintColor = UIColor.text.inverted.standart
            return imgView
        }()
        
        let priceStack: UIStackView = {
            let stack = UIStackView()
            stack.axis = .horizontal
            stack.alignment = .center
            stack.distribution = .equalSpacing
            return stack
        }()
        
        // MARK: - setuppers
        func setupViews() {
            guard self.contentView.subviews.count == 0 else { return }
            
            self.addSubview(bubbleView)
            bubbleView.addSubview(imageView)
            bubbleView.addSubview(titleLabel)
            bubbleView.addSubview(priceStack)
            priceStack.addArrangedSubview(priceLabel)
            priceStack.addArrangedSubview(currencyImage)
            
            bubbleView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            imageView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            titleLabel.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.left.greaterThanOrEqualTo(bubbleView.snp.left)
                make.right.lessThanOrEqualTo(bubbleView.snp.right)
                make.top.equalToSuperview().offset(20)
            }
            
            priceStack.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalTo(titleLabel.snp.bottom)
            }
        }
    }
}
