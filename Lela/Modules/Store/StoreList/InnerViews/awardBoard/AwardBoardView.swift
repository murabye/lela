//
//  AwardBoard.swift
//  Lela
//
//  Created by Влада Кузнецова on 26.04.2020.
//  Copyright © 2020 варя. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import SnapKit

extension Module.Store.List {
    class AwardBoardView: UIView, ViewType {
        typealias ViewModelType = Module.Store.List.ViewModel
        var viewModel: ViewModelType!
        let disposeBag = DisposeBag()
        
        // MARK:- view elems
        let headerLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
            label.text = "Поощрения"
            return label
        }()

        static let reusableID = "Module.Store.Award.Cell"
        let awardsCollection: UICollectionView = {
            let layout = BouncyLayout()
            layout.scrollDirection = .vertical
            layout.minimumInteritemSpacing = 5
            let width = UIScreen.main.bounds.width/2 - 15*2 - 15
            layout.itemSize = CGSize(width: width, height: 230)
            let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collection.translatesAutoresizingMaskIntoConstraints = false
            collection.backgroundColor = UIColor.app.background
            collection.showsVerticalScrollIndicator = false
            collection.showsHorizontalScrollIndicator = false
            collection.isScrollEnabled = false
            collection.register(AwardCell.self, forCellWithReuseIdentifier: AwardBoardView.reusableID)
            return collection
        }()
        
        // MARK:- init
        static func create(with viewModel: ViewModelType) -> UIView {
            let view = AwardBoardView()
            view.setupView()
            view.configure(with: viewModel)
            return view
        }
        
        func configure(with viewModel: ViewModelType) {
            self.viewModel = viewModel
            
            viewModel.output.awards.bind(to:
                awardsCollection.rx.items(
                    cellIdentifier: AwardBoardView.reusableID, cellType: AwardCell.self
                )
            ) { index, model, cell in
                cell.award = model
            }.disposed(by: disposeBag)
        }
        
        func setupView() {
            guard self.subviews.count == 0 else { return }
            
            self.addSubview(headerLabel)
            headerLabel.snp.makeConstraints { (make) in
                make.top.right.equalToSuperview()
                make.left.equalToSuperview().inset(15)
                make.height.equalTo(18)
            }
            
            self.addSubview(awardsCollection)
            awardsCollection.snp.makeConstraints { (make) in
                make.left.right.bottom.equalToSuperview()
                make.top.equalTo(headerLabel.snp.bottom).offset(16)
            }
        }
    }
}

