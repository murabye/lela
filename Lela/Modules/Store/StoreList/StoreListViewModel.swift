//
//  StoreListViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 20/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

extension Module.Store.List {
    class ViewModel: ViewModelProtocol {
        struct Input {
            let allUsersInited: AnyObserver<Void>
        }
        
        struct Output {
            let users: Observable<[RatedUser]>
            let awards: Observable<[AwardType]>
            let balance: Observable<Int>
            let openAllUsersModule: Observable<Void>
        }
        
        struct RatedUser {
            let name: String
            let balance: Int
            let isSelf: Bool
            let number: Int
            let id: Int
        }
        
        struct AwardType {
            let name: String
            let price: Int
            let imageUrl: String?
            let isAvailable: Bool
        }
        
        // MARK: - Public properties
        let input: Input
        let output: Output

        // MARK: - Private properties
        private let userSubject = ReplaySubject<[RatedUser]>.create(bufferSize: 1)
        private let awardSubject = ReplaySubject<[AwardType]>.create(bufferSize: 1)
        private let allUsersInited = PublishSubject<Void>()
        private let balanceSubject = ReplaySubject<Int>.create(bufferSize: 1)

        private let disposeBag = DisposeBag()
        
        // MARK: - Init and deinit
        init(_ service: Service) {
            input = Input(allUsersInited: allUsersInited.asObserver())
            output = Output(users: userSubject.asObservable(),
                            awards: awardSubject.asObservable(),
                            balance: balanceSubject.asObservable(),
                            openAllUsersModule: allUsersInited.asObservable())
            
            service.getUsers()
                .subscribe(onNext: { [weak self] (users) in
                    let newArr = users
                        .enumerated()
                        .map { (index: Int, user: Models.User) -> RatedUser in
                        return RatedUser(name: user.name ?? user.email,
                                         balance: user.balance,
                                         isSelf: user.email == UserService.currentUser?.email ?? "?",
                                         number: index + 1,
                                         id: user.id)
                    }
                    self?.userSubject.onNext(newArr)
                    }, onError: { /*[weak self]*/ error in
                        //self?.userSubject.onError(error)
                })
                .disposed(by: disposeBag)
            
            service.getCurrentUserBalance()
                .subscribe(onNext: { [weak self] (users) in
                    self?.balanceSubject.onNext(users)
                }, onError: { [weak self] error in
                    self?.balanceSubject.onError(error)
                })
            .disposed(by: disposeBag)
            
            service.getAwards()
                .subscribe(onNext: { [weak self] (awards) in
                    let newArr = awards.map { award in
                            return AwardType(name: award.title,
                                             price: Int(award.price),
                                             imageUrl: award.imageUrl,
                                             isAvailable: award.isAvailable)
                    }
                    
                    self?.awardSubject.onNext(newArr)
                    }, onError: { /*[weak self]*/ error in
                        //self?.awardSubject.onError(error)
                })
                .disposed(by: disposeBag)
            }
        
        deinit {
            print("\(self) dealloc")
        }
    }
}
