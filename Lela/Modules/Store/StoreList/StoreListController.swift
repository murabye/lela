//
//  StoreListController.swift
//  Lela
//
//  Created by Влада Кузнецова on 20/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

extension Module.Store {
    struct StoreConstants {
        static let headerId = "STORE_HEADER"
        static let balanceId = "STORE_BALANCE"
        
        static func makeCellId(cellEmail: String) -> String {
            return "STORE_CELL_\(cellEmail)"
        }
    }
}


extension Module.Store.List {
    
    final class Controller: ViewController, ControllerType {
        // MARK:- private
        private let disposeBag = DisposeBag()
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!

        // MARK:- inner views
        private var balance = UILabel()
        private var leaderBoard: LeaderBoardView!
        private var awardBoard: AwardBoardView!

        // MARK:- lifecycle && initializers
        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }
        
        func configure(with viewModel: ViewModel) {
            self.viewModel = viewModel
            
            guard let leaderBoard = LeaderBoardView.create(with: viewModel) as? LeaderBoardView else { return }
            self.leaderBoard = leaderBoard
            
            guard let awardBoard = AwardBoardView.create(with: viewModel) as? AwardBoardView else { return }
            self.awardBoard = awardBoard

            viewModel.output.balance
                .asDriver(onErrorJustReturn: 0)
                .map { (balance: Int) -> String in
                    return String(balance)
                }
                .drive(balance.rx.text)
                .disposed(by: disposeBag)
            
            viewModel.output.openAllUsersModule
                .subscribe(onNext: { [weak self] (_) in
                    guard let self = self else { return }
                    self.showLeaders()
                })
                .disposed(by: disposeBag)
        }
        

        func setupView() {
            let scrollView = self.makeBaseScrollController(named: "Магазин поощрений")

            balance.font = UIFont.boldSystemFont(ofSize: 17)

            let balanceDescription = UILabel()
            balanceDescription.text = "Баланс:"
            balanceDescription.font = UIFont.systemFont(ofSize: 14)
            
            let currencyImage = UIImageView(image: UIImage.money)
            
            let balanceStackView = UIStackView(arrangedSubviews: [balanceDescription, balance, currencyImage])
            balanceStackView.alignment = .center
            balanceStackView.distribution = .equalSpacing
            balanceStackView.spacing = 5.0
            scrollView.addSubview(balanceStackView)
            balanceStackView.snp.makeConstraints { make in
                make.top.equalToSuperview()
                make.centerX.equalToSuperview()
            }
            balanceStackView.hero.id = Module.Store.StoreConstants.balanceId
            balanceStackView.hero.modifiers = [.useNormalSnapshot]
            
            scrollView.addSubview(leaderBoard)
            leaderBoard.snp.makeConstraints{ make in
                make.left.right.equalTo(scrollView).inset(15)
                make.top.equalTo(balanceStackView.snp.bottom).offset(15)
                make.width.equalTo(scrollView).inset(15)
            }
            scrollView.addSubview(awardBoard)
            awardBoard.snp.makeConstraints{ make in
                make.left.right.equalTo(scrollView).inset(15)
                make.top.equalTo(leaderBoard.snp.bottom).offset(15)
                make.width.equalTo(scrollView).inset(15)
            }
        }
    }
}


// MARK:- router
extension Module.Store.List.Controller {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = Module.Store.List.Controller()
        controller.viewModel = viewModel
        return controller
    }
    
    func showLeaders() {
        let service = Module.Store.Leaders.Service()
        let vm = Module.Store.Leaders.ViewModel(service)
        let vc = Module.Store.Leaders.Controller.create(with: vm) as! Module.Store.Leaders.Controller

        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .flipHorizontal
        
        self.hero.isEnabled = true
        vc.hero.isEnabled = true

        self.present(vc, animated: true)
    }
}
