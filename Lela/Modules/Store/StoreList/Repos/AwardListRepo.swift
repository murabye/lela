//
//  AwardListRepo.swift
//  Lela
//
//  Created by Влада Кузнецова on 20/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol AwardListRepositoryProtocol {
    func getAwards(onlyAvailable: Bool) -> Observable<[Models.AwardType]>
}

class AwardListRepository: AwardListRepositoryProtocol {
    struct RequestModel: Codable {
        let availableOnly: Bool
    }

    func getAwards(onlyAvailable: Bool) -> Observable<[Models.AwardType]> {
        let awardObservable: Observable<[Models.AwardType]> = NetworkManager
            .requestObservable(from: AdressesProvider.awardTypeList.value(),
                               allowedFromCache: true,
                               method: .post,
                               body: RequestModel(availableOnly: onlyAvailable))

        return awardObservable
    }
}
