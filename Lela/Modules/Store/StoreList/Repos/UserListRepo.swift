//
//  UserListRepo.swift
//  Lela
//
//  Created by Влада Кузнецова on 20/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol UserListRepositoryProtocol {
    func getUsers() -> Observable<[Models.User]>
}

class UserListRepository: UserListRepositoryProtocol {
    func getUsers() -> Observable<[Models.User]> {
        let usersObservable: Observable<[Models.User]> = NetworkManager
            .requestObservable(from: AdressesProvider.userList.value(),
                               allowedFromCache: true,
                               method: .post,
                               body: nil)

        return usersObservable
    }
}
