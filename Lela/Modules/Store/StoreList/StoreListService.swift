//
//  StoreListService.swift
//  Lela
//
//  Created by Влада Кузнецова on 20/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol StoreListServiceProtocol {
    func getUsers() -> Observable<[Models.User]>
    func getAwards() -> Observable<[Models.AwardType]>
}

extension Module.Store.List {
    class Service: StoreListServiceProtocol {
        var userListRepo = UserListRepository()
        var awardListRepo = AwardListRepository()
        var currentUserRepo = LKRepository()
        
        func getUsers() -> Observable<[Models.User]> {
            return userListRepo
                .getUsers()
                .map { (users: [Models.User]) -> [Models.User] in
                    let slice = users
                        .sorted { $0.balance > $1.balance }
                        .prefix(10)
                    return Array(slice)
            }
        }
        
        func getCurrentUserBalance() -> Observable<Int> {
            return currentUserRepo
                .getCurrentUser()
                .map { (user: Models.User) -> Int in
                    return user.balance
            }
        }
    
        func getAwards() -> Observable<[Models.AwardType]> {
            return currentUserRepo.getCurrentUser()
                .flatMap { user in
                    return self.awardListRepo.getAwards(onlyAvailable: Access.adminAward.available(user.role))
            }
        }

    }
}
