//
//  MyEventsController.swift
//  Lela
//
//  Created by Влада Кузнецова on 23.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

extension Module.History {
    final class Controller: ViewController, ControllerType {
        // MARK:- private
        private let disposeBag = DisposeBag()
        typealias ViewModelType = ViewModel
        private var viewModel: ViewModelType!

        private var oldEvent: OldEventView!
        private var activeEvent: CurrentEventView!
        private var conflictAward: CurrentAwardView!
        private var oldAward: OldAwardView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configure(with: viewModel)
            setupView()
        }
        
        func configure(with viewModel: Module.History.ViewModel) {
            self.viewModel = viewModel
            
            viewModel.output.openEvent.subscribe(onNext: { [weak self] (event) in
                let service = Module.Event.Detail.Service(with: event.id)
                let vm = Module.Event.Detail.ViewModel(service)
                let vc = Module.Event.Detail.Controller.create(with: vm)
                self?.present(vc, animated: true)
            }).disposed(by: disposeBag)
            
            viewModel.output.openAward.subscribe(onNext: { [weak self] (award) in
//                let service = Module.Event.Detail.Service(with: event.id)
//                let vm = Module.Event.Detail.ViewModel(service)
//                let vc = Module.Event.Detail.Controller.create(with: vm)
//                self?.present(vc, animated: true)
            }).disposed(by: disposeBag)

        }
        
        static func create(with viewModel: Module.History.ViewModel) -> UIViewController {
            let controller = Module.History.Controller()
            controller.viewModel = viewModel
            controller.oldEvent = OldEventView
                .create(with: viewModel) as? OldEventView
            controller.activeEvent = CurrentEventView
                .create(with: viewModel) as? CurrentEventView
            controller.conflictAward = CurrentAwardView
                .create(with: viewModel) as? CurrentAwardView
            controller.oldAward = OldAwardView
                .create(with: viewModel) as? OldAwardView
            return controller
        }
        
        func setupView() {
            let scrollView = self.makeBaseScrollController(named: "История")

            let stack = UIStackView(arrangedSubviews: [conflictAward, activeEvent, oldEvent, oldAward])
            stack.translatesAutoresizingMaskIntoConstraints = false
            stack.axis = .vertical
            stack.alignment = .leading
            stack.distribution = .equalSpacing
            stack.spacing = 8
            scrollView.addSubview(stack)
            
            [conflictAward, activeEvent, oldEvent, oldAward].forEach {
                $0?.translatesAutoresizingMaskIntoConstraints = false
            }
            conflictAward.setupView()
            activeEvent.setupView()
            oldEvent.setupView()
            oldAward.setupView()

            stack.snp.makeConstraints { make in
                make.edges.equalTo(scrollView).inset(15)
                make.width.equalTo(scrollView).inset(15)
            }
        }
    }
}
