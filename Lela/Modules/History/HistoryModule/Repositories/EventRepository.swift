//
//  EventRepository.swift
//  Lela
//
//  Created by Влада Кузнецова on 13.06.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol MyEventRepositoryProtocol {
    func getEvents(for: Int) -> Observable<[Models.Event]>
}

class MyEventRepository: MyEventRepositoryProtocol {
    struct RequestModel: Codable {
        let memberId: Int
    }

    func getEvents(for member: Int) -> Observable<[Models.Event]> {
        let awardObservable: Observable<[Models.Event]> = NetworkManager
            .requestObservable(
                from: AdressesProvider.eventList.value(),
                allowedFromCache: false,
                method: .post,
                body: RequestModel(memberId: member)
        )

        return awardObservable
    }
}
