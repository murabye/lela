//
//  AwardRepository.swift
//  Lela
//
//  Created by Влада Кузнецова on 13.06.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol AwardRepositoryProtocol {
    func getAwards(for: Int) -> Observable<[Models.Award]>
}

class AwardRepository: AwardRepositoryProtocol {
    struct RequestModel: Codable {
        let ownerId: Int
    }

    func getAwards(for owner: Int) -> Observable<[Models.Award]> {
        let awardObservable: Observable<[Models.Award]> = NetworkManager
            .requestObservable(
                from: AdressesProvider.awardExemplareList.value(),
                allowedFromCache: true,
                method: .post,
                body: RequestModel(ownerId: owner)
        )

        return awardObservable
    }
}
