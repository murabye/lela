//
//  HistoryAwardCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 13.06.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import UIKit

extension Module.History {
    class AwardCell: UICollectionViewCell {
        static let reuseId = "History.AwardCell"

        var award: Module.History.HistoryAward? = nil {
            didSet {
                guard let strongAward = award else { return }

                self.titleLabel.text = strongAward.name
                
                if let imgUrl = strongAward.imageUrl,
                    let url = URL(string: imgUrl) {
                    self.imageView.isHidden = false
                    self.imageView.kf.setImage(
                        with: url,
                        placeholder: UIImage.darkPlaceholder
                    )
                } else {
                    self.imageView.isHidden = true
                }
            }
        }
        
        // MARK: - view
        let bubbleView: UIView = {
            let view = UIView()
            view.round()
            return view
        }()
        
        let imageView: UIImageView = {
            let imgView = UIImageView()
            imgView.contentMode = .scaleAspectFill
            imgView.isHidden = true
            return imgView
        }()
        
        let titleLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.textColor = UIColor.text.inverted.standart
            return label
        }()
                
        // MARK: - setuppers
        func setupViews() {
            guard self.contentView.subviews.count == 0 else { return }
            
            self.addSubview(bubbleView)
            bubbleView.addSubview(imageView)
            bubbleView.addSubview(titleLabel)
            
            bubbleView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            imageView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            titleLabel.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview()
                make.left.greaterThanOrEqualToSuperview()
                make.right.lessThanOrEqualToSuperview()
                make.top.greaterThanOrEqualToSuperview()
                make.bottom.lessThanOrEqualToSuperview()
            }
        }
    }
}
