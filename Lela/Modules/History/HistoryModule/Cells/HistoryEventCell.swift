//
//  HistoryAwardCell.swift
//  Lela
//
//  Created by Влада Кузнецова on 13.06.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import UIKit

extension Module.History {
    class EventCell: UICollectionViewCell {
        static let reuseId = "History.EventCell"
        
        var event: Module.History.HistoryEvent? = nil {
            didSet {
                self.titleLabel.text = event?.name ?? ""
            }
        }
        
        // MARK: - view
        let bubbleView: UIView = {
            let view = UIView()
            view.round()
            view.backgroundColor = UIColor.view.inverted.background
            return view
        }()
                
        let titleLabel: UILabel = {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.textColor = UIColor.text.inverted.standart
            return label
        }()
                
        // MARK: - setuppers
        func setupViews() {
            guard self.contentView.subviews.count == 0 else { return }
            
            self.addSubview(bubbleView)
            bubbleView.addSubview(titleLabel)
            
            bubbleView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
                        
            titleLabel.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview()
                make.left.greaterThanOrEqualToSuperview()
                make.right.lessThanOrEqualToSuperview()
                make.top.greaterThanOrEqualToSuperview()
                make.bottom.lessThanOrEqualToSuperview()
            }
        }
    }
}
