//
//  HistoryOldEvent.swift
//  Lela
//
//  Created by Влада Кузнецова on 23.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

extension Module.History {
    class OldEventView : UIView, ViewType {
        typealias ViewModelType = Module.History.ViewModel
        var viewModel: ViewModelType!
        let disposeBag = DisposeBag()

        // MARK:- view elems
        let headerLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
            label.text = "Прошедшие мероприятия"
            return label
        }()

        let collection: UICollectionView = {
            let layout = BouncyLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 230)
            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.backgroundColor = .clear
            collectionView.showsVerticalScrollIndicator = false
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.hero.isEnabled = true
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            collectionView.register(EventCell.self, forCellWithReuseIdentifier: "EventCell")
            return collectionView
        }()
        
        // MARK: - configure
        func configure(with viewModel: Module.History.ViewModel) {
            self.viewModel = viewModel
            
            viewModel.output.oldEvents.bind(to:
                collection.rx.items(
                    cellIdentifier: EventCell.reuseId, cellType: EventCell.self
                )
            ) { index, model, cell in
                cell.event = model
            }.disposed(by: disposeBag)

            collection.rx
                .modelSelected(HistoryEvent.self)
                .asObservable()
                .subscribe(viewModel.input.eventSelected)
                .disposed(by: disposeBag)
        }
        
        func setupView() {
            self.addSubview(headerLabel)
            self.addSubview(collection)
                        
            headerLabel.snp.makeConstraints { make in
                make.top.equalToSuperview()
                make.left.right.equalToSuperview().inset(15)
                make.height.equalTo(18)
            }
            
            collection.snp.makeConstraints { make in
                make.top.equalTo(headerLabel.snp.bottom).offset(8)
                make.left.right.bottom.equalToSuperview()
                make.height.equalTo(200)
            }
        }
        
        static func create(with viewModel: Module.History.ViewModel) -> UIView {
            let view = OldEventView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.configure(with: viewModel)
            return view
        }
    }
}
