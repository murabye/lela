//
//  MyEventsService.swift
//  Lela
//
//  Created by Влада Кузнецова on 23.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol HistoryServiceProtocol {
//    func getAwardTypes() -> Observable<[Models.AwardType]>
    func getGettedAwards() -> Observable<[Module.History.HistoryAward]>
    func getConflictAwards() -> Observable<[Module.History.HistoryAward]>
    func getOldEvents() -> Observable<[Module.History.HistoryEvent]>
    func getActiveEvents() -> Observable<[Module.History.HistoryEvent]>
}

extension Module.History {
    struct HistoryAward {
        let awardId: Int
        let awardTypeId: Int
        let name: String
        let descriprion: String
        let imageUrl: String?
        let status: Models.Award.AwardStatus
    }
    
    struct HistoryEvent: Codable {
        let id: Int
        let name: String
        let description: String
    }

    class Service: HistoryServiceProtocol {
        let awardTypesRepository = AwardListRepository()
        let awardRepository = AwardRepository()
        let awardObservable: Observable<([HistoryAward])>
        
        let eventRepository = MyEventRepository()
        let eventObservable: Observable<([Models.Event])>

        init() {

            let awardObservable: Observable<[Models.Award]>
            if let currentUser = UserService.currentUser {
                awardObservable = awardRepository
                    .getAwards(for: currentUser.id)
                eventObservable = eventRepository
                    .getEvents(for: currentUser.id)
                    .share(replay: 1, scope: .forever)
            } else {
                awardObservable = PublishSubject<[Models.Award]>()
                    .asObservable()
                eventObservable = PublishSubject<[Models.Event]>()
                    .asObservable()
            }
            
            let awardTypeObservable = awardTypesRepository
                .getAwards(onlyAvailable: false)
            
            self.awardObservable = Observable
                .combineLatest(awardObservable, awardTypeObservable)
                .map { awards, types in
                    let historyAwards: [HistoryAward] = awards.compactMap { award in
                        guard let id = award.id,
                            let type = types.first(where: { type -> Bool in
                            return type.id == award.typeId
                        })
                        else { return nil }
                        
                        return HistoryAward(
                            awardId: id,
                            awardTypeId: award.typeId,
                            name: type.title,
                            descriprion: type.description,
                            imageUrl: type.imageUrl,
                            status: Models.Award
                                .AwardStatus(rawValue: award.status)
                                ?? .free
                        )
                    }
                    return historyAwards
            }
                .share(replay: 1, scope: .forever)
        }
        
        func getGettedAwards() -> Observable<[HistoryAward]> {
            return self.awardObservable.map { awards in
                return awards.filter {
                    return $0.status == .awarded
                        || $0.status == .free
                }
            }
        }
        
        func getConflictAwards() -> Observable<[HistoryAward]> {
            return self.awardObservable.map { awards in
                return awards.filter {
                    return $0.status == .conflicted
                        || $0.status == .waiting
                }
            }
        }
        
        func getOldEvents() -> Observable<[HistoryEvent]> {
            return eventObservable
                .map { events in
                    return events
                        .filter { event in
                        let lastDate = event.periodArray
                            .flatMap { return [$0.startDate, $0.endDate] }
                            .max()
                            return lastDate == nil ? false : lastDate! < Date()
                    }
                        .compactMap { event in
                            guard let id = event.id,
                                let descr = event.noteArray.first?.text
                                else { return nil }
                            return HistoryEvent(
                                id: id,
                                name: event.title,
                                description: descr
                            )
                    }
            }
        }
        
        func getActiveEvents() -> Observable<[HistoryEvent]> {
            return eventObservable
                .map { events in
                    return events
                        .filter { event in
                        let lastDate = event.periodArray
                            .flatMap { return [$0.startDate, $0.endDate] }
                            .max()
                            return lastDate == nil ? false : lastDate! > Date()
                    }
                        .compactMap { event in
                            guard let id = event.id,
                                let descr = event.noteArray.first?.text
                                else { return nil }
                            return HistoryEvent(
                                id: id,
                                name: event.title,
                                description: descr
                            )
                    }
            }
        }
        
    }
}
