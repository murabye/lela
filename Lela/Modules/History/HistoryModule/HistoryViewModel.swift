//
//  MyEventsViewModel.swift
//  Lela
//
//  Created by Влада Кузнецова on 23.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import RxSwift

extension Module.History {
    class ViewModel: ViewModelProtocol {
        struct Input {
            let awardSelected: AnyObserver<HistoryAward>
            let eventSelected: AnyObserver<HistoryEvent>
        }
        
        struct Output {
            let oldAwards: Observable<[HistoryAward]>
            let currentAwards: Observable<[HistoryAward]>
            let oldEvents: Observable<[HistoryEvent]>
            let currentEvents: Observable<[HistoryEvent]>
            let openEvent: Observable<HistoryEvent>
            let openAward: Observable<HistoryAward>
        }

        // MARK: - Public properties
        let input: Input
        let output: Output
        
        // MARK: - Private properties
        private let disposeBag = DisposeBag()
        
        private let eventSelected = PublishSubject<HistoryEvent>()
        private let awardSelected = PublishSubject<HistoryAward>()

        // MARK: - Init and deinit
        init(service: HistoryServiceProtocol) {
            input = Input(
                awardSelected: awardSelected.asObserver(),
                eventSelected: eventSelected.asObserver()
            )
            
            output = Output(
                oldAwards: service.getGettedAwards(),
                currentAwards: service.getConflictAwards(),
                oldEvents: service.getOldEvents(),
                currentEvents: service.getActiveEvents(),
                openEvent: eventSelected.asObservable(),
                openAward: awardSelected.asObservable()
            )
        }
        
        deinit {
            print("\(self) dealloc")
        }

    }
}
