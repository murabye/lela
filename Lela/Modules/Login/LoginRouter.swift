//
//  LoginRouter.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/10/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

class LoginRouter {
    static var isLoginPageOpened: Bool {
        get {
            let window = UIApplication.shared.windows.first
            return window?.topViewController() is LoginController
        }
    }
    
    static private var lastRootViewController: UIViewController?
    static private var onLoginCompletions = [() -> Void]()
    
    public static func loginDone() {
        let lastOpened = lastRootViewController ?? MainTabBar()
        lastOpened.hero.isEnabled = true
        
        UIApplication.setRootView(lastOpened,
                                  animationType: .uncover(direction: .down),
                                  completion: {
                                    for completion in onLoginCompletions {
                                        completion()
                                    }
                                    onLoginCompletions = []
        })
    }
  
    public static func dislogin(completion: (() -> Void)?) {
        if let completionHandler = completion {
            onLoginCompletions.append(completionHandler)
        }
        
        guard !isLoginPageOpened else { return }
        
        lastRootViewController = UIApplication.shared.windows.first?.topViewController()
        let loginRepository = LoginRepository()
        let loginControllerViewModel = LoginControllerViewModel(loginRepository)
        let loginController = LoginController.create(with: loginControllerViewModel)
    
        UIApplication.setRootView(loginController,
                                  animationType: .cover(direction: .up),
                                  completion: {})
    }
}
