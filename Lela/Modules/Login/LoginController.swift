//
//  LoginView.swift
//  Lela
//
//  Created by Владимир on 18/05/2019.
//Copyright © 2019 варя. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import Hero

//MARK: LoginView Class
final class LoginController: ViewController, ControllerType {
    
    typealias ViewModelType = LoginControllerViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()
    
    var loginField = Component.textField
    var passwordField = Component.textField
    var signInButton = Component.button
    var signUpButton = Component.helpButton

    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        
//        if let login = UserDefaults.standard.string(forKey: "login"),
//            let password = UserDefaults.standard.string(forKey: "password") {
//            viewModel.input.email.onNext(login)
//            viewModel.input.password.onNext(password)
//            viewModel.input.signInDidTap.onNext(())
//        }
        
        configure(with: viewModel)
    }
    
    func configureView() {
        self.view.backgroundColor = .white
        
        let logoView = UIImageView(image: .logo)
        logoView.contentMode = .scaleToFill
        
        self.view.addSubview(logoView)
        logoView.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
            make.height.equalTo(logoView.snp.width).dividedBy(1958.0/468.0)
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY).offset(-200)
        }
        
        let descriptionLabel = UILabel()
        descriptionLabel.text = "ВХОД"
        descriptionLabel.hero.id = "descriptionLabel"
        descriptionLabel.font = UIFont.systemFont(ofSize: 17.0, weight: .thin)
        descriptionLabel.textColor = UIColor.text.standart.standart
        
        self.view.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) in
            make.height.equalTo(35)
            make.width.equalTo(60)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(logoView.snp.bottom).offset(20)
        }
        
        let loginLabel = UILabel()
        loginLabel.text = "Логин"
        loginLabel.hero.id = "loginLabel"
        loginLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .thin)
        loginLabel.textColor = UIColor.text.standart.standart
        self.view.addSubview(loginLabel)
        loginLabel.snp.makeConstraints { (make) in
            make.width.lessThanOrEqualTo(self.view.snp.width).offset(-30)
            make.width.equalTo(270)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(descriptionLabel.snp.bottom).offset(20)
        }
        
        loginField.placeholder = "Обязательно"
        loginField.returnKeyType = .continue
        loginField.autocorrectionType = .no
        loginField.autocapitalizationType = .none
        loginField.hero.id = "loginField"
        
        self.view.addSubview(loginField)
        loginField.snp.makeConstraints { (make) in
            make.width.equalTo(loginLabel.snp.width).offset(30)
            make.height.equalTo(40)
            make.top.equalTo(loginLabel.snp.bottom).offset(6)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        let passwordLabel = UILabel()
        passwordLabel.text = "Пароль"
        passwordLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .thin)
        passwordLabel.textColor = UIColor.text.standart.standart
        passwordLabel.hero.id = "passwordLabel"
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (make) in
            make.width.equalTo(loginLabel.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(loginField.snp.bottom).offset(12)
        }
        
        passwordField.placeholder = "Обязательно"
        passwordField.returnKeyType = .done
        passwordField.isSecureTextEntry = true
        passwordField.autocapitalizationType = .none
        passwordField.hero.id = "passwordField"
        
        self.view.addSubview(passwordField)
        passwordField.snp.makeConstraints { (make) in
            make.width.equalTo(loginField.snp.width)
            make.height.equalTo(40)
            make.top.equalTo(passwordLabel.snp.bottom).offset(6)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        signInButton.setTitle("ВОЙТИ", for: .normal)
        signInButton.hero.id = "signInButton"
        
        self.view.addSubview(signInButton)
        signInButton.snp.makeConstraints { (make) in
            make.width.equalTo(passwordField.snp.width)
            make.height.equalTo(40)
            make.top.equalTo(passwordField.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        signUpButton.setTitle("Регистрация", for: .normal)
        signUpButton.hero.id = "signUpButton"
        
        self.view.addSubview(signUpButton)
        signUpButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.top.equalTo(signInButton.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.snp.centerX)
        }
    }
    
    func configure(with viewModel: LoginControllerViewModel) {
        
        loginField.rx.text.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.email)
            .disposed(by: disposeBag)
        
        passwordField.rx.text.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.password)
            .disposed(by: disposeBag)
        
        let signInObservable = signInButton.rx.tap.asObservable()
        signInObservable
            .subscribe(onNext: { [weak self] in
                self?.signInButton.show(loading: true)
                self?.view.endEditing(true)
            })
            .disposed(by: disposeBag)
        
        signInObservable
            .subscribe(viewModel.input.signInDidTap)
            .disposed(by: disposeBag)
        
        signUpButton.rx.tap.asObservable()
            .subscribe(viewModel.input.signUpDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.errors
            .subscribe(onNext: { [weak self] (error) in
                self?.signInButton.show(loading: false)
                self?.presentError(error)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.loginResult
            .subscribe(onNext: { [unowned self] (user) in
                let tabBar = MainTabBar()
                tabBar.hero.isEnabled = true
                
                self.view.hero.modifiers = [.whenDisappearing(.translate(x: 0, y: 1200, z: 0), .fade)]
                
                self.present(tabBar, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.signUp
            .subscribe({[unowned self] _ in
                let registerRepository = RegisterService(with: LoginRepository())
                let registerControllerViewModel = RegisterControllerViewModel(registerRepository)
                let registerController = RegisterController.create(with: registerControllerViewModel)
                registerController.modalPresentationStyle = .fullScreen
                registerController.hero.isEnabled = true
                
                self.present(registerController, animated: true)
            })
            .disposed(by: disposeBag)

    }
    
    func showAuthAlert() {
        let alert = UIAlertController(title: "Извините", message: "Что–то пошло не так: зайдите еще раз, чтобы продолжить работу", preferredStyle: .alert)
        self.present(alert, animated: false, completion: nil)
    }
    
    override func keyboardShowView() -> UIView? {
        return passwordField
    }

    override func keyboardViewOffsetY() -> CGFloat {
        return 100
    }
    
}

extension LoginController {
    static func create(with viewModel: ViewModelType) -> UIViewController {
        let controller = LoginController()
        controller.viewModel = viewModel
        return controller
    }
}
