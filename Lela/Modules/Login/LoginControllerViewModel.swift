//
//  LoginControllerViewModel.swift
//  MVVM-RxSwift-LoginFlow
//
//  Created by Alexey Savchenko on 6/19/18.
//  Copyright © 2018 Alexey Savchenko. All rights reserved.
//

import RxSwift

class LoginControllerViewModel: ViewModelProtocol {
    struct Input {
        let email: AnyObserver<String>
        let password: AnyObserver<String>
        let signInDidTap: AnyObserver<Void>
        let signUpDidTap: AnyObserver<Void>
    }
    struct Output {
        let loginResult: Observable<UserService.LoginResponce>
        let errors: Observable<Error>
        let signUp: Observable<Void>
    }
    // MARK: - Public properties
    let input: Input
    let output: Output
    
    // MARK: - Private properties
    private let emailSubject = PublishSubject<String>()
    private let passwordSubject = PublishSubject<String>()
    private let signInDidTapSubject = PublishSubject<Void>()
    private let signUpDidTapSubject = PublishSubject<Void>()
    private let loginResultSubject = PublishSubject<UserService.LoginResponce>()
    private let errorsSubject = PublishSubject<Error>()
    private let disposeBag = DisposeBag()
    
    private var credentialsObservable: Observable<Credentials> {
        return Observable.combineLatest(emailSubject.asObservable(), passwordSubject.asObservable()) { (email, password) in
            return Credentials(email: email, password: password)
        }
    }
    
    // MARK: - Init and deinit
    init(_ loginRepository: LoginRepositoryProtocol) {
        
        input = Input(email: emailSubject.asObserver(),
                      password: passwordSubject.asObserver(),
                      signInDidTap: signInDidTapSubject.asObserver(),
                      signUpDidTap: signUpDidTapSubject.asObserver())
        
        output = Output(loginResult: loginResultSubject.asObservable(),
                        errors: errorsSubject.asObservable(),
                        signUp: signUpDidTapSubject.asObservable())
        
        signInDidTapSubject
            .withLatestFrom(credentialsObservable)
            .flatMapLatest { credentials in
                return loginRepository.signIn(with: credentials).materialize()
            }
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .next(let person):
                    self?.loginResultSubject.onNext(person)
                case .error(let error):
                    self?.errorsSubject.onNext(error)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
        
        if let autoLogin = KeychainService.getCredentials() {
            emailSubject.onNext(autoLogin.email)
            passwordSubject.onNext(autoLogin.password)
            signInDidTapSubject.onNext(())
        }
    }
    
    deinit {
        print("\(self) dealloc")
    }
}
