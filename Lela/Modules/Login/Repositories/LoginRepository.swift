//
//  LoginRepository.swift
//  Lela
//
//  Created by Владимир on 21/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginRepositoryProtocol {
    func signIn(with credentials: Credentials) -> Observable<UserService.LoginResponce>
}

struct Credentials {
    let email: String
    let password: String
}

class LoginRepository: LoginRepositoryProtocol {    
    func signIn(with credentials: Credentials) -> Observable<UserService.LoginResponce> {
        KeychainService.setCredentials(credentials)
        return NetworkManager.login(with: credentials)
    }
}
