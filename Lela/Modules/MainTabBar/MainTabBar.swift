//
//  MainTabBar.swift
//  Lela
//
//  Created by варя on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import Hero

class MainTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalTransitionStyle = .flipHorizontal
        modalPresentationStyle = .overFullScreen
        
        self.tabBar.hero.modifiers = [.translate(x: 0, y: 100, z: 0)]
        self.tabBar.hero.isEnabled = true
        self.hero.isEnabled = true

        // color
        self.tabBar.tintColor = UIColor.tabBar.selectedItem
        self.tabBar.unselectedItemTintColor = UIColor.tabBar.unselectedItem
        self.tabBar.barTintColor = UIColor.tabBar.background
        
        // round
        self.tabBar.clipsToBounds = true
        self.tabBar.layer.cornerRadius = 20
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.tabBar.isTranslucent = true
        
        // shadow
        //self.tabBar.layer.masksToBounds = false
        self.tabBar.layer.shadowOffset = CGSize.zero
        self.tabBar.layer.shadowRadius = 20.0
        self.tabBar.layer.shadowColor = UIColor.shadow.standart.cgColor
        self.tabBar.layer.shadowOpacity = 1
        self.tabBar.layer.shadowPath =
            UIBezierPath(roundedRect: self.tabBar.bounds,
                         cornerRadius: self.tabBar.layer.cornerRadius).cgPath
        
        
        let lk = Module.Cabinet.Personal.Controller.create(with: Module.Cabinet.Personal.ViewModel(LKRepository()))
        lk.tabBarItem.image = UIImage.profileMenu
        
        let eventList = EventListController.create(with: EventListControllerViewModel(EventListRepository()))
        eventList.tabBarItem.image = UIImage.eventsMenu
        
        let store = Module.Store.List.Controller.create(with: Module.Store.List.ViewModel(Module.Store.List.Service()))
        store.tabBarItem.image = UIImage.store

        let history = Module.History.Controller.create(with: Module.History.ViewModel(service: Module.History.Service()))
        history.tabBarItem.image = UIImage.history
        
        self.viewControllers = [eventList, store, history, lk]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
