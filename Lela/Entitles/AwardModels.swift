//
//  AwardModels.swift
//  Lela
//
//  Created by Влада Кузнецова on 26.04.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation

extension Models {
    struct AwardType: Codable {
        var id: Int
        
        var title: String
        var description: String
        var isAvailable: Bool
        var price: Double
        
        var imageUrl: String?
    }
    
    struct Award: Codable {
        var id: Int?
        
        var typeId: Int
        var ownerId: Int?
        var operatorId: Int?
        
        var status: Int
        
        var priceFact: Double
        var description: String
        
        enum AwardStatus: Int {
            case free = 0
            case waiting = 1
            case awarded = 2
            case conflicted = 3
            case cancelled = 4
        }

        func getStatus() -> AwardStatus {
            return AwardStatus.init(rawValue: status) ?? .free
        }
    }

    class AwardOffice: Codable {
        var id: Int
        var address: String
        var operatorFio: [String]
        var timetable: String
    }
}
