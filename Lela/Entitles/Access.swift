//
//  Access.swift
//  Lela
//
//  Created by Влада Кузнецова on 03.05.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation

enum Access: Int {
    
    case modifyCategory = 0b1000000
    case modifyAward = 0b0100000
    case adminAward = 0b0010000
    case manageUserRole = 0b0001000
    
    func available(_ value: Int) -> Bool {
        return value & self.rawValue != 0
    }
}
