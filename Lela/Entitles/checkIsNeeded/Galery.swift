//
//  Galery.swift
//  Lela
//
//  Created by Вова Петров on 13.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

struct Galery {
    let id: Int
    let eventId: Int
    let photoId: Int
}
