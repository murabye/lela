//
//  Icon.swift
//  Lela
//
//  Created by Вова Петров on 13.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

struct Icon {
    let id: Int
    let icon: UIImage?
    let alter: String?
}
