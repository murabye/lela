//
//  Photo.swift
//  Lela
//
//  Created by Вова Петров on 13.08.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

struct Photo {
    let id: Int
    let image: UIImage
    let description: String?
    let personId: Int
}
