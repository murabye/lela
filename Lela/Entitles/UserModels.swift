//
//  UserModels.swift
//  Lela
//
//  Created by Влада Кузнецова on 26.04.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import Foundation

extension Models {
    struct User: Codable {
        var id: Int
        
        var avatarURL: String?
        var name: String?
        var surname: String?
        var birthday: String?
        
        var balance: Int
        var points: Int
        
        var email: String
        var registerDate: Date
        var role: Int
        var orgInfo: String
        
        static func empty() -> User {
            return User(id: -1,
                        avatarURL: nil,
                        name: nil,
                        surname: nil,
                        birthday: nil,
                        balance: 0,
                        points: 0,
                        email: "",
                        registerDate: Date(),
                        role: 0,
                        orgInfo: "")
        }
    }
}
