//
//  EventCategory.swift
//  Lela
//
//  Created by Вова Петров on 12.09.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

class Models {
    struct EventCategory: Codable {
        let title: String
        let iconId: String
        let id: Int?
        var events: [Event]?
    }
    
    struct Event: Codable {
        var id: Int?
        var title: String
        var points: Int
        var maxPersons: Int? //max people
        
        var periodArray: [Period]
        var categoryId: Int
        var ownerId: Int
        
        var partArray: [Part]
        var noteArray: [Note]
//        var photoArray: [Photo]
    }
    
    struct File: Codable {
        var id: Int
        var url: String
    }
    
    struct Period: Codable {
        var startDate: Date
        var endDate: Date
    }
    
    struct Note: Codable {
        var number: Int
        var theme: String?
        var text: String
        var isAnonimus: Bool
        var personId: Int?
        var person: User?
        var time: String
        var attachmentId: Int?
        
        static func getEmpty() -> Note {
            return Note(number: 0,
                        theme: nil,
                        text: "Пустое описание",
                        isAnonimus: true,
                        personId: nil,
                        person: nil,
                        time: Date().getString(with: .server),
                        attachmentId: nil)
        }
    }
    
    struct Part: Codable {
        var id: Int
        var userId: Int
        var confirmed: Bool
    }
        
}
