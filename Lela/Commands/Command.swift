//
//  Command.swift
//  Lela
//
//  Created by Владимир on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

class Command {
    private var performBlock: ()->()
    
    init(block: @escaping ()->()) {
        self.performBlock = block
    }
    
    func perform(){
        self.performBlock()
    }
}
