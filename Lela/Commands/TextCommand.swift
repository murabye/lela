//
//  TextCommand.swift
//  Lela
//
//  Created by Владимир on 19/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

class TextCommand {
    private var performBlock: (String)->()
    
    init(block: @escaping (String)->()) {
        self.performBlock = block
    }
    
    func performWith(_ text: String){
        self.performBlock(text)
    }
}
