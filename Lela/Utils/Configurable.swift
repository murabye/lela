//
//  Configurable.swift
//  Lela
//
//  Created by варя on 02/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

protocol Configurable {
    func configureView()
}
