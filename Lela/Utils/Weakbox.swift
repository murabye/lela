//
//  Weakbox.swift
//  Lela
//
//  Created by варя on 02/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

final class WeakBox<A: AnyObject> {
    weak var unbox: A?
    init(_ value: A) {
        unbox = value
    }
}
