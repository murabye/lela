//
//  DateFormatter+populars.swift
//  Lela
//
//  Created by варя on 02/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

extension Date {
    enum PopularFormates {
        case time
        case dateTime
        case date
        case server
    }
    
    func getString(with formate: PopularFormates) -> String {
        return Date.getFormatter(with: formate).string(from: self)
    }
    
    static func transform(string: String, with formate1: PopularFormates, to formate2: PopularFormates) -> String? {
        let toDateFormatter = Date.getFormatter(with: formate1)
        let toStringFormatter = Date.getFormatter(with: formate2)
        
        guard let date = toDateFormatter.date(from: string) else { return nil }
        return toStringFormatter.string(from: date)
    }
    
    func string(with formate: PopularFormates) -> String {
        let toStringFormatter = Date.getFormatter(with: formate)
        return toStringFormatter.string(from: self)
    }
    
    private static func getFormatter(with formate: PopularFormates) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")

        switch formate {
        case .time:
            formatter.dateFormat = "HH:mm"
        case .dateTime:
            formatter.dateFormat = "MM.dd.yy HH:mm"
        case .date:
            formatter.dateFormat = "MM.dd.yy"
        case .server:
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        }
        
        return formatter
    }
}
