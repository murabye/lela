//
//  JSONEncode+Responce.swift
//  Lela
//
//  Created by Влада Кузнецова on 04/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation
import Alamofire

class ResponceDecoder {
  static func decodeResponse<T: Decodable>(from response: DataResponse<Any>) -> Result<T> {
     guard response.error == nil else {
       return .failure(response.error!)
     }
    
     guard let responseData = response.data else {
        return .failure(NetworkManager.Errors.unknown(description: nil))
     }
    
     do {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601
        let item = try jsonDecoder.decode(T.self, from: responseData)
        return .success(item)
     } catch {
        return .failure(error)
     }
  }
}

