//
//  UIView+Alert.swift
//  Lela
//
//  Created by Влада Кузнецова on 01/12/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, text: String, okHandler: (()->())? = nil) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ок", style: .default) { (_) in
            if let okHandler = okHandler {
                okHandler()
            }
        }
        alert.addAction(ok)
        self.present(alert, animated: false, completion: nil)
    }
}
