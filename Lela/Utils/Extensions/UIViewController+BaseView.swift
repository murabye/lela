//
//  UIViewController+BaseView.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit
import SnapKit
import Hero

extension UIViewController {
    /// returned bottom vc of new structure
    func makeBaseController(named: String, dontUseHero: Bool = false) -> UIView {
        self.view.backgroundColor = UIColor.app.background
        let logo = Component.headerLogo
        if dontUseHero {
            logo.hero.id = nil
        }
        let pageName = Component.headerPageName
        pageName.text = named.uppercased()

        self.view.addSubview(logo)
        self.view.addSubview(pageName)
        
        let logoImgSize = UIImage.logo.size
        
        logo.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(logo.snp.height).multipliedBy(logoImgSize.width / logoImgSize.height)
            make.width.equalTo(150)
        }
        
        pageName.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(25)
            make.left.right.equalTo(view)
            make.height.equalTo(50)
        }
        
        return pageName
    }
    
    /// returned bottom vc of new structure
    func makeBaseScrollController(named: String, dontUseHero: Bool = false) -> UIScrollView {
        let bottomVC = self.makeBaseController(named: named, dontUseHero: dontUseHero)
        let scrollView = UIScrollView()
        scrollView.autoresizingMask = .flexibleHeight
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.top.equalTo(bottomVC.snp.bottom)
        }
        
        return scrollView
    }

}
