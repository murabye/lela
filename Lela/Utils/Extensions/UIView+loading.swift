//
//  UIView+.swift
//  Lela
//
//  Created by Вова Петров on 01.12.2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIView {
    func showLoading(isWhite: Bool = true) {
        UIView.animate(withDuration: 0.25) {
            self.subviews.forEach{ $0.alpha = 0.5 }
        }
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        if !isWhite {
            activity.color = UIColor.loading.activityIndicator
        }
        activity.startAnimating()
        addSubview(activity)
        activity.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    func stopLoading() {
        UIView.animate(withDuration: 0.25) {
            self.subviews.forEach{ $0.alpha = 1.0 }
        }
        
        subviews.forEach{
            if $0 is UIActivityIndicatorView { $0.removeFromSuperview() }
        }
    }
}
