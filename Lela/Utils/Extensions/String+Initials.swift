//
//  String+.swift
//  VIP
//
//  Created by Vladimir Petrov on 18/05/2019.
//  Copyright © 2019 Vladimir Petrov. All rights reserved.
//

import Foundation

extension String {
    var first: String {
        return String(prefix(1))
    }
    var uppercasedFirst: String {
        return first.uppercased() + String(dropFirst())
    }

    func getNameInitials() -> String {
        let nameParts = self.components(separatedBy: " ")
        guard nameParts.count > 1 else {
            return self
        }
        
        let surname = nameParts[0]
        let name = nameParts[1].first.uppercased() + "."
        
        if nameParts.count == 2 {
            return surname + " " + name
        }
        
        let patronymic = nameParts[2].first.uppercased() + "."
        return surname + " " + name + patronymic
    }
    
    
    static func getNameInitials(name: String?, surname: String?) -> String {
        if name == nil && surname == nil {
            return "не определено"
        } else if let userName = name, let userSurname = surname {
            return userName.first.uppercased() + ". " + userSurname.uppercasedFirst
        } else if let userName = name {
            return userName.uppercasedFirst
        } else if let userSurname = surname {
            return userSurname.uppercasedFirst
        }
        
        return "не определено"
    }
}

func safeString(_ value: Any?) -> String {
    guard let text = value else { return "" }
    return String(describing: text)
}

