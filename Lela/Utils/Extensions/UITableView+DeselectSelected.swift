//
//  UITableView+DeselectSelected.swift
//  Lela
//
//  Created by варя on 02/05/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UITableView {
    func deselectSelectedRow() {
        guard let selectedIndexPath = self.indexPathForSelectedRow else {return}
        self.deselectRow(at: selectedIndexPath, animated: true)
    }
}
