//
//  UIViewController+Visible.swift
//  Lela
//
//  Created by Влада Кузнецова on 04/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIWindow {
    func topViewController() -> UIViewController? {
        var top = self.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
}
