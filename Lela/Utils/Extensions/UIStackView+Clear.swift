//
//  UIStackView+Clear.swift
//  Lela
//
//  Created by Влада Кузнецова on 21/01/2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit

extension UIStackView {
    @discardableResult func removeAllArrangedSubviews() -> [UIView] {
        let removedSubviews = arrangedSubviews.reduce([]) { (removedSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            NSLayoutConstraint.deactivate(subview.constraints)
            subview.removeFromSuperview()
            return removedSubviews + [subview]
        }
        return removedSubviews
    }
}
