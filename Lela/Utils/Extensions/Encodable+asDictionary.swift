//
//  Encodable+asDictionary.swift
//  Lela
//
//  Created by Влада Кузнецова on 03/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import Foundation

extension Encodable {
  var asDictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
