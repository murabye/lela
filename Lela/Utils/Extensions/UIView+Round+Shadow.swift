//
//  UIView+Round.swift
//  Lela
//
//  Created by Влада Кузнецова on 17/11/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit

extension UIView {
    func round(radius: CGFloat = 15) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func shadow(radius: CGFloat = 10) {
        layer.shadowColor = UIColor.shadow.standart.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 1
        layer.shadowRadius = radius
        layer.masksToBounds = false
    }

    func makeBaseView() {
        self.backgroundColor = UIColor.view.standart.background
        self.round(radius: 15)
        self.shadow(radius: 5)
    }
}
