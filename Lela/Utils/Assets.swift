//
//  Assets.swift
//  Lela
//
//  Created by Вова Петров on 12.01.2020.
//  Copyright © 2020 варя. All rights reserved.
//

import UIKit

class Assets {
    enum Icon: String, CaseIterable {
        case binoculars = "binoculars_icon"
        case link = "link_icon"
        case home = "home_icon"
        case bookmark = "bookmark_icon"
        case idea = "idea_icon"
        case info = "info_icon"
        case calendar = "calendar_icon"
        case picture = "picture_icon"
        case key = "key_icon"
        case contacts = "contacts_icon"
        case box = "box_icon"
        case trash = "trash_icon"
        case settings = "settings_icon"
        case news = "news_icon"
        case aboutUs = "about_us_icon"
        case cancel = "cancel_icon"
        case checkAll = "check_all_icon"
        case plus = "plus_icon"
        case share = "share_icon"
        case portfolio = "portfolio_icon"
        case checked = "checked_icon"
        case edit = "edit_icon"
        case sun = "sun_icon"
        case delete = "delete_icon"
        case clock = "clock_icon"
        case toolbox = "toolbox_icon"
        case icSuccess = "ic_success"
        
        static func icon(for name: String? = "") -> Icon {
            Icon.allCases.first { $0.rawValue == name } ?? Icon.binoculars
        }

        var image: UIImage {
            switch self {
                case .binoculars:   return UIImage(named: "binoculars")!
                case .link:         return UIImage(named: "link")!
                case .home:         return UIImage(named: "home")!
                case .bookmark:     return UIImage(named: "bookmark")!
                case .idea:         return UIImage(named: "idea")!
                case .info:         return UIImage(named: "info")!
                case .calendar:     return UIImage(named: "calendar")!
                case .picture:      return UIImage(named: "picture")!
                case .key:          return UIImage(named: "key")!
                case .contacts:     return UIImage(named: "contacts")!
                case .box:          return UIImage(named: "box")!
                case .trash:        return UIImage(named: "trash")!
                case .settings:     return UIImage(named: "settings")!
                case .news:         return UIImage(named: "news")!
                case .aboutUs:      return UIImage(named: "about_us")!
                case .cancel:       return UIImage(named: "cancel")!
                case .checkAll:     return UIImage(named: "check_all")!
                case .plus:         return UIImage(named: "plus")!
                case .share:        return UIImage(named: "share")!
                case .portfolio:    return UIImage(named: "portfolio")!
                case .checked:      return UIImage(named: "checked")!
                case .edit:         return UIImage(named: "edit")!
                case .sun:          return UIImage(named: "sun")!
                case .delete:       return UIImage(named: "delete")!
                case .clock:        return UIImage(named: "clock")!
                case .toolbox:      return UIImage(named: "toolbox")!
                case .icSuccess:    return UIImage(named: "ic_success")!
            }
        }
    }
}
