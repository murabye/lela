//
//  UIApplication.swift
//  Lela
//
//  Created by Влада Кузнецова on 22/10/2019.
//  Copyright © 2019 варя. All rights reserved.
//

import UIKit
import Hero

extension UIApplication {
    public static func setRootView(_ viewController: UIViewController,
                    animationType: HeroDefaultAnimationType?,
                                    completion: (() -> Void)? = nil) {
        guard let animationType = animationType,
            UIApplication.shared.keyWindow != nil,
            UIApplication.shared.keyWindow!.rootViewController != nil else {
            UIApplication.shared.keyWindow?.rootViewController = viewController
            return
        }
        
        UIApplication.shared.keyWindow?.rootViewController?.hero.isEnabled = true
        viewController.hero.isEnabled = true
        
        let transition = HeroTransition()
        transition.defaultAnimation = animationType
        
        transition.transition(from: UIApplication.shared.keyWindow!.rootViewController!, to: viewController, in: UIApplication.shared.keyWindow!) { (finished) in
            UIApplication.shared.keyWindow!.rootViewController = viewController
            if let completion = completion {
                completion()
            }
        }
    }
}

